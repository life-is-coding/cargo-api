<div>
    <h4>เรียน คุณ {{ $user->CstFirstname }} {{ $user->CstLastname }}</h4>
    <p>คุณได้ทำการสมัครสมาชิกเว็บไซต์ Banana The Cargo เรียบร้อยแล้ว</p>
    <p>กรุณายืนยันการสมัครสมาชิกอีกครั้ง เพื่อเริ่มใช้งาน โดยกดที่ปุ่มด้านล่าง</p>
    <p>
        <a href="https://api.bananathecargo.com/verify?tk={{ $token }}" target="_blank">
            <button type="button">
                ยืนยันการสมัครสมาชิก
            </button>
        </a>
    </p>
</div>