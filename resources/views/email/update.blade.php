<div>
    <h4>เรียน คุณ {{ $order->CstFirstname }} {{ $order->CstLastname }}</h4>
    <p>แจ้งอัพเดทสถานะคำสั่งซื้อเลขที่คำสั่งซื้อ : {{ $order->No }}</p>
    @if( $type=='order' && $status_id == 3 )
    <p>กรุณาตรวจสอบข้อมูลใบเสนอราคา และ ทำการยืนยันคำสั่งซื้อผ่านทางหน้าเว็บไซต์ หรือ ติดต่อเจ้าหน้าที่</p>
    @endif
    @if( $type=='order' && $status_id == 5 )
    <p>ขณะนี้ทางเจ้าหน้าที่ได้ดำเนินการสั่งซื้อสินค้าของท่านกับทางร้านค้าเรียบร้อยแล้ว ทางร้านค้ากำลังเตรียมจัดส่งพัสดุ</p>
    <p><b>สถานะคำสั่งซื้อ </b> : {{ $order->UserStatus }}</p>
    <p><b>วันที่สั่งซื้อกับร้านค้า </b> : {{ $order->OrderDate }}</p>
    <p><b>เลขที่คำสั่งซื้อของร้านค้า </b> : {{ $order->OrderNumber }}</p>
    @endif
    @if( $type=='delivery' && $status_id == 2 )
    <p>คำสั่งซื้อของท่านได้จัดส่งถึงโกดังจีน และ ได้จัดส่งพัสดุออกมาที่ประเทศไทยเรียบร้อยแล้ว </p>
    <p><b>วันที่ของถึงโกดังจีน </b> : {{ $delivery->WarehouseCNDate }}</p>
    <p><b>วันที่ส่งออกจากโกดังจีน </b> : {{ $delivery->ExportDate }}</p>
    <p><b>เลขที่บิลใบสั่งซื้อ (PO) </b> : {{ $delivery->PurchaseOrder }}</p>
    <p><b>ตู้ที่ </b> : {{ $delivery->Container }}</p>
    <p><b>ขนาดของพัสดุ ( กว้าง x ยาว x สูง )</b> : {{ $delivery->Width }} x {{ $delivery->Length }} x {{ $delivery->Height }}</p>
    <p><b>ปริมาตรของพัสดุ </b> : {{ $delivery->Queue }} CBM</p>
    <p><b>น้ำหนักของพัสดุ </b> : {{ $delivery->KG }} KG</p>
    @endif
    @if( $type=='delivery' && $status_id == 3 )
    <p>คำสั่งซื้อของท่านได้จัดส่งถึงโกดังไทยเรียบร้อยแล้ว กรุณาติดต่อเจ้าหน้าที่เพื่อดำเนินการชำระค่าจัดส่งอีกครั้ง</p>
    <p><b>วันที่ของถึงโกดังไทย </b> : {{ $delivery->ImportDate }}</p>
    @endif
    @if( $type=='delivery' && $status_id == 5 )
    <p>คำสั่งซื้อของท่านได้จัดส่งออกจากโกดังไทยเรียบร้อยแล้ว ข้อมูลการจัดส่งตามรายละเอียดด้านล่าง</p>
    <p><b>วันที่จัดส่ง </b> : {{ $delivery->DeliveryDate }}</p>
    <p><b>บริษัทขนส่ง </b> : {{ $delivery->CompanyName }}</p>
    <p><b>เลขที่จัดส่ง ( Tracking No. ) </b> : {{ $delivery->TrackingNO }}</p>
    @endif
    @if( $type=='order' && $status_id == 9 )
    <p>คำสั่งซื้อของท่านเสร็จสมบูรณ์แล้ว ขอบคุณที่ไว้วางใจใช้บริการ bananathecargo.com </p>
    @endif
    <hr />
    <p>ท่านสามารถตรวจสอบสถานะคำสั่งซื้อ และ รายละเอียดเพิ่มเติมได้ที่ทางหน้าเว็บไซต์
        <a href="https://www.bananathecargo.com" target="_blank">
            www.bananathecargo.com
        </a>
    </p>
</div>