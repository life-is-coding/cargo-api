<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Banana The Cargo </title>
    <link href="{{ asset('css/font-awesome/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">
</head>

<body id="page-top">
    <div id="wrapper">
        <div id="content-wrapper" class="d-flex flex-column pt-4 pb-4">
            <div id="content" class="pt-4 pb-4">
                <div class="container-fluid pt-4 pb-4">
                    <div class="text-center pt-4 pb-4">
                        <h1 class="mx-auto mt-4 fw-100">{{ $res['message1'] }}</h1>
                        <p class="lead text-gray-800 mt-5 mb-5">{{ $res['message2'] }}</p>
                        <p class="text-gray-500 mb-0">web page will be redirect to main page in ...<span id="txt">5</span> seconds <br />or <a href="https://www.bananathecargo.com">Click here Go to Main Page</a> </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/jquery/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
</body>

<script>
    $(document).ready(function() {
        let x = 5
        setInterval(function() {
            x--
            if (x == -1) {
                window.location.href = 'https://www.bananathecargo.com';
            }
            if (x >= 0) {
                $("#txt").html(x)
            }
        }, 1000); // 5 seconds   
    });
</script>

</html>