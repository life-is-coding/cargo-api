@php
if (!Auth::check()) {
header( "location: https://api.bananathecargo.com/login" );
exit(0);
}
@endphp
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <!-- <title>Banana The Cargo : Admin</title> -->
    @yield('title')
    <link href="{{ asset('css/font-awesome/all.min.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet">

    <!-- dataTables -->
    <link href="{{ asset('css/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">

    <!-- select2 -->
    <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />

    <!-- litepicker -->
    <!-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/litepicker/dist/css/litepicker.css" /> -->

    @yield('css')

    <style>
        .select2-container--default .select2-selection--single {
            border-color: #d2d6de;
            display: block;
            width: 100%;
            height: 38px;
        }

        .select2-selection__rendered {
            padding-left: 14px !important;
            padding-top: 5px !important;
            color: #858796 !important;
        }

        .select2-selection__arrow {
            margin-top: 5px !important;
        }

        .pointerclick {
            cursor: pointer !important;
        }
    </style>
</head>

<body id="page-top">
    <div id="wrapper">

        <!-- Sidebar -->
        <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('orders') }}">
                <div class="sidebar-brand-icon rotate-n-15">
                    <i class="fas fa-laugh-wink"></i>
                </div>
                <div class="sidebar-brand-text mx-3">BANANA THE CARGO</div>
            </a>
            <hr class="sidebar-divider my-0">
            <!-- <li class="nav-item {{ (Request::is('dashboard') ? 'active' : '') }} ">
                <a class="nav-link" href="{{ route('dashboard') }}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>หน้าหลัก</span>
                </a>
            </li> 
            <hr class="sidebar-divider my-0"> -->
            <li class="nav-item {{ (Request::is('orders') ? ' active ' : '') }} ">
                <a class="nav-link" href="{{ route('orders') }}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>รายการคำสั่งซื้อ</span>
                </a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (Request::is('customers') ? ' active ' : '') }} ">
                <a class="nav-link" href="{{ route('customers') }}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>ข้อมูลลูกค้า</span>
                </a>
            </li>
            <hr class="sidebar-divider my-0">
            <li class="nav-item {{ (Request::is('report') ? ' active ' : '') }}">
                <a class="nav-link" href="{{ route('report') }}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>รายงาน</span>
                </a>
            </li>
            <hr class="sidebar-divider">
            @if(Auth::user()->role=='superadmin')
            <div class="sidebar-heading">
                ตั้งค่า
            </div>
            <li class="nav-item {{ (Request::is('users') ? ' active ' : '') }}">
                <a class="nav-link" href="{{ route('users') }}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>ผู้ใช้งาน (แอดมิน)</span>
                </a>
            </li>
            <li class="nav-item {{ (Request::is('fees') ? ' active ' : '') }}">
                <a class="nav-link" href="{{ route('fees') }}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>ค่าธรรมเนียมขนส่ง</span>
                </a>
            </li>
            <li class="nav-item {{ (Request::is('products') ? ' active ' : '') }}">
                <a class="nav-link" href="{{ route('products') }}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>ประเภทสินค้า</span>
                </a>
            </li>
            <li class="nav-item {{ (Request::is('exchange') ? ' active ' : '') }}">
                <a class="nav-link" href="{{ route('exchange') }}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>อัตราแลกเปลี่ยน</span>
                </a>
            </li>
            <li class="nav-item {{ (Request::is('banks') ? ' active ' : '') }}">
                <a class="nav-link" href="{{ route('banks') }}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>บัญชีธนาคาร</span>
                </a>
            </li>
            <li class="nav-item {{ (Request::is('articles') ? ' active ' : '') }}">
                <a class="nav-link" href="{{ route('articles') }}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>บทความ</span>
                </a>
            </li>
            <li class="nav-item {{ (Request::is('shipping') ? ' active ' : '') }}">
                <a class="nav-link" href="{{ route('shipping') }}">
                    <i class="fas fa-fw fa-tachometer-alt"></i>
                    <span>ขนส่ง</span>
                </a>
            </li>
            @endif
        </ul>
        <!-- End of Sidebar -->


        <div id="content-wrapper" class="d-flex flex-column">
            <div id="content">

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <!-- d-md-none  -->
                    <button id="sidebarToggleTop" class="btn btn-link rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    อัตราแลกเปลี่ยน 1 หยวน ~ <span id="thb" class="ml-1 mr-1"></span>บาท
                    <small class="ml-2">(<span id="lastupd"></span>)</small>

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <div class="topbar-divider d-none d-sm-block"></div>

                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small">
                                    Hi,
                                    @if(isset(Auth::user()->name ))
                                    {{ Auth::user()->name }}
                                    @else
                                    NULL
                                    @endif
                                </span>
                                <img class="img-profile rounded-circle" src="{{ asset('img/undraw_profile.svg') }}">
                            </a>
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                                <!-- <a class="dropdown-item" href="#">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Profile
                                </a>
                                <div class="dropdown-divider"></div> -->
                                <a class="dropdown-item" href="{{ route('logout-admin') }}">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Logout
                                </a>
                            </div>
                        </li>

                    </ul>

                </nav>
                <!-- End of Topbar -->

                <!-- MODAL -->
                <!-- New Order Modal-->
                <div class="modal fade" id="newOrderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="newOrderModalLabel">รายการคำสั่งซื้อใหม่</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body" id="newOrderModalData">
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">ยกเลิก</button>
                                <a class="btn btn-primary" href="#" onclick="saveBookingOrder()">ยืนยัน</a>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- View Order Modal-->
                <div class="modal fade" id="viewOrderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-xl" role="document">
                        <div class="modal-content">
                            <div class="modal-body" id="viewOrderModalData">
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">ปิด</button>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Cancel Order Modal-->
                <div class="modal fade" id="cancelOrderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="cancelOrderModalLabel">กรุณากรอกเหตุผลที่ยกเลิกคำสั่งซื้อนี้</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body" id="cancelOrderModalData">
                                <label for="deleteRemark"></label>
                                <input class="form-control form-control-solid" id="deleteRemark" type="text" placeholder="กรุณากรอกเหตุผล" />

                                <input type="hidden" id="deleteID" />
                            </div>
                            <div class="modal-footer">
                                <button class="btn btn-secondary" type="button" data-dismiss="modal">ยกเลิก</button>
                                <a class="btn btn-primary" onclick="saveCancelOrder()">ยืนยัน</a>
                            </div>
                        </div>
                    </div>
                </div>


                <!-- Cancel Order Modal-->
                <div class="modal fade" id="updateOrderModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="updateOrderModalLabel">อัพเดทสถานะคำสั่งซื้อ</h5>
                                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body" id="updateOrderModalData">
                                <div style="display:none" class="row" id="order_cn">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <strong>เลขที่คำสั่งซื้อ (ร้านค้า)</strong>
                                            <input type="text" name="OrderNumber" id="OrderNumber" class="form-control" placeholder="กรอกเลขคำสั่งซื้อ" value="" maxlength="100">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <strong>วันที่สั่งซื้อ (ร้านค้า)</strong>
                                            <input type="date" name="OrderDate" id="OrderDate" class="form-control" value="" maxlength="10">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <strong>บัญชีแอดมิน</strong>
                                            <input type="text" name="AdminAccount" id="AdminAccount" class="form-control" placeholder="กรอกบัญชีแอดมิน" value="" maxlength="50">
                                        </div>
                                    </div>
                                </div>

                                <div style="display:none" class="row" id="trans_to_thai">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <strong>วันที่ของถึงโกดังจีน</strong>
                                            <input type="date" name="WarehouseCNDate" id="WarehouseCNDate" class="form-control" value="" maxlength="10" placeholder="กรอกวันที่ถึงโกดังจีน">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <strong>วันที่ส่งออกจากโกดังจีน</strong>
                                            <input type="date" name="ExportDate" id="ExportDate" class="form-control" value="" maxlength="10" placeholder="กรอกวันที่ส่งออก">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <strong>เลขที่บิลใบสั่งซื้อ (PO)</strong>
                                            <input type="text" name="PurchaseOrder" id="PurchaseOrder" class="form-control" placeholder="กรอกเลขบิลใบสั่งซื้อ" value="" maxlength="20">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <strong>ตู้ที่</strong>
                                            <input type="text" name="Container" id="Container" class="form-control" placeholder="กรอกตู้ที่" value="" maxlength="50">
                                        </div>
                                    </div>
                                    <hr />
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <strong>ความกว้าง (CM)</strong>
                                            <input type="number" step="0.01" min="0" onchange="calCBM()" name="Width" id="Width" class="form-control" value="" placeholder="กรอกความกว้าง">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <strong>ความยาว (CM)</strong>
                                            <input type="number" step="0.01" min="0" onchange="calCBM()" name="Length" id="Length" class="form-control" value="" placeholder="กรอกความยาว">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <strong>ความสูง (CM)</strong>
                                            <input type="number" step="0.01" min="0" onchange="calCBM()" name="Height" id="Height" class="form-control" value="" placeholder="กรอกความสูง">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <strong>จำนวนกล่องพัสดุ</strong>
                                            <input type="number" onchange="calCBM()" id="TotalBox" name="TotalBox" class="form-control" value="" min="1" placeholder="กรอกจำนวนกล่องพัสดุ">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <strong>ปริมาตร (CBM)</strong>
                                            <input type="number" step="0.0001" min="0" name="Queue" id="Queue" class="form-control" value="" placeholder="กรอกปริมาตร">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <strong>น้ำหนัก (KG)</strong>
                                            <input type="number" step="0.01" min="0" name="KG" id="KG" class="form-control" value="" placeholder="กรอกน้ำหนัก">
                                        </div>
                                    </div>
                                </div>

                                <div style="display:none" class="row" id="thai_received">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <strong>วันที่ของถึงโกดังไทย</strong>
                                            <input type="date" name="ImportDate" id="ImportDate" class="form-control" value="" maxlength="10" placeholder="กรอกวันที่นำเข้า">
                                        </div>
                                    </div>
                                </div>

                                <div style="display:none" class="row" id="delivered">
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <strong>วันที่จัดส่งหาลูกค้า</strong>
                                            <input type="date" name="DeliveryDate" id="DeliveryDate" class="form-control" value="" maxlength="10" placeholder="กรอกวันที่จัดส่ง">
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group" id="divtrans">

                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group">
                                            <strong>เลขที่พัสดุจัดส่ง</strong>
                                            <input type="text" name="TrackingNO" id="TrackingNO" class="form-control" placeholder="กรอกเลขพัสดุ" value="" maxlength="50">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <input type="hidden" id="update_order_id" />
                                <input type="hidden" id="update_status_id" />
                                <input type="hidden" id="update_status" />
                                <input type="hidden" id="update_status_type" />
                                <button class="btn btn-secondary" type="button" onclick="closeUpdateStatus()">ยกเลิก</button>
                                <a class="btn btn-primary" onclick="prepareUpdateStatus()">ยืนยัน</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- END MODAL -->

                <div class="container-fluid">
                    @yield('content')
                </div>
            </div>
        </div>
    </div>

    <script src="{{ asset('js/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('js/jquery/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
    <script src="{{ asset('js/chart/Chart.min.js') }}"></script>

    <!-- dataTables -->
    <script src="{{ asset('js/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('js/datatables/dataTables.bootstrap4.min.js') }}"></script>

    <!-- select2 -->
    <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>

    <!-- litepicker -->
    <script src="https://cdn.jsdelivr.net/npm/litepicker/dist/litepicker.js"></script>

    @yield('javascript')
    <script>
        $(document).ready(function() {
            $.ajax({
                method: "GET",
                url: "/exchange-rate",
            }).done(function(data) {
                $("#thb").html(data.THRate);
                $("#lastupd").html(data.LastUpd);
            });
        });

        function viewOrder(order_id) {
            $('#viewOrderModal').modal('show');
            $.ajax({
                method: "GET",
                url: `/orders-view/${order_id}`,
            }).done(function(data) {
                $('#viewOrderModalData').html(data)
            });

        }

        function calCBM() {
            let Width = $('#Width').val();
            let Length = $('#Length').val();
            let Height = $('#Height').val();
            let TotalBox = $('#TotalBox').val();

            let cbm = (Width * Length * Height * TotalBox) / 1000000
            if (cbm < 0.0001) {
                cbm = 0.0001
            } else {
                cbm = cbm.toFixed(4)
            }
            // alert(cbm)
            $('#Queue').val(cbm);

        }
    </script>
</body>

</html>