@extends('layouts.app')
@section('title')
<title>ตั้งค่าบัญชีธนาคาร</title>
@endsection

@section('content')
<div class="card">
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h1 class="h3 mb-0 text-gray-800">{{ ($StaPage == 'create') ? 'เพิ่ม' : 'แก้ไข' }}บัญชีธนาคาร</h1>
                    <a href="{{ route('banks') }}" class="btn btn-secondary">กลับ</a>
                </div>
            </div>
            <div class="card-body">
                @if($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoop!</strong>
                    There were some problems with your input. <br><br>
                    <ul>
                        @foreach(@errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form action="{{ ($StaPage == 'create') ? route('banks-store') : route('banks-update',$data[0]->acc_id) }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-lg-4">
                            <div class="form-group">
                                <strong>ชื่อบัญชี</strong>
                                <input type="text" name="acc_name" class="form-control" value="{{ ($StaPage == 'edit') ? $data[0]->acc_name : '' }}" placeholder="กรอกชื่อบัญชี" required>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <strong>เลขที่บัญชี</strong>
                                <input type="text" name="acc_no" class="form-control" value="{{ ($StaPage == 'edit') ? $data[0]->acc_no : '' }}" placeholder="กรอกเลขที่บัญชี" required>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="form-group">
                                <strong>ธนาคาร</strong>
                                <select name="acc_bank" class="form-control" required>
                                    <option value=""> - เลือก -</option>
                                    @foreach($arr_bank as $each)
                                    <option @if($StaPage=='edit' ) {{ ($data[0]->acc_bank == $each) ? "selected" : "" }} @endif value="{{ $each }}">{{ $each }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-success">บันทึก</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')

<script>

</script>

@endsection