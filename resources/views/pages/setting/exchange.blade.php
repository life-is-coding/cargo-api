@extends('layouts.app')
@section('title')
<title>ตั้งค่าอัตราแลกเปลี่ยน</title>
@endsection

@section('content')
@if($message = Session::get('success'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {{ $message }}
</div>
@endif
<div class="card">
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h1 class="h3 mb-0 text-gray-800">อัตราแลกเปลี่ยน</h1>
                </div>
            </div>
            <div class="card-body">
                @if($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoop!</strong>
                    There were some problems with your input. <br><br>
                    <ul>
                        @foreach(@errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form action="{{ route('exchange-update',$data[0]->ID) }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>หยวนจีน</strong>
                                <input type="number" name="CNRate" class="form-control" value="{{ $data[0]->CNRate }}" step="0.01" min="1" max="1" readonly>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>บาทไทย</strong>
                                <input type="number" name="THRate" class="form-control" value="{{ $data[0]->THRate }}" step="0.01" min="1" max="1000" placeholder="กรอกบาทไทย" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary">บันทึก</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')

<script>

</script>

@endsection