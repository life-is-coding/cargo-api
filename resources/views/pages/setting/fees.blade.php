@extends('layouts.app')

@section('title')
<title>ตั้งค่าค่าธรรมเนียมขนส่ง</title>
@endsection
@section('content')
@if($message = Session::get('success'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {{ $message }}
</div>
@endif

<div class="card">
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h1 class="h3 mb-0 text-gray-800">ค่าธรรมเนียมขนส่ง</h1>
                    <a href="{{ route('fees-create') }}" class="btn btn-success">เพิ่ม</a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-bordered table-hover" id="dataTable">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">ประเภทลูกค้า</th>
                            <th scope="col">ประเภทขนส่ง</th>
                            <th scope="col">ประเภทสินค้า</th>
                            <th scope="col">กิโลกรัม</th>
                            <th scope="col">คิว</th>
                            <th scope="col">สถานะ</th>
                            <th scope="col">จัดการ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $key => $value)
                        <tr>
                            <th scope="row">{{ $key + 1 }}</th>
                            <td>{{ $value->cst_type_name }}</td>
                            <td>{{ $value->TransName }}</td>
                            <td>{{ $value->ProdName }}</td>
                            <td>{{ $value->KG }}</td>
                            <td>{{ $value->CBM }}</td>
                            <td>{{ $value->WeightStatus == '1' ? 'ใช้งาน' : 'ไม่ใช้งาน' }}</td>
                            <td>
                                <a href="{{ route('fees-edit',$value->WeightID) }}" class="btn btn-sm btn-info">แก้ไข</a>
                                <a href="{{ route('fees-destroy',$value->WeightID) }}" class="ml-2 btn btn-sm btn-danger">ลบ</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')

<script>
    $(document).ready(function() {
        $('#dataTable').DataTable();
    });
</script>

@endsection