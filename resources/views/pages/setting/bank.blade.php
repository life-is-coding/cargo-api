@extends('layouts.app')

@section('title')
<title>ตั้งค่าบัญชีธนาคาร</title>
@endsection
@section('content')
@if($message = Session::get('success'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {{ $message }}
</div>
@endif

<div class="card">
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h1 class="h3 mb-0 text-gray-800">บัญชีธนาคาร</h1>
                    <a href="{{ route('banks-create') }}" class="btn btn-success">เพิ่ม</a>
                </div>
            </div>
            <div class="card-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-6">
                            <div class="row">
                                <div class="col-8">
                                    บัญชีที่ใช้สำหรับการโอนเงินค่าสินค้า
                                </div>
                                <div class="col-4">
                                    <a href="#" class="btn btn-warning btn-sm" onclick="saveAccount('item')">บันทึก</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <select name="acc_bank_item" id="acc_bank_item" class="form-control" required>
                                        <option value=""> - เลือก -</option>
                                        @foreach($data as $key => $value)
                                        <option @if($value->acc_type=='item') {{ 'selected' }} @endif value="{{ $value->acc_id }}">{{ $value->acc_no." ".$value->acc_bank." ".$value->acc_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="row">
                                <div class="col-8">
                                    บัญชีที่ใช้สำหรับการโอนเงินค่าจัดส่งสินค้า
                                </div>
                                <div class="col-4">
                                    <a href="#" class="btn btn-warning btn-sm" onclick="saveAccount('trans')">บันทึก</a>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-12">
                                    <select name="acc_bank_trans" id="acc_bank_trans" class="form-control" required>
                                        <option value=""> - เลือก -</option>
                                        @foreach($data as $key => $value)
                                        <option @if($value->acc_type=='trans') {{ 'selected' }} @endif value="{{ $value->acc_id }}">{{ $value->acc_no." ".$value->acc_bank." ".$value->acc_name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr />
                    <table class="table table-bordered table-hover" id="dataTable">
                        <thead class="thead-dark">
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">ชื่อบัญชี</th>
                                <th scope="col">เลขที่บัญชี</th>
                                <th scope="col">ธนาคาร</th>
                                <th scope="col">จัดการ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $key => $value)
                            <tr>
                                <th scope="row">{{ $key + 1 }}</th>
                                <td>{{ $value->acc_name }}</td>
                                <td>{{ $value->acc_no }}</td>
                                <td>{{ $value->acc_bank }}</td>
                                <td>
                                    <a href="{{ route('banks-edit',$value->acc_id) }}" class="btn btn-info btn-sm">แก้ไข</a>
                                    <a onclick="delAccount('{{$value->acc_id}}')" class="btn btn-secondary btn-sm">ลบ</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @section('javascript')

    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable();
        });

        function saveAccount(type) {
            acc_val = $(`#acc_bank_${type}`).val()
            if (acc_val != "") {
                $.ajax({
                    method: "POST",
                    url: `/banks-change` + '?_token=' + '{{ csrf_token() }}',
                    data: {
                        type: type,
                        acc_id: acc_val
                    }
                }).done(function(data) {
                    window.location.reload();
                });
            }
        }

        function delAccount(acc_id) {
            if (confirm('Delete bank account ? ')) {
                $.ajax({
                    method: "GET",
                    url: `/banks-destroy/${acc_id}` + '?_token=' + '{{ csrf_token() }}',
                }).done(function(data) {
                    window.location.reload();
                });
            }
        }
    </script>

    @endsection