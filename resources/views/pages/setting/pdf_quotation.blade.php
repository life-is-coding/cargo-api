<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>ใบเสนอราคา / Quotation {{ $data[0]->No }}</title>
    <style>
        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: normal;
            src: url("{{ asset('/fonts/THSarabunNew.ttf') }}") format("truetype");
        }

        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: normal;
            src: url("{{ asset('/fonts/THSarabunNew Italic.ttf') }}") format("truetype");
        }

        @font-face {
            font-family: 'THSarabunNew';
            font-style: italic;
            font-weight: bold;
            src: url("{{ asset('/fonts/THSarabunNew BoldItalic.ttf') }}") format("truetype");
        }

        @font-face {
            font-family: 'THSarabunNew';
            font-style: normal;
            font-weight: bold;
            src: url("{{ asset('/fonts/THSarabunNew Bold.ttf') }}") format("truetype");
        }

        /* @font-face {
            font-family: 'Sarabun';
            font-style: normal;
            font-weight: 300;
            font-display: swap;
            src: url(https://fonts.gstatic.com/s/sarabun/v8/DtVmJx26TKEr37c9YL5rik8s6zDX.woff2) format('woff2');
            unicode-range: U+0E01-0E5B, U+200C-200D, U+25CC;
        } */
        body {
            font-family: 'THSarabunNew', serif;
            margin: 5px;
            line-height: 82%;
        }

        .itempic {
            width: 70px;
            height: 70px;
            max-width: 70px;
            max-height: 70px;
        }

        .label1 {
            font-size: 16px;
        }

        .label2 {
            font-size: 16px;
            font-weight: bold;
        }

        .label3 {
            font-size: 14px;
        }

        .label4 {
            font-size: 18px;
        }


        .paddingcontent {
            padding-left: 10px;
            padding-right: 10px;
        }

        #logo {
            /* font-size: 110%; */
            font-family: 'Cambria', 'Cochin', 'Georgia', 'Times', 'Times New Roman', 'serif';
            color: #b50000;
        }

        @page {
            margin: 5px;
        }

        .page {
            page-break-after: always;
        }

        #itemdetail,
        #itemdetail th,
        #itemdetail td {
            border: 1px solid #ccc;
            border-collapse: collapse;
        }
    </style>
</head>

<body>
    @php
    $per_page = 8;
    $total_items = count($detail);
    if($qu->Delivery != ''){
    $arr_del = explode(',', $qu->Delivery);
    $total_items = count($arr_del) * 0.5;
    }
    $total_page = ceil($total_items/$per_page);
    $row_items = 0;

    $total_price_item = 0;
    @endphp

    @for( $no = 1 ; $no <= $total_page ; $no++) <div class="{{ ($total_page==$no ) ? '' : 'page' }}">
        <table width="100%" cellspacing="0" cellpadding="0">
            <tr bgcolor="#1155cc">
                <td width="10%">&nbsp;</td>
                <td width="25%">&nbsp;</td>
                <td width="6%">&nbsp;</td>
                <td width="12%">&nbsp;</td>
                <td width="12%">&nbsp;</td>
                <td width="14%">&nbsp;</td>
                <td width="10%">&nbsp;</td>
                <td width="11%">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="4" align="left">
                    <h1 id="logo">Banana The Cargo</h1>
                </td>
                <td colspan="4" align="right">
                    <h1>ใบเสนอราคา / Quotation</h1>
                </td>
            </tr>
            <tr>
                <td class="label2">เลขที่คำสั่งซื้อ</td>
                <td class="label1">{{ $data[0]->No }}</td>
                <td colspan="5"></td>
                <td align="right">หน้า {{$no}} / {{$total_page}}</td>
            </tr>
            <tr>
                <td class="label2">รหัสลูกค้า</td>
                <td class="label1">{{ $data[0]->CstID }} ({{$data[0]->cst_type_name}})</td>
                <td class="label2">ชื่อบริษัท</td>
                <td class="label1" style="vertical-align:top" colspan="3" rowspan="2">{{ $data[0]->CstCompanyName }}</td>
                <td class="label2" align="right">วันที่</td>
                <td class="label1" align="right">{{ date("d/m/Y") }}</td>
            </tr>
            <tr>
                <td class="label2">ชื่อลูกค้า</td>
                <td class="label1">{{ $data[0]->CstFirstname.' '.$data[0]->CstLastname }}</td>
                <td class="label2"></td>
                <td class="label2"></td>
                <td class="label2"></td>
            </tr>
            <tr>
                <td class="label2">อีเมล์</td>
                <td class="label1">{{ $data[0]->email }}</td>
                <td class="label2" style="vertical-align:top" rowspan="3">โทรศัพท์</td>
                <td class="label1" style="vertical-align:top" rowspan="3">{{ $data[0]->CstTel }}</td>
                <td class="label2">ประเภทการขนส่ง</td>
                <td class="label1">{{ $data[0]->TransName }}</td>
                <td class="label2"></td>
                <td class="label2"></td>
            </tr>
            <tr>
                <td class="label2">ที่อยู่</td>
                <td class="label1" rowspan="2" style="vertical-align:top">{{ $data[0]->CstAddress.' '.$data[0]->CstDistrict.' '.$data[0]->CstSubdistrict.' '.$data[0]->CstProvice.' '.$data[0]->CstZipcode }}</td>
                <td class="label2">อัตราแลกเปลี่ยน</td>
                <td class="label1">1 หยวน = {{ $data[0]->ExchangeRate }} บาท</td>
                <td class="label2"></td>
                <td class="label2"></td>
            </tr>
            <tr>
                <td class="label2"></td>
                <td class="label2">ประเภทสินค้า</td>
                <td class="label1">{{ $data[0]->ProdName }}</td>
                <td class="label2"></td>
                <td class="label2"></td>
            </tr>
        </table>
        @if($qu->Product=='Y')
        <table id="itemdetail" width="100%" cellspacing="0" cellpadding="0">
            <tr bgcolor="#c9daf8">
                <td align="center" class="label2" width="3%">#</td>
                <td align="center" class="label2" width="10%">รูป</td>
                <td align="center" class="label2" width="39%">รายละเอียดสินค้า</td>
                <td align="center" class="label2" width="12%">ราคา/ชิ้น<br />(หยวน)</td>
                <td align="center" class="label2" width="12%">จำนวน</td>
                <td align="center" class="label2" width="12%">ราคารวม<br />(หยวน)</td>
                <td align="center" class="label2" width="12%">ราคารวม<br />(บาท)</td>
            </tr>
            @php
            $total_price_item = $data[0]->TotalAllTHB;
            $QtyAll = 0;
            $row_items = 0;
            $start = ($no * $per_page)-$per_page;
            $stop = ($no * $per_page)-1;
            if($stop>$total_items){
            $stop = $total_items-1;
            }
            @endphp
            @for($item = $start ; $item <= $stop ; $item++) @php $QtyAll +=$detail[$item]->Qty;
                $row_items ++;
                @endphp
                <tr>
                    <td height="70px" align="center" class="label1">{{ $item + 1 }}</td>
                    <td align="center">
                        @if($detail[$item]->Pic != '')
                        <img src="{{ (strpos($detail[$item]->Pic,'https')!='') ? $detail[$item]->Pic : 'https:'.$detail[$item]->Pic }}" class="itempic"><br>
                        @endif
                    </td>
                    <td style="word-wrap:break-word;" class="paddingcontent">
                        <span class="label2">{{ $detail[$item]->ItemName }} </span><br>
                        <span class="label1">{{ $detail[$item]->Detail }} </span><br>
                        <span class="label3"> Link : {{ $detail[$item]->Link }} </span>
                    </td>
                    <td align="right" class="label1 paddingcontent">{{ number_format($detail[$item]->Price,2) }}</td>
                    <td align="center" class="label1 paddingcontent">{{ $detail[$item]->Qty }}</td>
                    <td align="right" class="label1 paddingcontent">{{ number_format($detail[$item]->TotalLine,2) }}</td>
                    <td align="right" class="label1 paddingcontent">{{ number_format($detail[$item]->TotalLineTHB,2) }}</td>
                </tr>
                @endfor

                <!-- @if ($row_items<$per_page) @for( $no_item=$row_items ; $no_item <=$per_page ;$no_item++) <tr>
                    <td height="70px"></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>
                    @endfor
                    @endif -->
                <!-- @if ($no == $total_page)
                    <tr bgcolor="#c9daf8">
                        <td colspan="4" class="label2 paddingcontent" align="right">&nbsp;สรุปราคาสินค้า</td>
                        <td align="center" class="label2 paddingcontent">{{ $QtyAll }}</td>
                        <td align="right" class="label2 paddingcontent">{{ number_format($data[0]->TotalAllCNY,2) }}</td>
                        <td align="right" class="label2 paddingcontent"><u>{{ number_format($data[0]->TotalAllTHB,2) }}</u></td>
                    </tr>
                    @endif -->
        </table>
        @endif
        @php
        $sum_delivery = 0;
        @endphp
        @if($qu->Delivery != '')
        <table id="itemdetail" style="margin-top:5px" width="100%" cellspacing="0" cellpadding="0">
            <tr bgcolor="#c9daf8">
                <td align="center" class="label2" width="50%">รายละเอียดการจัดส่ง</td>
                <td align="center" class="label2" width="10%">ค่าขนส่งจีน-จีน</td>
                <td align="center" class="label2" width="10%">ค่าขนส่งจีน-ไทย</td>
                <td align="center" class="label2" width="10%">ค่าขนส่งไทย-ไทย</td>
                <td align="center" class="label2" width="10%">รวมค่าขนส่ง</td>
            </tr>
            @foreach($delivery as $key => $resDlv)

            @php
            $sum_each = 0;
            $sum_each = $resDlv->DeliveryTHTH + $resDlv->DeliveryCNTH + $resDlv->DeliveryCNCN;
            $sum_delivery += $sum_each;
            @endphp

            <tr>

                <td class="label1 paddingcontent">
                    เลขที่จัดส่ง (ร้านค้า) : {{ $resDlv->OrderTracking }} | จำนวนกล่อง : {{ $resDlv->TotalBox }} กล่อง<br />
                    น้ำหนัก {{ $resDlv->KG }} กิโลกรัม | ขนาด {{ $resDlv->Width }} x {{ $resDlv->Length }} x {{ $resDlv->Height }} (กว้าง x ยาว x สูง)<br />
                    จัดส่งสินค้าในไทยโดย {{ $resDlv->CompanyName }} | เลขที่ติดตามพัสดุจัดส่ง {{ $resDlv->TrackingNO }}
                </td>
                <td class="label1 paddingcontent" align="right">{{ ($resDlv->DeliveryCNCN > 0) ? number_format($resDlv->DeliveryCNCN,2) : ""  }}</td>
                <td class="label1 paddingcontent" align="right">{{ ($resDlv->DeliveryCNTH > 0) ? number_format($resDlv->DeliveryCNTH,2) : "" }}</td>
                <td class="label1 paddingcontent" align="right">{{ ($resDlv->DeliveryTHTH > 0) ? number_format($resDlv->DeliveryTHTH,2) : ""  }}</td>
                <td class="label1 paddingcontent" align="right">{{ number_format($sum_each,2) }}</td>
            </tr>
            @endforeach
        </table>
        @endif
        <table style="margin-top:5px; " width="100%" cellspacing="0" cellpadding="0">
            <tr>
                <td width="60%">&nbsp;</td>
                <td style="border: 1px solid #ccc;border-collapse: collapse;" width="20%" bgcolor="#c9daf8" class="label2 paddingcontent" align="right">&nbsp;ราคาค่าสินค้า</td>
                <td style="border: 1px solid #ccc;border-collapse: collapse;" width="20%" align="right" class="label2 paddingcontent"><u>{{ number_format($total_price_item,2) }}</u> บาท</td>
            </tr>
            <tr>
                <td width="60%">&nbsp;</td>
                <td style="border: 1px solid #ccc;border-collapse: collapse;" bgcolor="#c9daf8" class="label2 paddingcontent" align="right">&nbsp;ค่าจัดส่ง</td>
                <td style="border: 1px solid #ccc;border-collapse: collapse;" align="right" class="label2 paddingcontent"><u>{{ number_format($sum_delivery,2) }}</u> บาท</td>
            </tr>
            <tr>
                <td width="60%">&nbsp;</td>
                <td style="border: 1px solid #ccc;border-collapse: collapse;" bgcolor="#c9daf8" class="label2 paddingcontent" align="right">&nbsp;รวม</td>
                <td style="border: 1px solid #ccc;border-collapse: collapse;" align="right" class="label2 paddingcontent"><u>{{ number_format($total_price_item + $sum_delivery,2) }}</u> บาท</td>
            </tr>
        </table>
        <table style="margin-top:5px" width="100%" cellspacing="0" cellpadding="0">
            <tr>
                <td width="10%" valign="top">
                    <img class="itempic" src="{{ public_path('/images/qr_line_confirm.jpg') }}" />
                </td>
                <td width="40%" class="paddingcontent" valign="top">
                    <b><u>เงื่อนไขการชำระเงิน </u></b> <br />
                    - กรุณาโอนชำระเงินภายในวันที่ได้รับใบเสนอราคา เนื่องจากอัตราค่าแลกเปลี่ยนอาจมีการเปลี่ยนแปลง หรือ ติดต่อแอดมินก่อนทำการโอนชำระค่าสินค้า<br />
                    - ลูกค้าสามารถแจ้งชำระเงินได้ที่หน้าเว็บไซท์ หรือบัญชี line@ โดยสามารถ Scan จาก QR Code <br />
                </td>
                <td class="label4" width="40%" valign="top">
                    @if(isset($bank_item->acc_bank))
                    <b><u>เลขที่บัญชีสำหรับการโอนชำระค่าสินค้า</u></b> <br />
                    <b>ธนาคาร : </b>{{ $bank_item->acc_bank}} | <b>เลขที่บัญชี : </b>{{ $bank_item->acc_no}}<br />
                    <b>ชื่อบัญชี : </b>{{ $bank_item->acc_name}} <br /><br />
                    @endif
                    @if(isset($bank_trans->acc_bank))
                    <b><u>เลขที่บัญชีสำหรับการโอนชำระค่าจัดส่งสินค้า</u></b> <br />
                    <b>ธนาคาร : </b>{{ $bank_trans->acc_bank}} | <b>เลขที่บัญชี : </b>{{ $bank_trans->acc_no}}<br />
                    <b>ชื่อบัญชี : </b>{{ $bank_trans->acc_name}} <br />
                    @endif
                </td>
        </table>
        </div>
        @endfor
</body>

</html>