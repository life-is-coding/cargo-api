@extends('layouts.app')
@section('title')
<title>ตั้งค่าบทความ</title>
@endsection

@section('content')
<div class="card">
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h1 class="h3 mb-0 text-gray-800">{{ ($StaPage == 'create') ? 'เพิ่ม' : 'แก้ไข' }}บทความ</h1>
                    <a href="{{ route('articles') }}" class="btn btn-secondary">กลับ</a>
                </div>
            </div>
            <div class="card-body">
                @if($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoop!</strong>
                    There were some problems with your input. <br><br>
                    <ul>
                        @foreach(@errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form action="{{ ($StaPage == 'create') ? route('articles-store') : route('articles-update',$data[0]->article_id) }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="row">
                                <div class="col-lg-10">
                                    <div class="form-group">
                                        <strong>หัวข้อ</strong>
                                        <input type="text" name="article_title" class="form-control" value="{{ ($StaPage == 'edit') ? $data[0]->article_title : '' }}" placeholder="กรอกหัวข้อ" required>
                                    </div>
                                </div>
                                <div class="col-lg-2">
                                    <div class="form-group">
                                        <strong>สถานะ</strong><br />
                                        <input type="checkbox" value="1" @if($StaPage=='edit' ) {{ ($data[0]->article_status == '1') ? 'checked' : '' }} @else checked @endif id="article_status" name="article_status">
                                        <label for="article_status">ใช้งาน</label>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <strong>รายละเอียด</strong>
                                        <textarea rows="3" name="article_subtitle" class="form-control" placeholder="กรอกรายละเอียด" required>@if($StaPage == 'edit'){{$data[0]->article_subtitle}}@endif</textarea>
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <strong>Link</strong>
                                        <input type="text" name="article_link" class="form-control" value="{{ ($StaPage == 'edit') ? $data[0]->article_link : '' }}" placeholder="กรอก Link">
                                    </div>
                                </div>
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <strong>รูปบทความ</strong>
                                        <input type="file" id="article_picture" name="article_picture" accept="image/jpg, image/jpeg, image/png" class="form-control" />
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <button type="submit" class="btn btn-success">บันทึก</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            @if($StaPage == 'edit')
                            <img src="{{ $data[0]->article_picture }}" width="80%" />
                            @endif
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')

<script>

</script>

@endsection