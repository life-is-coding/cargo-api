@extends('layouts.app')

@section('title')
<title>ตั้งค่าประเภทสินค้า</title>
@endsection
@section('content')
@if($message = Session::get('success'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {{ $message }}
</div>
@endif

<div class="card">
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h1 class="h3 mb-0 text-gray-800">ประเภทสินค้า</h1>
                    <a href="{{ route('products-create') }}" class="btn btn-success">เพิ่ม</a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-bordered table-hover" id="dataTable">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col" width="5%">#</th>
                            <th scope="col" width="75%">ประเภทสินค้า</th>
                            <th scope="col" width="10%">สถานะ</th>
                            <th scope="col" width="10%">จัดการ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $key => $value)
                        <tr>
                            <th scope="row">{{ $key + 1 }}</th>
                            <td>{{ $value->ProdName }}</td>
                            <td>{{ $value->ProdStatus == '1' ? 'ใช้งาน' : 'ไม่ใช้งาน' }}</td>
                            <td>
                                <a href="{{ route('products-edit',$value->ProdID) }}" class="btn btn-info">แก้ไข</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')

<script>
    $(document).ready(function() {
        $('#dataTable').DataTable();
    });
</script>

@endsection