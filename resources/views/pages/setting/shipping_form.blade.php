@extends('layouts.app')
@section('title')
<title>ตั้งค่าขนส่ง</title>
@endsection

@section('content')
<div class="card">
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h1 class="h3 mb-0 text-gray-800">{{ ($StaPage == 'create') ? 'เพิ่ม' : 'แก้ไข' }}ขนส่ง</h1>
                    <a href="{{ route('shipping') }}" class="btn btn-primary">กลับ</a>
                </div>
            </div>
            <div class="card-body">
                @if($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoop!</strong>
                    There were some problems with your input. <br><br>
                    <ul>
                        @foreach(@errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form action="{{ ($StaPage == 'create') ? route('shipping-store') : route('shipping-update',$data[0]->ID) }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>ชื่อขนส่ง</strong>
                                <input type="text" name="CompanyName" class="form-control" placeholder="กรอกชื่อ" value="{{ ($StaPage == 'edit') ? $data[0]->CompanyName : '' }}" maxlength="50" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3 pt-4">
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" value="Y" class="custom-control-input" @if($StaPage=='edit' ) {{ ($data[0]->Status == '1') ? 'checked' : '' }} @else checked @endif id="Status" name="Status">
                                    <label class="custom-control-label" for="Status">ใช้งาน</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary">บันทึก</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')

<script>

</script>

@endsection