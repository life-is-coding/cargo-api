@extends('layouts.app')
@section('title')
<title>ตั้งค่าผู้ใช้งาน (แอดมิน)</title>
@endsection
@section('content')
@if($message = Session::get('success'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {{ $message }}
</div>
@endif
<div class="card">
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h1 class="h3 mb-0 text-gray-800">ผู้ใช้งาน (แอดมิน)</h1>
                    <a href="{{ route('users-create') }}" class="btn btn-success">เพิ่ม</a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">ชื่อผู้ใช้</th>
                            <th scope="col">อีเมล์</th>
                            <th scope="col">สิทธิ์</th>
                            <th scope="col">สถานะ</th>
                            <th scope="col">จัดการ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $key => $value)
                        <tr>
                            <th scope="row">{{ $key + 1 }}</th>
                            <td>{{ $value->name }}</td>
                            <td>{{ $value->email }}</td>
                            <td>{{ $value->role }}</td>
                            <td>{{ $value->user_status == '2' ? 'ใช้งาน' : 'ไม่ใช้งาน' }}</td>
                            <td>
                                <a href="{{ route('users-edit',$value->id) }}" class="btn btn-info">แก้ไข</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

</div>
@endsection

@section('javascript')
<script>
    $(document).ready(function() {
        $('#dataTable').DataTable();
    });
</script>

@endsection