@extends('layouts.app')
@section('title')
<title>ตั้งค่าผู้ใช้งาน (แอดมิน)</title>
@endsection

@section('content')
<div class="card">
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h1 class="h3 mb-0 text-gray-800">{{ ($StaPage == 'create') ? 'เพิ่ม' : 'แก้ไข' }}ผู้ใช้</h1>
                    <a href="{{ route('users') }}" class="btn btn-primary">กลับ</a>
                </div>
            </div>
            <div class="card-body">
                @if($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoop!</strong>
                    There were some problems with your input. <br><br>
                    <ul>
                        @foreach(@errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form action="{{ ($StaPage == 'create') ? route('users-store') : route('users-update',$data[0]->id) }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>ชื่อผู้ใช้</strong>
                                <input type="text" name="name" class="form-control" value="{{ ($StaPage == 'edit') ? $data[0]->name : '' }}" placeholder="กรอกชื่อผู้ใช้" maxlength="25" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>อีเมล์</strong>
                                <input type="email" name="email" class="form-control" value="{{ ($StaPage == 'edit') ? $data[0]->email : '' }}" placeholder="กรอกอีเมล์" maxlength="50" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>รหัสผ่าน</strong>
                                <input type="password" name="password" {{ ($StaPage == 'create') ? 'required' : '' }} class="form-control" value="" placeholder="กรอกรหัสผ่าน" maxlength="15">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>ประเภท</strong>
                                <select name="role" class="form-control" required>
                                    <option value=""> - เลือก -</option>
                                    <option @if($StaPage=='edit' ){{ ($data[0]->role === 'admin') ? 'selected' : '' }} @endif value="admin">Admin</option>
                                    <option @if($StaPage=='edit' ){{ ($data[0]->role === 'superadmin') ? 'selected' : '' }} @endif value="superadmin">Super Admin</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" value="Y" class="custom-control-input" @if($StaPage=='edit' ) {{ ($data[0]->user_status == '2') ? 'checked' : '' }} @else checked @endif id="user_status" name="user_status">
                                    <label class="custom-control-label" for="user_status">ใช้งาน</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <button type="submit" class="btn btn-primary">บันทึก</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')

<script>

</script>

@endsection