@extends('layouts.app')
@section('title')
<title>ตั้งค่าประเภทสินค้า</title>
@endsection

@section('content')
<div class="card">
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h1 class="h3 mb-0 text-gray-800">{{ ($StaPage == 'create') ? 'เพิ่ม' : 'แก้ไข' }}ประเภทสินค้า</h1>
                    <a href="{{ route('products') }}" class="btn btn-primary">กลับ</a>
                </div>
            </div>
            <div class="card-body">
                @if($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoop!</strong>
                    There were some problems with your input. <br><br>
                    <ul>
                        @foreach(@errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form action="{{ ($StaPage == 'create') ? route('products-store') : route('products-update',$data[0]->ProdID) }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>ประเภทสินค้า</strong>
                                <input type="text" name="ProdName" class="form-control" value="{{ ($StaPage == 'edit') ? $data[0]->ProdName : '' }}" maxlength="100" placeholder="กรอกประเภทสินค้า" required>

                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" value="Y" class="custom-control-input" @if($StaPage=='edit' ) {{ ($data[0]->ProdStatus == '1') ? 'checked' : '' }} @else checked @endif id="ProdStatus" name="ProdStatus">
                                    <label class="custom-control-label" for="ProdStatus">ใช้งาน</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary">บันทึก</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')

<script>

</script>

@endsection