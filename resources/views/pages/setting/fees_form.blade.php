@extends('layouts.app')
@section('title')
<title>ตั้งค่าค่าธรรมเนียมขนส่ง</title>
@endsection

@section('content')
<div class="card">
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h1 class="h3 mb-0 text-gray-800">{{ ($StaPage == 'create') ? 'เพิ่ม' : 'แก้ไข' }}ค่าธรรมเนียมขนส่ง</h1>
                    <a href="{{ route('fees') }}" class="btn btn-primary">กลับ</a>
                </div>
            </div>
            <div class="card-body">
                @if($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoop!</strong>
                    There were some problems with your input. <br><br>
                    <ul>
                        @foreach(@errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form action="{{ ($StaPage == 'create') ? route('fees-store') : route('fees-update',$data[0]->WeightID) }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>ประเภทลูกค้า</strong>
                                <select name="CstTypeId" class="form-control" required>
                                    <option value=""> - เลือก -</option>
                                    @foreach($cst as $key => $val)
                                    <option @if($StaPage=='edit' ) {{ ($data[0]->CstTypeId == $val->cst_type_id) ? "selected" : "" }} @endif value="{{ $val->cst_type_id }}">{{ $val->cst_type_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>ประเภทขนส่ง</strong>
                                <select name="TransID" class="form-control" required>
                                    <option value=""> - เลือก -</option>
                                    @foreach($trans as $key => $val)
                                    <option @if($StaPage=='edit' ) {{ ($data[0]->TransID == $val->TransID) ? "selected" : "" }} @endif value="{{ $val->TransID }}">{{ $val->TransName }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>ประเภทสินค้า</strong>
                                <select name="ProdID" class="form-control" required>
                                    <option value=""> - เลือก -</option>
                                    @foreach($prod as $key => $val)
                                    <option @if($StaPage=='edit' ){{ ($data[0]->ProdID == $val->ProdID) ? "selected" : "" }} @endif value="{{ $val->ProdID }}">{{ $val->ProdName }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>น้ำหนัก (กิโลกรัม)</strong>
                                <input type="number" name="KG" class="form-control" value="{{ ($StaPage == 'edit') ? $data[0]->KG : '' }}" min="0" placeholder="กรอกกิโลกรัม" required>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>ปริมาตร (CBM)</strong>
                                <input type="number" name="CBM" class="form-control" value="{{ ($StaPage == 'edit') ? $data[0]->CBM : '' }}" min="0" placeholder="กรอกคิว" required>
                            </div>
                        </div>
                        <div class="col-lg-3 pt-4">
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" value="Y" class="custom-control-input" @if($StaPage=='edit' ) {{ ($data[0]->WeightStatus == '1') ? 'checked' : '' }} @else checked @endif id="WeightStatus" name="WeightStatus">
                                    <label class="custom-control-label" for="WeightStatus">ใช้งาน</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary">บันทึก</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')

<script>

</script>

@endsection