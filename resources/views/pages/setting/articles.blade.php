@extends('layouts.app')

@section('title')
<title>ตั้งค่าบทความ</title>
@endsection
@section('content')
@if($message = Session::get('success'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {{ $message }}
</div>
@endif

<div class="card">
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h1 class="h3 mb-0 text-gray-800">บทความ</h1>
                    <a href="{{ route('articles-create') }}" class="btn btn-success">เพิ่ม</a>
                </div>
            </div>
            <div class="card-body">
                <div class="container-fluid">
                    <table class="table table-bordered table-hover" id="dataTable">
                        <thead class="thead-dark">
                            <tr>
                                <th width="5%" scope="col">#</th>
                                <th width="45%" scope="col">รายละเอียดบทความ</th>
                                <th width="30%" scope="col">รูปภาพ</th>
                                <th width="15%" scope="col">สถานะ</th>
                                <th width="5%" scope="col">จัดการ</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($data as $key => $value)
                            <tr>
                                <th scope="row">{{ $key + 1 }}</th>
                                <td>
                                    <b>{{ $value->article_title }}</b><br />
                                    {{ $value->article_subtitle }}<br />
                                    Link : {{ $value->article_link }}
                                </td>
                                <td>
                                    <img src="{{ $value->article_picture }}" width="80%" />
                                </td>
                                <td>
                                    {{ ($value->article_status==0 ) ? "ไม่ใช้งาน" : "ใช้งาน" }}
                                </td>
                                <td>
                                    <a href="{{ route('articles-edit',$value->article_id) }}" class="btn btn-info btn-sm">แก้ไข</a>
                                    <a onclick="delAccount('{{$value->article_id}}')" class="btn btn-secondary btn-sm">ลบ</a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    @endsection

    @section('javascript')

    <script>
        $(document).ready(function() {
            $('#dataTable').DataTable();
        });

        function delAccount(acc_id) {
            if (confirm('Delete bank account ? ')) {
                $.ajax({
                    method: "GET",
                    url: `/articles-destroy/${acc_id}` + '?_token=' + '{{ csrf_token() }}',
                }).done(function(data) {
                    window.location.reload();
                });
            }
        }
    </script>

    @endsection