@extends('layouts.app')
@section('title')
<title>รายการคำสั่งซื้อ</title>
@endsection

@section('css')
<style>
    td {
        padding: 0 !important;
    }

    .inputnum {
        text-align: right;
    }

    .itempic {
        width: 180px;
        height: 180px;
    }
</style>
@endsection

@section('content')
<div class="card">
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h1 class="h3 mb-0 text-gray-800">{{ ($StaPage == 'create') ? 'เพิ่ม' : 'แก้ไข' }}คำสั่งซื้อ</h1>
                    <!-- <a href="{{ route('orders') }}" class="btn btn-primary">กลับ</a> -->
                </div>
            </div>
            <div class="card-body">
                @if($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoop!</strong>
                    There were some problems with your input. <br><br>
                    <ul>
                        @foreach(@errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif


                <form action="{{ ($StaPage == 'create') ? route('orders-store') : route('orders-update',$data[0]->ID) }}" enctype="multipart/form-data" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>ประเภทขนส่ง</strong>
                                <select name="TransID" id="TransID" class="form-control" onchange="chkCalDemo()" required>
                                    <option value=""> - เลือก -</option>
                                    @foreach($trans as $key => $val)
                                    <option @if($StaPage=='edit' ) {{ ($data[0]->TransID == $val->TransID) ? "selected" : "" }} @endif value="{{ $val->TransID }}">{{ $val->TransName }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>ประเภทสินค้า</strong>
                                <select name="ProdID" id="ProdID" class="form-control" onchange="chkCalDemo()" required>
                                    <option value=""> - เลือก -</option>
                                    @foreach($prod as $key => $val)
                                    <option @if($StaPage=='edit' ){{ ($data[0]->ProdID == $val->ProdID) ? "selected" : "" }} @endif value="{{ $val->ProdID }}">{{ $val->ProdName }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>รูปแบบการสั่ง</strong>
                                <select name="OrderType" id="OrderType" class="form-control" equired>
                                    <option value=""> - เลือก -</option>
                                    <option @if($StaPage=='edit' ){{ ($data[0]->OrderType == 'Preorder') ? "selected" : "" }} @endif value="Preorder">ลูกค้าฝากพรีออเดอร์สินค้า</option>
                                    <option @if($StaPage=='edit' ){{ ($data[0]->OrderType == 'Import') ? "selected" : "" }} @endif value="Import">ลูกค้าฝากนำเข้า</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>อัตราแลกเปลี่ยน(1หยวน:บาท)</strong>
                                <input type="number" onchange="calExchange()" name="ExchangeRate" id="ExchangeRate" class="form-control" value="{{ ($StaPage == 'edit') ? $data[0]->ExchangeRate : $exchange[0]->THRate }}" step="0.01" min="1" max="1000" placeholder="กรอกอัตราแลกเปลี่ยน" required>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>Website</strong>
                                <input type="text" name="OrderWeb" class="form-control" value="{{ ($StaPage == 'edit') ? $data[0]->OrderWeb : '' }}" placeholder="กรอกเว็บไซท์ที่สั่งซื้อ">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>บัญชีแอดมิน</strong>
                                <input type="text" name="AdminAccount" class="form-control" value="{{ ($StaPage == 'edit') ? $data[0]->AdminAccount : '' }}" placeholder="กรอกบัญชีแอดมิน">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>เลขที่คำสั่งซื้อ (ร้านค้า)</strong>
                                <input type="text" name="OrderNumber" id="OrderNumberEdit" class="form-control" placeholder="กรอกเลขคำสั่งซื้อ" value="{{ ($StaPage == 'edit') ? $data[0]->OrderNumber : '' }}" maxlength="100">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>วันที่สั่งซื้อ (ร้านค้า)</strong>
                                <input type="date" name="OrderDate" class="form-control" value="{{ ($StaPage == 'edit') ? $data[0]->OrderDate == '' ? '' : date('Y-m-d', strtotime($data[0]->OrderDate)) : '' }}" maxlength="10">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 align-items-end" align="center">
                            <a href="{{ route('orders') }}" class="btn btn-secondary">กลับ</a> &nbsp;
                            <button type="submit" class="btn btn-success">บันทึกการแก้ไขคำสั่งซื้อ</button>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-lg-12">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="customer-tab" data-toggle="tab" href="#customer" role="tab" aria-controls="customer" aria-selected="true">ข้อมูลลูกค้า</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="item-tab" data-toggle="tab" href="#item" role="tab" aria-controls="item" aria-selected="false">ข้อมูลสินค้า</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="delivery-tab" data-toggle="tab" href="#delivery" role="tab" aria-controls="delivery" aria-selected="false">ข้อมูลจัดส่ง</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="qu-tab" data-toggle="tab" href="#qu" role="tab" aria-controls="qu" aria-selected="false">ข้อมูลใบเสนอราคา</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active pt-2" id="customer" role="tabpanel" aria-labelledby="customer-tab">
                                    <div class="row">
                                        @if($StaPage!='edit')
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <strong>ลูกค้า</strong>
                                                <select name="CstID" id="CstID" class="form-control">
                                                    <option value=""> - เลือก -</option>
                                                    @foreach($cst as $key => $val)
                                                    <option value="{{ $val->ID }}">{{ $val->CstID.' '.$val->CstFirstname.' ('.$val->CstCompanyName.')' }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        @endif
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <strong>ชื่อลูกค้า</strong>
                                                <input type="text" value="{{ ($StaPage == 'edit') ? $data[0]->CstFirstname : '' }}" placeholder="กรอกชื่อ" name="CstFirstname" id="CstFirstname" class="form-control" maxlength="50" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <strong>นามสกุล</strong>
                                                <input type="text" name="CstLastname" id="CstLastname" class="form-control" placeholder="กรอกนามสกุล" value="{{ ($StaPage == 'edit') ? $data[0]->CstLastname : '' }}" maxlength="50" required>
                                            </div>
                                        </div>
                                        @if($StaPage=='edit')
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <strong>ประเภทลูกค้า</strong><br />
                                                {{ $cst_type[0]->cst_type_name }}
                                                <input type="hidden" name="CstTypeId" id="CstTypeId" value="{{ $cst_type[0]->cst_type_id }}" />
                                            </div>
                                        </div>
                                        @else
                                        <input type="hidden" name="CstTypeId" id="CstTypeId" value="" />
                                        @endif
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <strong>ชื่อบริษัท</strong>
                                                <input type="text" name="CstCompanyName" id="CstCompanyName" class="form-control" placeholder="กรอกชื่อบริษัท" value="{{ ($StaPage == 'edit') ? $data[0]->CstCompanyName : '' }}" maxlength="100">
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <strong>โทรศัพท์</strong>
                                                <input type="text" name="CstTel" id="CstTel" class="form-control" placeholder="กรอกเบอร์โทรศัพท์" value="{{ ($StaPage == 'edit') ? $data[0]->CstTel : '' }}" maxlength="50" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <strong>อีเมล์</strong>
                                                <input type="email" name="email" id="email" class="form-control" value="{{ ($StaPage == 'edit') ? $data[0]->email : '' }}" placeholder="กรอกอีเมล์" maxlength="50" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <strong>ที่อยู่</strong>
                                                <textarea name="CstAddress" id="CstAddress" class="form-control" rows="2" placeholder="กรอกที่อยู่" maxlength="255">{{ ($StaPage == 'edit') ? $data[0]->CstAddress : '' }}</textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <strong>จังหวัด</strong>
                                                <input type="text" name="CstProvice" id="CstProvice" class="form-control" placeholder="กรอกจังหวัด" value="{{ ($StaPage == 'edit') ? $data[0]->CstProvice : '' }}" maxlength="100" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <strong>เขต/อำเภอ</strong>
                                                <input type="text" name="CstDistrict" id="CstDistrict" class="form-control" placeholder="กรอกเขต/อำเภอ" value="{{ ($StaPage == 'edit') ? $data[0]->CstDistrict : '' }}" maxlength="100" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <strong>แขวง/ตำบล</strong>
                                                <input type="text" name="CstSubdistrict" id="CstSubdistrict" class="form-control" placeholder="กรอกแขวง/ตำบล" value="{{ ($StaPage == 'edit') ? $data[0]->CstSubdistrict : '' }}" maxlength="100" required>
                                            </div>
                                        </div>
                                        <div class="col-lg-3">
                                            <div class="form-group">
                                                <strong>รหัสไปรษณีย์</strong>
                                                <input type="text" name="CstZipcode" id="CstZipcode" class="form-control" placeholder="กรอกรหัสไปรษณีย์" value="{{ ($StaPage == 'edit') ? $data[0]->CstZipcode : '' }}" maxlength="100" required>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="tab-pane fade pt-2" id="item" role="tabpanel" aria-labelledby="item-tab">
                                    <div class="row">
                                        <input type="hidden" id="nRow" name="nRow" value="{{ $StaPage == 'edit' ? count($detail) + 1 : 2 }}">
                                        <div class="col-12" align="right">
                                            <button type="button" onclick="createNewOrder()" class="btn btn-sm btn-warning">ย้ายสินค้าไปคำสั่งซื้อใหม่</button>
                                            <br />
                                        </div>
                                        <div class="col-xs-12 table-responsive table_wrapper">
                                            <table class="table table-bordered" id="tblDetail">
                                                <thead class="thead-dark">
                                                    <tr>
                                                        <th align="center" width="44%">ข้อมูลสินค้า</th>
                                                        <th align="center" width="10%">รูป</th>
                                                        <th align="center" width="9%">ราคา/ชิ้น (หยวน)</th>
                                                        <th align="center" width="9%">จำนวน</th>
                                                        <th align="center" width="9%">รวม (หยวน)</th>
                                                        <th align="center" width="10%">รวม (บาท)</th>
                                                        @if ($StaPage != 'edit' || ($StaPage == 'edit' && ($data[0]->Orderstatus == 1 || $data[0]->Orderstatus == 2)))
                                                        <th align="center" width="5%">
                                                            <button type="button" onclick="addRowDetail()" class="btn btn-primary">เพิ่ม</button>
                                                        </th>
                                                        @endif
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if($StaPage == 'edit')
                                                    @foreach($detail as $key => $value)
                                                    <tr id="tr{{ $key + 1 }}">
                                                        <input type="hidden" name="ItemID{{ $key + 1 }}" value="{{ $value->ID }}">
                                                        <td>
                                                            <div class="container-fluid">
                                                                <div class="row">
                                                                    <div class="input-group mb-3 mt-3">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">ชื่อ</span>
                                                                        </div>
                                                                        <input {{ ($data[0]->Orderstatus >2) ? 'readonly' : '' }} type="text" id="ItemName{{ $key + 1 }}" name="ItemName{{ $key + 1 }}" value="{{ $value->ItemName }}" class="form-control" maxlength="200">
                                                                    </div>
                                                                    <div class="input-group mb-3">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">รายละเอียด</span>
                                                                        </div>
                                                                        <input {{ ($data[0]->Orderstatus >2) ? 'readonly' : '' }} type="text" id="Detail{{ $key + 1 }}" name="Detail{{ $key + 1 }}" value="{{ $value->Detail }}" class="form-control" maxlength="200">
                                                                    </div>
                                                                    <div class="input-group mb-3">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">Link</span>
                                                                        </div>
                                                                        <input {{ ($data[0]->Orderstatus >2) ? 'readonly' : '' }} type="text" id="Link{{ $key + 1 }}" name="Link{{ $key + 1 }}" value="{{ $value->Link }}" class="form-control" maxlength="200">
                                                                    </div>
                                                                    <div class="input-group mb-3">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">หมายเหตุ</span>
                                                                        </div>
                                                                        <input {{ ($data[0]->Orderstatus >2) ? 'readonly' : '' }} type="text" id="Remark{{ $key + 1 }}" name="Remark{{ $key + 1 }}" value="{{ $value->Remark }}" class="form-control" maxlength="200">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            @if($value->Pic != '')
                                                            <a onClick="window.open('{{ $value->Pic }}','pagename','resizable,height=400,width=400'); return false;" href="#" target="_blank">
                                                                <img src="{{ $value->Pic }}" class="itempic img-thumbnail" alt=""><br>
                                                            </a>
                                                            @endif
                                                            @if ($StaPage == 'edit' && ($data[0]->Orderstatus == 1 || $data[0]->Orderstatus == 2))
                                                            <input type="file" id="Pic{{ $key + 1 }}" name="Pic{{ $key + 1 }}" accept="image/jpg, image/jpeg, image/png" class="form-control">
                                                            @endif
                                                        </td>
                                                        <td><input {{ ($data[0]->Orderstatus >2) ? 'readonly' : '' }} type="number" onchange="calPrice('{{ $key + 1 }}')" min="0" id="Price{{ $key + 1 }}" step="0.01" name="Price{{ $key + 1 }}" value="{{ $value->Price }}" class="form-control inputnum"></td>
                                                        <td><input {{ ($data[0]->Orderstatus >2) ? 'readonly' : '' }} type="number" onchange="calPrice('{{ $key + 1 }}')" min="0" id="Qty{{ $key + 1 }}" step="1" name="Qty{{ $key + 1 }}" value="{{ $value->Qty }}" class="form-control inputnum"></td>
                                                        <td><input type="number" min="0" id="TotalLine{{ $key + 1 }}" data-rows="{{ $key + 1 }}" name="TotalLine{{ $key + 1 }}" value="{{ $value->TotalLine }}" class="form-control inputnum calTotalAll" readonly></td>
                                                        <td><input type="number" min="0" id="TotalLineTHB{{ $key + 1 }}" name="TotalLineTHB{{ $key + 1 }}" value="{{ $value->TotalLineTHB }}" class="form-control inputnum" readonly></td>
                                                        @if ($data[0]->Orderstatus == 1 || $data[0]->Orderstatus == 2)
                                                        <td align="center">
                                                            <button type="button" onclick="delDetail('{{ $key + 1 }}')" class="btn btn-danger">ลบ</button>
                                                            <br /><br />
                                                            <input type="checkbox" name="chkMoveItems" id="{{ $value->ID }}" />
                                                        </td>
                                                        @endif
                                                    </tr>
                                                    @endforeach
                                                    @else
                                                    <tr id="tr1">
                                                        <td>
                                                            <div class="container-fluid">
                                                                <div class="row">
                                                                    <div class="input-group mb-3 mt-3">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">ชื่อ</span>
                                                                        </div>
                                                                        <input type="text" id="ItemName1" name="ItemName1" class="form-control" maxlength="200">
                                                                    </div>
                                                                    <div class="input-group mb-3">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">รายละเอียด</span>
                                                                        </div>
                                                                        <input type="text" id="Detail1" name="Detail1" class="form-control" maxlength="200">
                                                                    </div>
                                                                    <div class="input-group mb-3">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">Link</span>
                                                                        </div>
                                                                        <input type=" text" id="Link1" name="Link1" class="form-control" maxlength="200">
                                                                    </div>
                                                                    <div class="input-group mb-3">
                                                                        <div class="input-group-prepend">
                                                                            <span class="input-group-text">หมายเหตุ</span>
                                                                        </div>
                                                                        <input type="text" id="Remark1" name="Remark1" class="form-control" maxlength="200">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <img src="{{ asset('/images/unnamed.jpg') }}" class="itempic img-thumbnail" alt=""><br>
                                                            <input type="file" id="Pic1" name="Pic1" accept="image/jpg, image/jpeg, image/png" class="form-control">
                                                        </td>
                                                        <td><input type="number" onchange="calPrice('1')" min="0" id="Price1" step="0.01" name="Price1" value="0" class="form-control inputnum"></td>
                                                        <td><input type="number" onchange="calPrice('1')" min="0" id="Qty1" step="1" name="Qty1" value="0" class="form-control inputnum"></td>
                                                        <td><input type="number" min="0" id="TotalLine1" data-rows="1" name="TotalLine1" value="0" class="form-control inputnum calTotalAll" readonly></td>
                                                        <td><input type="number" min="0" id="TotalLineTHB1" name="TotalLineTHB1" value="0" class="form-control inputnum" readonly></td>
                                                        <td align="center">
                                                            <button type="button" onclick="delDetail('1')" class="btn btn-danger">ลบ</button>
                                                        </td>
                                                    </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4 offset-lg-8">
                                            <div class="form-group row">
                                                <strong class="col-lg-5 text-right" for="TotalAllCNY">ยอดรวม (หยวน)</strong>
                                                <div class="col-lg-7">
                                                    <input type="text" name="TotalAllCNY" id="TotalAllCNY" class="form-control inputnum" value="{{ ($StaPage == 'edit') ? $data[0]->TotalAllCNY : 0 }}" readonly>
                                                </div>
                                            </div>
                                            <div class="form-group row">
                                                <strong class="col-lg-5 text-right" for="TotalAllTHB">ยอดรวม (บาท)</strong>
                                                <div class="col-lg-7">
                                                    <input type="text" name="TotalAllTHB" id="TotalAllTHB" class="form-control inputnum" value="{{ ($StaPage == 'edit') ? $data[0]->TotalAllTHB : 0 }}" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade pt-2" id="delivery" role="tabpanel" aria-labelledby="delivery-tab">
                                    <div class="container-fluid">
                                        <div id="MainDlv">
                                            @if(count($delivery) > 0)
                                            @foreach($delivery as $nkey => $each)
                                            <div class="row" id="rowDlv{{ $nkey + 1 }}">
                                                <input type="hidden" id="DlvID{{ $nkey + 1 }}" name="DlvID{{ $nkey + 1 }}" class="form-control" value="{{ $each->ID }}">
                                                <div class="card border-info mb-3">
                                                    <div class="card-header">
                                                        <div class="row">
                                                            <div class="mr-auto">ข้อมูลการจัดส่ง ครั้งที่ {{ $nkey + 1 }}</div>
                                                            <div class="col-auto">
                                                                <button class="btn btn-danger btn-sm" type="button" onclick="delDelivery('{{ $nkey + 1 }}')">ลบ</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-body text-info">
                                                        <div class="row">
                                                            <div class="col-lg-3">
                                                                <strong>จำนวนกล่องพัสดุ</strong>
                                                                <input type="number" onchange="calCBMEdit('{{ $nkey + 1 }}')" id="TotalBoxEdit{{ $nkey + 1 }}" name="TotalBox{{ $nkey + 1 }}" class="form-control" value="{{ ($StaPage == 'edit') ? $each->TotalBox : 1 }}" min="1" placeholder="กรอกจำนวนกล่องพัสดุ">
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <strong>น้ำหนัก (กิโลกรัม)</strong>
                                                                <input type="number" step="0.01" name="KG{{ $nkey + 1 }}" id="KGEdit{{ $nkey + 1 }}" onchange="chkCalDemo('{{ $nkey + 1 }}')" class="form-control" value="{{ ($StaPage == 'edit') ? $each->KG : '' }}" min="0" placeholder="กรอกกิโลกรัม">
                                                            </div>
                                                            <div class="col-lg-6" id="cal-demo{{ $nkey + 1 }}"></div>
                                                            <div class="col-lg-3">
                                                                <strong>กว้าง</strong>
                                                                <input type="number" step="0.01" onchange="calCBMEdit('{{ $nkey + 1 }}')" id="WidthEdit{{ $nkey + 1 }}" name="Width{{ $nkey + 1 }}" class="form-control" value="{{ ($StaPage == 'edit') ? $each->Width : '' }}" min="0" placeholder="กรอกความกว้าง">
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <strong>ยาว</strong>
                                                                <input type="number" step="0.01" onchange="calCBMEdit('{{ $nkey + 1 }}')" id="LengthEdit{{ $nkey + 1 }}" name="Length{{ $nkey + 1 }}" class="form-control" value="{{ ($StaPage == 'edit') ? $each->Length : '' }}" min="0" placeholder="กรอกความยาว">
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <strong>สูง</strong>
                                                                <input type="number" step="0.01" onchange="calCBMEdit('{{ $nkey + 1 }}')" id="HeightEdit{{ $nkey + 1 }}" name="Height{{ $nkey + 1 }}" class="form-control" value="{{ ($StaPage == 'edit') ? $each->Height : '' }}" min="0" placeholder="กรอกความสูง">
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <strong>ปริมาตร (CBM)</strong>
                                                                <input type="number" step="0.0001" id="QueueEdit{{ $nkey + 1 }}" name="Queue{{ $nkey + 1 }}" class="form-control" value="{{ ($StaPage == 'edit') ? $each->Queue : '' }}" placeholder="กรอกปริมาตร">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-3">
                                                                <strong>ค่าขนส่งจีน-จีน</strong>
                                                                <input type="number" step="0.01" name="DeliveryCNCN{{ $nkey + 1 }}" class="form-control" value="{{ ($StaPage == 'edit') ? $each->DeliveryCNCN : '' }}" min="0" placeholder="กรอกค่าขนส่งจีน-จีน">
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <strong>ค่าขนส่งจีน-ไทย</strong>
                                                                <input type="number" step="0.01" name="DeliveryCNTH{{ $nkey + 1 }}" class="form-control" value="{{ ($StaPage == 'edit') ? $each->DeliveryCNTH : '' }}" min="0" placeholder="กรอกค่าขนส่งจีน-ไทย">
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <strong>ค่าขนส่งไทย-ไทย</strong>
                                                                <input type="number" step="0.01" name="DeliveryTHTH{{ $nkey + 1 }}" class="form-control" value="{{ ($StaPage == 'edit') ? $each->DeliveryTHTH : '' }}" min="0" placeholder="กรอกค่าขนส่งไทย-ไทย">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-3">
                                                                <strong>เลขที่จัดส่ง (ร้านค้า)</strong>
                                                                <input type="text" name="OrderTracking{{ $nkey + 1 }}" id="OrderTracking{{ $nkey + 1 }}" class="form-control" placeholder="กรอกเลขที่จัดส่ง" value="{{ ($StaPage == 'edit') ? $each->OrderTracking : '' }}" maxlength="100">
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <strong>วันที่ของถึงโกดังจีน</strong>
                                                                <input type="date" name="WarehouseCNDate{{ $nkey + 1 }}" class="form-control" value="{{ ($StaPage == 'edit') ? $each->WarehouseCNDate == '' ? '' : date('Y-m-d', strtotime($each->WarehouseCNDate)) : '' }}" maxlength="10" placeholder="กรอกวันที่ถึงโกดังจีน">
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <strong>วันที่ส่งออกจากโกดังจีน</strong>
                                                                <input type="date" name="ExportDate{{ $nkey + 1 }}" class="form-control" value="{{ ($StaPage == 'edit') ? $each->ExportDate == '' ? '' : date('Y-m-d', strtotime($each->ExportDate)) : '' }}" maxlength="10" placeholder="กรอกวันที่ส่งออก">
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <strong>เลขที่บิลใบสั่งซื้อ (PO)</strong>
                                                                <input type="text" name="PurchaseOrder{{ $nkey + 1 }}" id="PurchaseOrderEdit{{ $nkey + 1 }}" class="form-control" placeholder="กรอกเลขบิลใบสั่งซื้อ" value="{{ ($StaPage == 'edit') ? $each->PurchaseOrder : '' }}" maxlength="20">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-3">
                                                                <strong>ตู้ที่</strong>
                                                                <input type="text" name="Container{{ $nkey + 1 }}" id="ContainerEdit{{ $nkey + 1 }}" class="form-control" placeholder="กรอกตู้ที่" value="{{ ($StaPage == 'edit') ? $each->Container : '' }}" maxlength="50">
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <strong>วันที่ของถึงโกดังไทย</strong>
                                                                <input type="date" name="ImportDate{{ $nkey + 1 }}" class="form-control" value="{{ ($StaPage == 'edit') ? $each->ImportDate == '' ? '' : date('Y-m-d', strtotime($each->ImportDate)) : '' }}" maxlength="10" placeholder="กรอกวันที่นำเข้า">
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <strong>วันที่จัดส่งหาลูกค้า</strong>
                                                                <input type="date" name="DeliveryDate{{ $nkey + 1 }}" class="form-control" value="{{ ($StaPage == 'edit') ? $each->DeliveryDate == '' ? '' : date('Y-m-d', strtotime($each->DeliveryDate)) : '' }}" maxlength="10" placeholder="กรอกวันที่จัดส่ง">
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <strong>บริษัทขนส่ง</strong>
                                                                <select name="ShipComID{{ $nkey + 1 }}" class="form-control">
                                                                    <option value=""> - เลือก -</option>
                                                                    @foreach($ShipCom as $key => $valSC)
                                                                    <option {{ ($each->ShipComID == $valSC->ID) ? "selected" : "" }} value="{{ $valSC->ID }}">{{ $valSC->CompanyName }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-3">
                                                                <strong>เลขที่พัสดุจัดส่ง</strong>
                                                                <input type="text" name="TrackingNO{{ $nkey + 1 }}" id="TrackingNOEdit{{ $nkey + 1 }}" class="form-control" placeholder="กรอกเลขพัสดุ" value="{{ ($StaPage == 'edit') ? $each->TrackingNO : '' }}" maxlength="50">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endforeach
                                            @else
                                            <div class="row" id="rowDlv1">
                                                <div class="card border-info mb-3">
                                                    <div class="card-header">
                                                        <div class="row">
                                                            <div class="mr-auto">ข้อมูลการจัดส่ง ครั้งที่ 1</div>
                                                            <div class="col-auto">
                                                                <button class="btn btn-danger btn-sm" type="button" onclick="delDelivery('1')">ลบ</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="card-body text-info">
                                                        <div class="row">
                                                            <div class="col-lg-3">
                                                                <strong>จำนวนกล่องพัสดุ</strong>
                                                                <input type="number" onchange="calCBMEdit('1')" id="TotalBoxEdit1" name="TotalBox1" class="form-control" min="1" placeholder="กรอกจำนวนกล่องพัสดุ">
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <strong>น้ำหนัก (กิโลกรัม)</strong>
                                                                <input type="number" step="0.01" name="KG1" id="KGEdit1" onchange="chkCalDemo('1')" class="form-control" min="0" placeholder="กรอกกิโลกรัม">
                                                            </div>
                                                            <div class="col-lg-6" id="cal-demo1"></div>
                                                            <div class="col-lg-3">
                                                                <strong>กว้าง</strong>
                                                                <input type="number" step="0.01" onchange="calCBMEdit('1')" id="WidthEdit1" name="Width1" class="form-control" min="0" placeholder="กรอกความกว้าง">
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <strong>ยาว</strong>
                                                                <input type="number" step="0.01" onchange="calCBMEdit('1')" id="LengthEdit1" name="Length1" class="form-control" min="0" placeholder="กรอกความยาว">
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <strong>สูง</strong>
                                                                <input type="number" step="0.01" onchange="calCBMEdit('1')" id="HeightEdit1" name="Height1" class="form-control" min="0" placeholder="กรอกความสูง">
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <strong>ปริมาตร (CBM)</strong>
                                                                <input type="number" step="0.0001" id="QueueEdit1" name="Queue1" class="form-control" placeholder="กรอกปริมาตร">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-3">
                                                                <strong>ค่าขนส่งจีน-จีน</strong>
                                                                <input type="number" step="0.01" name="DeliveryCNCN1" class="form-control" min="0" placeholder="กรอกค่าขนส่งจีน-จีน">
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <strong>ค่าขนส่งจีน-ไทย</strong>
                                                                <input type="number" step="0.01" name="DeliveryCNTH1" class="form-control" min="0" placeholder="กรอกค่าขนส่งจีน-ไทย">
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <strong>ค่าขนส่งไทย-ไทย</strong>
                                                                <input type="number" step="0.01" name="DeliveryTHTH1" class="form-control" min="0" placeholder="กรอกค่าขนส่งไทย-ไทย">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-3">
                                                                <strong>เลขที่จัดส่ง (ร้านค้า)</strong>
                                                                <input type="text" name="OrderTracking1" id="OrderTracking1" class="form-control" placeholder="กรอกเลขที่จัดส่ง" maxlength="100">
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <strong>วันที่ของถึงโกดังจีน</strong>
                                                                <input type="date" name="WarehouseCNDate1" class="form-control" maxlength="10" placeholder="กรอกวันที่ถึงโกดังจีน">
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <strong>วันที่ส่งออกจากโกดังจีน</strong>
                                                                <input type="date" name="ExportDate1" class="form-control" maxlength="10" placeholder="กรอกวันที่ส่งออก">
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <strong>เลขที่บิลใบสั่งซื้อ (PO)</strong>
                                                                <input type="text" name="PurchaseOrder1" id="PurchaseOrderEdit1" class="form-control" placeholder="กรอกเลขบิลใบสั่งซื้อ" maxlength="20">
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-3">
                                                                <strong>ตู้ที่</strong>
                                                                <input type="text" name="Container1" id="ContainerEdit1" class="form-control" placeholder="กรอกตู้ที่" maxlength="50">
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <strong>วันที่ของถึงโกดังไทย</strong>
                                                                <input type="date" name="ImportDate1" class="form-control" maxlength="10" placeholder="กรอกวันที่นำเข้า">
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <strong>วันที่จัดส่งหาลูกค้า</strong>
                                                                <input type="date" name="DeliveryDate1" class="form-control" maxlength="10" placeholder="กรอกวันที่จัดส่ง">
                                                            </div>
                                                            <div class="col-lg-3">
                                                                <strong>บริษัทขนส่ง</strong>
                                                                <select name="ShipComID1" class="form-control">
                                                                    <option value=""> - เลือก -</option>
                                                                    @foreach($ShipCom as $key => $val)
                                                                    <option value="{{ $val->ID }}">{{ $val->CompanyName }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-3">
                                                                <strong>เลขที่พัสดุจัดส่ง</strong>
                                                                <input type="text" name="TrackingNO1" id="TrackingNOEdit1" class="form-control" placeholder="กรอกเลขพัสดุ" maxlength="50">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12" align="center">
                                                <button type="button" onclick="addRowDelivery()" class="btn btn-warning">เพิ่มข้อมูลการจัดส่ง</button>
                                                <input type="hidden" id="nRowDlv" name="nRowDlv" value="{{ $StaPage == 'edit' ? (count($delivery)==0) ? 2 : count($delivery) + 1 : 2 }}">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab-pane fade pt-2" id="qu" role="tabpanel" aria-labelledby="qu-tab">
                                    <div class="container-fluid">
                                        <div class="row">
                                            <div class="col-12" align="right">
                                                <button class="btn btn-success btn-sm" type="button" onclick="newQu()">สร้างใบเสนอราคาใหม่</button>
                                            </div>
                                            <div class="col-12 table-responsive table_wrapper">
                                                <table class="table table-bordered" id="tblQU">
                                                    <thead class="thead-dark">
                                                        <tr>
                                                            <th align="center" width="3%">#</th>
                                                            <th align="center" width="20%">วันที่สร้าง</th>
                                                            <th align="center" width="20%">รายละเอียด</th>
                                                            <th align="center" width="20%">จำนวนเงิน (บาท)</th>
                                                            <th align="center" width="25%">สถานะ</th>
                                                            <th align="center" width="12%"></th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        @if(count($quotation) > 0)
                                                        @foreach($quotation as $nkey => $each)
                                                        <tr>
                                                            <td align="center">{{$nkey+1}}</td>
                                                            <td align="center">{{ date("d/m/Y H:i",strtotime($each->created_at)) }}</td>
                                                            <td>
                                                                @if($each->Product == "Y")
                                                                ค่าสินค้า
                                                                @endif

                                                                @if($each->Delivery != "" )
                                                                ค่าจัดส่ง
                                                                @endif
                                                            </td>
                                                            <td align="right">{{$each->Total}}</td>
                                                            <td align="center">
                                                                {{ $each->qu_status_name }}
                                                            <td align="center">
                                                                <a href="{{ route('orders-quotation',$each->ID) }}" target="_blank" rel="noopener noreferrer">
                                                                    <button type="button" class="btn-info btn-sm">พิมพ์</button>
                                                                </a>
                                                                <button type="button" class="btn-danger btn-sm" onclick="deleteQu('{{ $each->ID }}')">ลบ</button>
                                                            </td>
                                                        </tr>
                                                        @endforeach
                                                        @endif

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<!-- MODAL -->
<!-- New QU Modal-->
<div class="modal fade" id="newQUModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="newQUModalLabel">สร้างใบเสนอราคาใหม่</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body" id="newQUModalData">
                <h6>กรุณาเลือกรายการที่จะสร้างใบเสนอราคา</h6>
                <input type="checkbox" id="Product" name="listQUProduct" value="Product">
                <label for="Product"> ค่าสินค้า</label><br>
                @if(count($delivery) > 0)
                @foreach($delivery as $nkey => $each)
                <input type="checkbox" id="Delivery_{{$nkey }}" name="listQUDelivery" value="{{ $each->ID }}">
                <label for="Delivery_{{$nkey }}"> ค่าจัดส่งสินค้า ครั้งที่ {{ $nkey + 1 }}</label><br>
                @endforeach
                @endif
            </div>
            <div class="modal-footer">
                <button class="btn btn-secondary" type="button" data-dismiss="modal">ยกเลิก</button>
                <a class="btn btn-primary" href="#" onclick="createNewQU()">ยืนยัน</a>
            </div>
        </div>
    </div>
</div>


@endsection

@section('javascript')

<script>
    const path = "{{ asset('/images/unnamed.jpg') }}"
    $(document).ready(function() {
        $('#CstID').select2();
        chkCalDemo()
    });
    var cst = <?php echo json_encode($cst); ?>;
    $('#CstID').change(function() {
        CstID = $(this).val();
        $('#CstFirstname').val(cst.find(obj => obj.ID == CstID).CstFirstname);
        $('#CstLastname').val(cst.find(obj => obj.ID == CstID).CstLastname);
        $('#CstCompanyName').val(cst.find(obj => obj.ID == CstID).CstCompanyName);
        $('#CstTel').val(cst.find(obj => obj.ID == CstID).CstTel);
        $('#CstAddress').val(cst.find(obj => obj.ID == CstID).CstAddress);
        $('#CstDistrict').val(cst.find(obj => obj.ID == CstID).CstDistrict);
        $('#CstSubdistrict').val(cst.find(obj => obj.ID == CstID).CstSubdistrict);
        $('#CstProvice').val(cst.find(obj => obj.ID == CstID).CstProvice);
        $('#CstZipcode').val(cst.find(obj => obj.ID == CstID).CstZipcode);
        $('#email').val(cst.find(obj => obj.ID == CstID).email);
    });

    function newQu() {
        $('#newQUModal').modal('show');
    }

    function deleteQu(quid) {
        if (confirm("ลบใบเสนอราคาใบนี้เลยหรือไม่ ?")) {
            $.ajax({
                method: "POST",
                url: `/cancel-qu` + '?_token=' + '{{ csrf_token() }}',
                data: {
                    quid: quid
                }
            }).done(function(res) {
                // $('form').submit();
                $('#newQUModal').modal('hide');
                location.reload();
                // alert(res)
            });
        }
    }

    function createNewQU() {
        var delivery = "";
        var product = "";
        var order_id = '{{ isset($data[0]->ID) ? $data[0]->ID : 0 }}'
        // $(':checkbox:checked').each(function(i){
        $("[name='listQUDelivery']:checked").each(function(i) {
            delivery += $(this).attr('value') + ",";
        })
        if (delivery != "") {
            delivery = delivery.substring(0, delivery.length - 1);
        }

        $("[name='listQUProduct']:checked").each(function(i) {
            product = "Y"
        })
        // alert(delivery)
        if (delivery != "" || (product == 'Y' && delivery == "")) {
            if (confirm('ยืนยันที่จะสร้างใบเสนอราคาใหม่ใช่หรือไม่ ?')) {

                $.ajax({
                    method: "POST",
                    url: `/create-qu` + '?_token=' + '{{ csrf_token() }}',
                    data: {
                        delivery: delivery,
                        product: product,
                        order_id: order_id
                    }
                }).done(function(res) {
                    // $('form').submit();
                    $('#newQUModal').modal('hide');
                    location.reload();
                    // alert(res)
                });
            }
        } else {
            alert('กรุณาเลือกรายการที่จะสร้างใบเสนอราคา')
        }
    }

    function createNewOrder() {
        var val = [];
        var order_id = '{{ isset($data[0]->ID) ? $data[0]->ID : 0 }}'
        // $(':checkbox:checked').each(function(i){
        $("[name='chkMoveItems']:checked").each(function(i) {
            val[i] = $(this).attr('id');
        })
        if (val.length > 0) {
            if (confirm('ยืนยันที่จะย้ายสินค้าทั้งหมดไปคำสั่งซื้อใหม่หรือไม่ ?')) {

                $.ajax({
                    method: "POST",
                    url: `/change-to-new-orders` + '?_token=' + '{{ csrf_token() }}',
                    data: {
                        val: val,
                        order_id: order_id
                    }
                }).done(function(res) {
                    // $('form').submit();
                    location.reload();
                });
            }
        } else {
            alert('กรุณาเลือกสินค้าก่อน')
        }
    }

    function calExchange() {
        ExchangeRate = parseFloat($('#ExchangeRate').val());
        $('.calTotalAll').each(function() {
            rows = $(this).data('rows');
            TotalLine = $('#TotalLine' + rows).val();
            TotalLineTHB = precise_round((TotalLine * ExchangeRate), 2);
            $('#TotalLineTHB' + rows).val(TotalLineTHB);
        });
        calTotalAll = 0;
        $('.calTotalAll').each(function() {
            calTotalAll += parseFloat(this.value);
        });
        TotalAllCNY = precise_round((calTotalAll), 2);
        TotalAllTHB = precise_round((TotalAllCNY * ExchangeRate), 2);
        $('#TotalAllCNY').val(TotalAllCNY);
        $('#TotalAllTHB').val(TotalAllTHB);
    }

    function calPrice(row) {
        Price = parseFloat($('#Price' + row).val());
        Qty = parseFloat($('#Qty' + row).val());
        ExchangeRate = parseFloat($('#ExchangeRate').val());
        TotalLine = precise_round((Price * Qty), 2);
        TotalLineTHB = precise_round((TotalLine * ExchangeRate), 2);
        $('#TotalLine' + row).val(TotalLine);
        $('#TotalLineTHB' + row).val(TotalLineTHB);
        calTotalAll = 0;
        $('.calTotalAll').each(function() {
            calTotalAll += parseFloat(this.value);
        });
        TotalAllCNY = precise_round((calTotalAll), 2);
        TotalAllTHB = precise_round((TotalAllCNY * ExchangeRate), 2);

        $('#TotalAllCNY').val(TotalAllCNY);
        $('#TotalAllTHB').val(TotalAllTHB);
    }

    function precise_round(num, decimals) {
        var sign = num >= 0 ? 1 : -1;
        return (Math.round((num * Math.pow(10, decimals)) + (sign * 0.001)) / Math.pow(10, decimals)).toFixed(decimals);
    }

    function addRowDetail() {
        nRow = $('#nRow').val();

        newRow = `<tr id="tr${nRow}">
            <td>
                <div class="container-fluid">
                    <div class="row">
                        <div class="input-group mb-3 mt-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">ชื่อ</span>
                            </div>
                            <input type="text" id="ItemName${nRow}" name="ItemName${nRow}" class="form-control" maxlength="200">
                        </div> 
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">รายละเอียด</span>
                            </div>
                            <input type="text" id="Detail${nRow}" name="Detail${nRow}" class="form-control" maxlength="200">
                        </div> 
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">Link</span>
                            </div>
                            <input type="text" id="Link${nRow}" name="Link${nRow}" class="form-control" maxlength="200">
                        </div> 
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text">หมายเหตุ</span>
                            </div>
                            <input type="text" id="Remark${nRow}" name="Remark${nRow}" class="form-control" maxlength="200">
                        </div>
                    </div>
                </div>
            </td>
            <td>
                <img src="${path}" class="itempic img-thumbnail" alt=""><br>
                <input type="file" id="Pic${nRow}" name="Pic${nRow}" accept="image/jpg, image/jpeg, image/png" class="form-control">
            </td>
            <td><input type="number" onchange="calPrice(${nRow})" min="0" id="Price${nRow}" step="0.1" name="Price${nRow}" value="0" class="form-control inputnum"></td>
            <td><input type="number" onchange="calPrice(${nRow})" min="0" id="Qty${nRow}" step="1" name="Qty${nRow}" value="0" class="form-control inputnum"></td>
            <td><input type="number" min="0" id="TotalLine${nRow}" data-rows="${nRow}" name="TotalLine${nRow}" value="0" class="form-control inputnum calTotalAll" readonly></td>
            <td><input type="number" min="0" id="TotalLineTHB${nRow}" name="TotalLineTHB${nRow}" value="0" class="form-control inputnum" readonly></td>
            <td align="center">
                <button type="button" onclick="delDetail(${nRow})" class="btn btn-danger">ลบ</button>
            </td>
        </tr>`;

        $("#tblDetail tbody").append(newRow);
        $('#nRow').val(parseInt(nRow) + 1);
    }

    function addRowDelivery() {
        nRow = $('#nRowDlv').val();
        newRow = ` 
                <div class="row" id="rowDlv${nRow}">
                    <div class="card border-info mb-3">
                        <div class="card-header">
                            <div class="row">
                                <div class="mr-auto">ข้อมูลการจัดส่ง ครั้งที่ ${nRow}</div>
                                <div class="col-auto">
                                    <button class="btn btn-danger btn-sm" type="button" onclick="delDelivery('${nRow}')">ลบ</button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body text-info">
                            <div class="row">
                                <div class="col-lg-3">
                                    <strong>จำนวนกล่องพัสดุ</strong>
                                    <input type="number" onchange="calCBMEdit('${nRow}')" id="TotalBoxEdit${nRow}" name="TotalBox${nRow}" class="form-control" min="1" placeholder="กรอกจำนวนกล่องพัสดุ">
                                </div>
                                <div class="col-lg-3">
                                    <strong>น้ำหนัก (กิโลกรัม)</strong>
                                    <input type="number" step="0.01" name="KG${nRow}" id="KGEdit${nRow}" onchange="chkCalDemo('${nRow}')" class="form-control" min="0" placeholder="กรอกกิโลกรัม">
                                </div>
                                <div class="col-lg-6" id="cal-demo${nRow}"></div>
                                <div class="col-lg-3">
                                    <strong>กว้าง</strong>
                                    <input type="number" step="0.01" onchange="calCBMEdit('${nRow}')" id="WidthEdit${nRow}" name="Width${nRow}" class="form-control" min="0" placeholder="กรอกความกว้าง">
                                </div>
                                <div class="col-lg-3">
                                    <strong>ยาว</strong>
                                    <input type="number" step="0.01" onchange="calCBMEdit('${nRow}')" id="LengthEdit${nRow}" name="Length${nRow}" class="form-control" min="0" placeholder="กรอกความยาว">
                                </div>
                                <div class="col-lg-3">
                                    <strong>สูง</strong>
                                    <input type="number" step="0.01" onchange="calCBMEdit('${nRow}')" id="HeightEdit${nRow}" name="Height${nRow}" class="form-control" min="0" placeholder="กรอกความสูง">
                                </div>
                                <div class="col-lg-3">
                                    <strong>ปริมาตร (CBM)</strong>
                                    <input type="number" step="0.0001" id="QueueEdit${nRow}" name="Queue${nRow}" class="form-control" placeholder="กรอกปริมาตร">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">
                                    <strong>ค่าขนส่งจีน-จีน</strong>
                                    <input type="number" step="0.01" name="DeliveryCNCN${nRow}" class="form-control" min="0" placeholder="กรอกค่าขนส่งจีน-จีน">
                                </div>
                                <div class="col-lg-3">
                                    <strong>ค่าขนส่งจีน-ไทย</strong>
                                    <input type="number" step="0.01" name="DeliveryCNTH${nRow}" class="form-control" min="0" placeholder="กรอกค่าขนส่งจีน-ไทย">
                                </div>
                                <div class="col-lg-3">
                                    <strong>ค่าขนส่งไทย-ไทย</strong>
                                    <input type="number" step="0.01" name="DeliveryTHTH${nRow}" class="form-control" min="0" placeholder="กรอกค่าขนส่งไทย-ไทย">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">
                                    <strong>เลขที่จัดส่ง (ร้านค้า)</strong>
                                    <input type="text" name="OrderTracking${nRow}" id="OrderTracking${nRow}" class="form-control" placeholder="กรอกเลขที่จัดส่ง" maxlength="100">
                                </div>
                                <div class="col-lg-3">
                                    <strong>วันที่ของถึงโกดังจีน</strong>
                                    <input type="date" name="WarehouseCNDate${nRow}" class="form-control" maxlength="10" placeholder="กรอกวันที่ถึงโกดังจีน">
                                </div>
                                <div class="col-lg-3">
                                    <strong>วันที่ส่งออกจากโกดังจีน</strong>
                                    <input type="date" name="ExportDate${nRow}" class="form-control" maxlength="10" placeholder="กรอกวันที่ส่งออก">
                                </div>
                                <div class="col-lg-3">
                                    <strong>เลขที่บิลใบสั่งซื้อ (PO)</strong>
                                    <input type="text" name="PurchaseOrder${nRow}" id="PurchaseOrderEdit${nRow}" class="form-control" placeholder="กรอกเลขบิลใบสั่งซื้อ" maxlength="20">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">
                                    <strong>ตู้ที่</strong>
                                    <input type="text" name="Container${nRow}" id="ContainerEdit${nRow}" class="form-control" placeholder="กรอกตู้ที่" maxlength="50">
                                </div>
                                <div class="col-lg-3">
                                    <strong>วันที่ของถึงโกดังไทย</strong>
                                    <input type="date" name="ImportDate${nRow}" class="form-control" maxlength="10" placeholder="กรอกวันที่นำเข้า">
                                </div>
                                <div class="col-lg-3">
                                    <strong>วันที่จัดส่งหาลูกค้า</strong>
                                    <input type="date" name="DeliveryDate${nRow}" class="form-control" maxlength="10" placeholder="กรอกวันที่จัดส่ง">
                                </div>
                                <div class="col-lg-3">
                                    <strong>บริษัทขนส่ง</strong>
                                    <select name="ShipComID${nRow}" class="form-control">
                                        <option value=""> - เลือก -</option>
                                        @foreach($ShipCom as $key => $valSC)
                                        <option value="{{ $valSC->ID }}">{{ $valSC->CompanyName }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-3">
                                    <strong>เลขที่พัสดุจัดส่ง</strong>
                                    <input type="text" name="TrackingNO${nRow}" id="TrackingNOEdit${nRow}" class="form-control" placeholder="กรอกเลขพัสดุ" maxlength="50">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            `;
        $("#MainDlv").append(newRow);
        $('#nRowDlv').val(parseInt(nRow) + 1);
    }

    function delDelivery(ID) {
        $('#rowDlv' + ID).remove();
    }

    function delDetail(ID) {
        $('#tr' + ID).remove();
    }

    function calCBMEdit(row) {
        let Width = $('#WidthEdit' + row).val();
        let Length = $('#LengthEdit' + row).val();
        let Height = $('#HeightEdit' + row).val();
        let TotalBox = $('#TotalBoxEdit' + row).val();

        let cbm = (Width * Length * Height * TotalBox) / 1000000
        if (cbm < 0.0001) {
            cbm = 0.0001
        } else {
            cbm = cbm.toFixed(4)
        }
        // alert(cbm)
        $('#QueueEdit' + row).val(cbm);
        chkCalDemo(row)

    }

    async function chkCalDemo(row) {
        let TransID = $('#TransID').val();
        let ProdID = $('#ProdID').val();
        let CstType = $('#CstTypeId').val();
        if (row !== undefined) {
            let KG = $('#KGEdit' + row).val();
            let CBM = $('#QueueEdit' + row).val();

            if (TransID != "" && ProdID != "" && CstType != "" && KG != "" && CBM != "") {
                $.ajax({
                    method: "POST",
                    url: `/orders-cal-fees` + '?_token=' + '{{ csrf_token() }}',
                    data: {
                        TransID: TransID,
                        ProdID: ProdID,
                        CstType: CstType,
                        KG: KG,
                        CBM: CBM,
                    }
                }).done(function(res) {
                    let response = res.res
                    if (response.status == "error") {
                        $('#cal-demo' + row).html(` 
                        <strong class="text-danger">${response.message}</strong>  
                    `)
                    } else {
                        $('#cal-demo' + row).html(`
                        <strong>คำนวณค่าขนส่ง</strong><br/>
                        <strong>- จากน้ำหนัก : </strong>${response.data.weight_price} บาท <br/>
                        <strong>- จากปริมาตร : </strong>${response.data.cbm_price} บาท <br/>  
                    `)
                    }
                });
            }
        } else {
            for (loop = 1; loop < $('#nRowDlv').val(); loop++) {

                let KG = $('#KGEdit' + loop).val();
                let CBM = $('#QueueEdit' + loop).val();

                if (TransID != "" && ProdID != "" && CstType != "" && KG != "" && CBM != "") {
                    await $.ajax({
                        method: "POST",
                        url: `/orders-cal-fees` + '?_token=' + '{{ csrf_token() }}',
                        data: {
                            TransID: TransID,
                            ProdID: ProdID,
                            CstType: CstType,
                            KG: KG,
                            CBM: CBM,
                        }
                    }).done(function(res) {
                        let response = res.res
                        if (response.status == "error") {
                            $().html(` 
                        <strong class="text-danger">${response.message}</strong>  
                    `)
                        } else {
                            $('#cal-demo' + loop).html(`
                        <strong>คำนวณค่าขนส่ง</strong><br/>
                        <strong>- จากน้ำหนัก : </strong>${response.data.weight_price} บาท <br/>
                        <strong>- จากปริมาตร : </strong>${response.data.cbm_price} บาท <br/>  
                    `)
                        }
                    });
                }
            }
        }
    }
</script>

@endsection