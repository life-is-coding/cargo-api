@extends('layouts.app')
@section('title')
<title>รายการคำสั่งซื้อ</title>
@endsection

@section('content')
@if($message = Session::get('success'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {{ $message }}
</div>
@endif

<div class="card">
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h1 class="h3 mb-0 text-gray-800">รายการคำสั่งซื้อ</h1>
                    <a href="#" class="btn btn-warning" onclick="openNewOrderModal()">รายการคำสั่งซื้อใหม่ <span class="badge badge-light" id="orderNew"></span></a>
                    <a href="{{ route('orders-create') }}" class="btn btn-success">เพิ่มรายการคำสั่งซื้อใหม่</a>
                </div>
            </div>
            <div class="card-body">
                <div class="row" id="orderStatus"></div>
                <br>
                <div class="row">
                    <div class="col-6">
                        <div class="input-group">
                            <input class="form-control" type="text" placeholder="พิมพ์คำค้นหา..." id="searchTable" />
                            <div class="input-group-append">
                                <!-- <span class="input-group-text">ค้นหา</span> -->
                                <button title="ค้นหา" class="btn btn-primary btn-icon" type="button" onclick="getOrderTable('all')">
                                    <i class="fas fa-search"></i>
                                    ค้นหา​
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="col-4">
                        <input class="form-control" type="text" placeholder="พิมพ์คำค้นหา..." id="searchTable" />
                    </div>
                    <div class="col-2">
                        <button title="ค้นหา" class="btn btn-primary btn-icon" type="button" onclick="getOrderTable('all')">
                            <i class="fas fa-search"></i>
                            ค้นหา​
                        </button>
                    </div> -->
                    <div class="col-2">
                        <button title="ค้นหา" class="btn btn-success btn-icon" type="button" onclick="$('#searchTable').val('');getOrderTable('all')">
                            <i class="fas fa-file-alt"></i>
                            ดูทั้งหมด
                        </button>
                    </div>
                </div>
                <div id="orderTable"></div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')

<script>
    $(document).ready(function() {
        getNewOrder()
        getOrderStatus()
        getOrderTable('all')
    });

    function getNewOrder() {
        $.ajax({
            method: "GET",
            url: "/orders-new",
        }).done(function(data) {
            // alert(data)
            $("#orderNew").html(data);
        });
    }

    function getOrderStatus() {
        $.ajax({
            method: "GET",
            url: "/orders-status",
        }).done(function(data) {
            // alert(data)
            $("#orderStatus").html(data);
        });
    }

    function getOrderTable(status, sub = "") {
        var searchTable = $("#searchTable").val()
        $.ajax({
            method: "POST",
            url: `/orders-table/${status}` + '?_token=' + '{{ csrf_token() }}',
            data: {
                searchTable: searchTable,
                sub: sub
            }
        }).done(function(data) {
            // alert(data)
            $("#orderTable").html(data);

            $('#dataTable').DataTable({
                "ordering": false,
                "searching": false
            });
        });
    }

    function openNewOrderModal() {
        $('#newOrderModal').modal('show');
        $.ajax({
            method: "GET",
            url: "/orders-request",
        }).done(function(data) {
            $("#newOrderModalData").html(data);
        });
    }

    function saveBookingOrder() {
        var val = [];
        $('.checkOrder:checked').each(function(i) {
            val[i] = $(this).val();
        });

        $.ajax({
            method: "POST",
            url: "/orders-booking" + '?_token=' + '{{ csrf_token() }}',
            data: {
                val: val
            }
        }).done(function(data) {
            $("#newOrderModalData").html("");
            $('#newOrderModal').modal('hide');
            location.reload();
        });
    }

    function cancelOrder(order_id) {
        $('#cancelOrderModal').modal('show');
        $('#deleteID').val(order_id)

    }

    function saveCancelOrder() {
        let deleteID = $.trim($('#deleteID').val());
        let deleteRemark = $.trim($('#deleteRemark').val());

        if (deleteRemark != "") {
            $.ajax({
                method: "POST",
                url: "/orders-cancel" + '?_token=' + '{{ csrf_token() }}',
                data: {
                    deleteID: deleteID,
                    deleteRemark: deleteRemark,
                }
            }).done(function(data) {
                $('#deleteID').val('')
                $('#deleteRemark').val('')
                $('#cancelOrderModal').modal('hide');
                location.reload();
            });
        }
    }

    function updateOrderModal(order_id, status_id, status, type) {
        $('#updateOrderModal').modal('show');
        $('#update_order_id').val(order_id)
        $('#update_status_id').val(status_id)
        $('#update_status').val(status)
        $('#update_status_type').val(type)
        $(`#${status}`).show()

        if (status == 'delivered') {
            $.ajax({
                method: "GET",
                url: "/main-data",
            }).done(function(data) {
                $("#divtrans").html(data);
            });
        }
    }

    function prepareUpdateStatus() {
        let update_order_id = $.trim($('#update_order_id').val());
        let update_status_id = $.trim($('#update_status_id').val());
        let update_status = $.trim($('#update_status').val());
        let update_status_type = $.trim($('#update_status_type').val());
        let data = {}

        if (update_status == 'order_cn') {
            data.OrderNumber = $('#OrderNumber').val()
            data.OrderDate = $('#OrderDate').val()
            data.AdminAccount = $('#AdminAccount').val()
        } else if (update_status == 'trans_to_thai') {
            data.WarehouseCNDate = $('#WarehouseCNDate').val()
            data.ExportDate = $('#ExportDate').val()
            data.PurchaseOrder = $('#PurchaseOrder').val()
            data.Container = $('#Container').val()
            data.Width = $('#Width').val()
            data.Length = $('#Length').val()
            data.Height = $('#Height').val()
            data.Queue = $('#Queue').val()
            data.KG = $('#KG').val()
            data.TotalBox = $('#TotalBox').val()
        } else if (update_status == 'thai_received') {
            data.ImportDate = $('#ImportDate').val()
        } else if (update_status == 'delivered') {
            data.DeliveryDate = $('#DeliveryDate').val()
            data.ShipComID = $('#ShipComID').val()
            data.TrackingNO = $('#TrackingNO').val()
        }
        updateStatus(update_order_id, update_status_id, data, update_status_type)
    }

    function closeUpdateStatus() {

        $('#order_cn').hide()
        $('#OrderNumber').val('')
        $('#OrderDate').val('')
        $('#AdminAccount').val('')

        $('#trans_to_thai').hide()
        $('#WarehouseCNDate').val('')
        $('#ExportDate').val('')
        $('#PurchaseOrder').val('')
        $('#Container').val('')
        $('#Width').val('')
        $('#Length').val('')
        $('#Height').val('')
        $('#Queue').val('')
        $('#KG').val('')
        $('#TotalBox').val('')

        $('#thai_received').hide()
        $('#ImportDate').val('')

        $('#delivered').hide()
        $('#DeliveryDate').val('')
        $('#ShipComID').val('')
        $('#TrackingNO').val('')

        $('#updateOrderModal').modal('hide');
    }

    function updateStatus(order_id, status_id, data, type) {
        // console.log(data); 
        $.ajax({
            method: "POST",
            url: `/orders-send-status/${order_id}/${status_id}/${type}` + '?_token=' + '{{ csrf_token() }}',
            data: data
        }).done(function(data) {
            location.reload();
        });

    }
</script>

@endsection