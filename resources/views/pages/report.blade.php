@extends('layouts.app')
@section('title')
<title>รายการคำสั่งซื้อ</title>
@endsection

@section('content')
@if($message = Session::get('success'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {{ $message }}
</div>
@endif

<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h1 class="h3 mb-0 text-gray-800">รายงานสรุปคำสั่งซื้อตามช่วงวันที่</h1>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <strong>เลือกช่วงวันที่</strong>
                            <input class="form-control" id="dateRangePicker" placeholder="กรุณาเลือกช่วงวันที่.." />
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <div class="form-check form-check-solid">
                                <input class="form-check-input" name="DetailChk1" id="DetailChk1" type="checkbox">
                                <label class="form-check-label" for="DetailChk1">ดึงรายงานพร้อมรายละเอียดของแต่ละคำสั่งซื้อ</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <button type="button" onclick="getReport1()" class="btn btn-success">ดึงรายงาน</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-header">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h1 class="h3 mb-0 text-gray-800">รายงานสรุปคำสั่งซื้อของลูกค้า</h1>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-12">
                        <div class="form-group">
                            <strong>ชื่อลูกค้า</strong>
                            <select name="CstID" id="CstID" class="form-control">
                                <option value="">กรุณาเลือกชื่อลูกค้า...</option>
                                @foreach($cst as $key => $val)
                                <option value="{{ $val->UserID }}">{{ $val->CstID.' '.$val->CstFirstname.' ('.$val->CstCompanyName.')' }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <div class="form-check form-check-solid">
                                <input class="form-check-input" name="DetailChk2" id="DetailChk2" type="checkbox">
                                <label class="form-check-label" for="DetailChk2">ดึงรายงานพร้อมรายละเอียดของแต่ละคำสั่งซื้อ</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-12">
                        <div class="form-group">
                            <button type="button" onclick="getReport2()" class="btn btn-success">ดึงรายงาน</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')

<script>
    $(document).ready(function() {
        $('#CstID').select2();
        const picker = new Litepicker({
            element: document.getElementById('dateRangePicker'),
            singleMode: false
        });
    });

    getReport1 = () => {
        let date = $("#dateRangePicker").val()
        let chk = $("#DetailChk1").prop('checked')
        if (date != "") {
            $.ajax({
                method: "POST",
                url: `/report/export` + '?_token=' + '{{ csrf_token() }}',
                data: {
                    date: date,
                    chk: chk,
                    cstid: ""
                }
            }).done(function(res) {
                // alert(res)
                window.open(`https://api.bananathecargo.com/file/${res}`)
            });
        }
    }

    getReport2 = () => {
        let cstid = $("#CstID").val()
        let chk = $("#DetailChk2").prop('checked')
        if (CstID != "") {
            $.ajax({
                method: "POST",
                url: `/report/export` + '?_token=' + '{{ csrf_token() }}',
                data: {
                    date: "",
                    cstid: cstid,
                    chk: chk,
                }
            }).done(function(res) {
                window.open(`https://api.bananathecargo.com/file/${res}`)
            });
        }
    }
</script>

@endsection