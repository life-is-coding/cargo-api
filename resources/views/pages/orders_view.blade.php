<div class="row">
    <div class="col-4 text-left">
        <b>
            <!-- <a href="{{ route('orders-quotation',$data->ID) }}" target="_blank" rel="noopener noreferrer"> -->
            <h5>{{ $data->No }}</h5>
            <!-- </a> -->
        </b>
    </div>
    <div class="col-4 text-left">
        <h5>{{ $data->AdminStatus }}</h5>
    </div>
    <div class="col-4 text-right">
        <h5>อัตราแลกเปลี่ยน 1 หยวน : {{ $data->ExchangeRate }} บาท </h5>
    </div>
</div>
<hr />
<div class="row">
    <div class="col-4">
        <div class="row">
            <div class="col-12">
                <b>
                    <h5>ข้อมูลลูกค้า</h5>
                </b>
            </div>
            <div class="col-12">
                <strong>รหัสลูกค้า</strong> : {{ $data->CstID }}
            </div>
            <div class="col-12">
                <strong>ชื่อลูกค้า</strong> : {{ $data->CstFirstname }} {{ $data->CstLastname }}
            </div>
            <div class="col-12">
                <strong>ประเภทลูกค้า</strong> : {{ $data->CstTypeName }}
            </div>
            <div class="col-12">
                <strong>ชื่อบริษัท</strong> : {{ $data->CstCompanyName }}
            </div>
            <div class="col-12">
                <strong>โทรศัพท์</strong> : {{ $data->CstTel }}
            </div>
            <div class="col-12">
                <strong>อีเมล์</strong> : {{ $data->email }}
            </div>
            <div class="col-12">
                <strong>ที่อยู่</strong> : {{ $data->CstAddress }} {{ $data->CstDistrict }} {{ $data->CstSubdistrict }} {{ $data->CstProvice }} {{ $data->CstZipcode }}
            </div>
        </div>
    </div>
    <div class="col-4">
        <div class="row">
            <div class="col-12">
                <b>
                    <h5>ข้อมูลการสั่งซื้อ</h5>
                </b>
            </div>
            <div class="col-12">
                <strong>ประเภทขนส่ง</strong> : {{ $data->TransName }}
            </div>
            <div class="col-12">
                <strong>ประเภทสินค้า</strong> : {{ $data->ProdName }}
            </div>
            <div class="col-12">
                <strong>รูปแบบการสั่ง</strong> : {{ ($data->OrderType=='Import') ? 'ลูกค้าฝากนำเข้า' : 'ลูกค้าฝากพรีออเดอร์สินค้า' }}
            </div>
            <hr />
            <div class="col-12">
                <strong>Website </strong> : {{ $data->OrderWeb }}
            </div>
            <div class="col-12">
                <strong>บัญชีแอดมิน</strong> : {{ $data->AdminAccount }}
            </div>
            <div class="col-12">
                <strong>เลขที่คำสั่งซื้อ (ร้านค้า)</strong> : {{ $data->OrderNumber }}
            </div>
            <div class="col-12">
                <strong>วันที่สั่งซื้อ (ร้านค้า)</strong> : @if($data->OrderDate) {{ date('d/m/y', strtotime($data->OrderDate)) }} @endif
            </div>

            <hr />
            <!-- <div class="col-12">
                <div class="container-fluid">
                    <div class="row">
                        @foreach($data->images as $key => $value)
                        <div class="col-3">
                            <a href="{{ 'https://api.bananathecargo.com/public/upload/'.$value->file_name }}" target="_blank"><img width="95%" src="{{ 'https://api.bananathecargo.com/public/upload/'.$value->file_name }}" /></a>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div> -->
        </div>
    </div>
    <div class="col-4">
        <div class="row">
            <div class="col-12">
                <b>
                    <h5>ข้อมูลพัสดุ</h5>
                </b>
            </div>
            <div class="col-12">
                <strong>น้ำหนัก</strong> : @if($data->KG) {{ $data->KG }} KG @endif
            </div>
            <div class="col-12">
                <strong>ปริมาตร</strong> : @if($data->Queue) {{ $data->Queue }} CBM @endif
            </div>
            <div class="col-12">
                <strong>ขนาด (กว้าง x ยาว x สูง)</strong> : @if($data->Width) {{ $data->Width }} x {{ $data->Length }} x {{ $data->Height }} @endif
            </div>
            <div class="col-12">
                <strong>จำนวนกล่องพัสดุ</strong> : @if($data->TotalBox) {{ $data->TotalBox }} กล่อง @endif
            </div>
            <hr />
            <div class="col-12">
                <strong>ค่าขนส่งจีน-จีน</strong> : @if($data->DeliveryCNCN) {{ number_format($data->DeliveryCNCN,2) }} บาท @endif
            </div>
            <div class="col-12">
                <strong>ค่าขนส่งจีน-ไทย</strong> : @if($data->DeliveryCNTH) {{ number_format($data->DeliveryCNTH,2) }} บาท @endif
            </div>
            <div class="col-12">
                <strong>ค่าขนส่งไทย-ไทย</strong> : @if($data->DeliveryTHTH) {{ number_format($data->DeliveryTHTH,2) }} บาท @endif
            </div>
        </div>
    </div>
</div>
<hr />
<div class="row">
    <div class="col-12">
        <table class="table table-bordered" id="tblQU">
            <thead class="thead-dark">
                <tr>
                    <th align="center" width="3%">#</th>
                    <th align="center" width="15%">วันที่เสนอราคา</th>
                    <th align="center" width="20%">รายละเอียด</th>
                    <th align="center" width="15%">จำนวนเงิน (บาท)</th>
                    <th align="center" width="25%">สถานะการเสนอราคา</th>
                    <th align="center" width="22%"></th>
                </tr>
            </thead>
            <tbody>
                @if(count($data->qu) > 0)
                @foreach($data->qu as $nkey => $each)
                <tr>
                    <td align="center">{{$nkey+1}}</td>
                    <td align="center">{{ date("d/m/Y H:i",strtotime($each->created_at)) }}</td>
                    <td>
                        @if($each->Product == "Y")
                        ค่าสินค้า
                        @endif

                        @if($each->Delivery != "" )
                        ค่าจัดส่ง
                        @endif
                    </td>
                    <td align="right">{{$each->Total}}</td>
                    <td align="center">
                        {{ $each->qu_status_name }}
                    <td align="center">
                        <a href="{{ route('orders-quotation',$each->ID) }}" target="_blank" rel="noopener noreferrer">
                            <button type="button" class="btn-info btn-sm">พิมพ์</button>
                        </a>
                        @if($each->qu_status_name == "ลูกค้าตกลงเสนอราคา")
                        <a href="{{ 'https://api.bananathecargo.com/public/upload/'.$value->file_name }}" target="_blank" rel="noopener noreferrer">
                            <button type="button" class="btn-secondary btn-sm">หลักฐานการชำระเงิน</button>
                        </a>
                        @endif
                    </td>
                </tr>
                @endforeach
                @endif
            </tbody>
        </table>
    </div>
</div>
<hr />
<div class="row">
    <div class="col-12">
        <b>
            <h5>ข้อมูลการจัดส่ง</h5>
        </b>
    </div>

    @foreach($delivery as $key => $resDlv)
    <div class="col-12">
        <div class="row">
            <div class="col-12">
                <div class="row">
                    <div class="col-4">
                        <strong>ค่าจัดส่งจีน-จีน</strong> : {{ $resDlv->DeliveryCNCN }}
                    </div>
                    <div class="col-4">
                        <strong>ค่าจัดส่งจีน-ไทย</strong> : {{ $resDlv->DeliveryCNTH }}
                    </div>
                    <div class="col-4">
                        <strong>ค่าจัดส่งไทย-ไทย</strong> : {{$resDlv->DeliveryTHTH }}
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="row">
                    <div class="col-12">
                        <strong>เลขที่จัดส่ง (ร้านค้า)</strong> : {{ $resDlv->OrderTracking }}
                    </div>
                    <div class="col-12">
                        <strong>วันที่ของถึงโกดังจีน</strong> : @if($resDlv->WarehouseCNDate) {{ date('d/m/y', strtotime($resDlv->WarehouseCNDate)) }} @endif
                    </div>
                    <div class="col-12">
                        <strong>วันที่ส่งออกจากโกดังจีน</strong> : @if($resDlv->ExportDate) {{ date('d/m/y', strtotime($resDlv->ExportDate)) }} @endif
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="row">
                    <div class="col-12">
                        <strong>เลขที่บิลใบสั่งซื้อ (PO)</strong> : {{ $resDlv->PurchaseOrder }}
                    </div>
                    <div class="col-12">
                        <strong>ตู้ที่</strong> : {{ $resDlv->Container }}
                    </div>
                    <div class="col-12">
                        <strong>วันที่ของถึงโกดังไทย</strong> : @if($resDlv->ImportDate) {{ date('d/m/y', strtotime($resDlv->ImportDate)) }} @endif
                    </div>
                </div>
            </div>
            <div class="col-4">
                <div class="row">
                    <div class="col-12">
                        <strong>วันที่จัดส่งหาลูกค้า</strong> : @if($resDlv->DeliveryDate) {{ date('d/m/y', strtotime($resDlv->DeliveryDate)) }} @endif
                    </div>
                    <div class="col-12">
                        <strong>บริษัทขนส่ง</strong> : {{ $resDlv->CompanyName }}
                    </div>
                    <div class="col-12">
                        <strong>เลขที่พัสดุจัดส่ง</strong> : {{ $resDlv->TrackingNO }}
                    </div>
                </div>
            </div>
        </div>
        <hr />
    </div>
    @endforeach
</div>
<hr />
<div class="row">
    <div class="col-xs-12 table-responsive table_wrapper">
        <table class="table table-bordered" id="tblDetail">
            <thead class="thead-dark">
                <tr>
                    <th align="center" width="20%">รูป</th>
                    <th align="center" width="20%">ชื่อสินค้า</th>
                    <th align="center" width="10%">ราคา/ชิ้น (หยวน)</th>
                    <th align="center" width="10%">จำนวน</th>
                    <th align="center" width="10%">ราคารวม (หยวน)</th>
                    <th align="center" width="10%">ราคารวม (บาท)</th>
                    <th align="center" width="20%">หมายเหตุ</th>
                </tr>
            </thead>
            <tbody>
                @foreach($data->detail as $key => $value)
                <tr>
                    <td>
                        <a href="{{ $value->Pic }}" target="_blank"><img width="100%" src="{{ $value->Pic }}" /></a>
                    </td>
                    <td>
                        {{ $value->ItemName }}
                        <br />
                        <b class="text-danger">{{ $value->Detail }}</b>
                        <br />
                        <a href="{{ $value->Link }}" target="_blank">Link สินค้า</a>
                    </td>
                    <td>{{ $value->Price }}</td>
                    <td>{{ $value->Qty }}</td>
                    <td align="right">{{ number_format($value->TotalLine,2) }}</td>
                    <td align="right">{{ number_format($value->TotalLine * $data->ExchangeRate,2) }}</td>
                    <td>{{ $value->Remark }}</td>
                </tr>
                @endforeach
                <tr class="thead-dark">
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td align="right"><b>{{ number_format($data->sum_price_cn,2) }}</b></td>
                    <td align="right"><b>{{ number_format($data->sum_price_cn * $data->ExchangeRate,2) }}</b></td>
                    <td></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>