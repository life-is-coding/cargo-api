@extends('layouts.app')

@section('title')
<title>แก้ไขข้อมูลลูกค้า</title>
@endsection
@section('content')
<div class="card">
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h1 class="h3 mb-0 text-gray-800">{{ ($StaPage == 'create') ? 'เพิ่ม' : 'แก้ไข' }}ลูกค้า</h1>
                    <a href="{{ route('customers') }}" class="btn btn-primary">กลับ</a>
                </div>
            </div>
            <div class="card-body">
                @if($errors->any())
                <div class="alert alert-danger">
                    <strong>Whoop!</strong>
                    There were some problems with your input. <br><br>
                    <ul>
                        @foreach(@errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif
                <form action="{{ ($StaPage == 'create') ? route('customers-store') : route('customers-update',$data[0]->ID) }}" method="post">
                    @csrf
                    <input type="hidden" name="UserID" value="{{ ($StaPage == 'edit') ? $data[0]->UserID : '' }}">
                    @if($StaPage == 'edit')
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>รหัสลูกค้า</strong>
                                <input type="text" name="CstID" class="form-control" placeholder="กรอกรหัสลูกค้า" value="{{ ($StaPage == 'edit') ? $data[0]->CstID : '' }}" maxlength="50" required>
                            </div>
                        </div>
                    </div>
                    @endif
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>ชื่อลูกค้า</strong>
                                <input type="text" name="CstFirstname" class="form-control" placeholder="กรอกชื่อ" value="{{ ($StaPage == 'edit') ? $data[0]->CstFirstname : '' }}" maxlength="50" required>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>นามสกุล</strong>
                                <input type="text" name="CstLastname" class="form-control" placeholder="กรอกนามสกุล" value="{{ ($StaPage == 'edit') ? $data[0]->CstLastname : '' }}" maxlength="50" required>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>ประเภทลูกค้า</strong>
                                <select name="CstTypeId" id="CstTypeId" class="form-control">
                                    <option value="">- เลือกประเภทลูกค้า-</option>
                                    @foreach($cst_type as $key => $val)
                                    <option @if($StaPage=='edit' ) {{ ($data[0]->CstTypeId == $val->cst_type_id) ? "selected" : "" }} @endif value="{{ $val->cst_type_id }}">{{ $val->cst_type_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-lg-3 pt-4">
                            <div class="form-group">
                                <div class="custom-control custom-checkbox">
                                    <input type="checkbox" value="Y" class="custom-control-input" @if($StaPage=='edit' ) {{ ($data[0]->CstStatus == '1') ? 'checked' : '' }} @else checked @endif id="CstStatus" name="CstStatus">
                                    <label class="custom-control-label" for="CstStatus">ใช้งาน</label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>ชื่อบริษัท</strong>
                                <input type="text" name="CstCompanyName" class="form-control" placeholder="กรอกชื่อบริษัท" value="{{ ($StaPage == 'edit') ? $data[0]->CstCompanyName : '' }}" maxlength="100">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>โทรศัพท์</strong>
                                <input type="text" name="CstTel" class="form-control" placeholder="กรอกโทรศัพท์" value="{{ ($StaPage == 'edit') ? $data[0]->CstTel : '' }}" maxlength="50">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>ชื่อผู้ใช้</strong>
                                <input type="text" name="name" class="form-control" value="{{ ($StaPage == 'edit') ? $data[0]->name : '' }}" placeholder="กรอกชื่อผู้ใช้" maxlength="25" required>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>อีเมล์</strong>
                                <input type="email" name="email" class="form-control" value="{{ ($StaPage == 'edit') ? $data[0]->email : '' }}" placeholder="กรอกอีเมล์" maxlength="50">
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>รหัสผ่าน</strong>
                                <input type="password" name="password" {{ ($StaPage == 'create') ? 'required' : '' }} class="form-control" value="" placeholder="กรอกรหัสผ่าน" maxlength="15">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <strong>ที่อยู่</strong>
                                <textarea name="CstAddress" class="form-control" rows="2" placeholder="กรอกที่อยู่" maxlength="255">{{ ($StaPage == 'edit') ? $data[0]->CstAddress : '' }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>จังหวัด</strong>
                                <input type="text" name="CstProvice" class="form-control" placeholder="กรอกจังหวัด" value="{{ ($StaPage == 'edit') ? $data[0]->CstProvice : '' }}" maxlength="100" required>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>เขต/อำเภอ</strong>
                                <input type="text" name="CstDistrict" class="form-control" placeholder="กรอกเขต/อำเภอ" value="{{ ($StaPage == 'edit') ? $data[0]->CstDistrict : '' }}" maxlength="100" required>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>แขวง/ตำบล</strong>
                                <input type="text" name="CstSubdistrict" class="form-control" placeholder="กรอกแขวง/ตำบล" value="{{ ($StaPage == 'edit') ? $data[0]->CstSubdistrict : '' }}" maxlength="100" required>
                            </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="form-group">
                                <strong>รหัสไปรษณีย์</strong>
                                <input type="text" name="CstZipcode" class="form-control" placeholder="กรอกรหัสไปรษณีย์" value="{{ ($StaPage == 'edit') ? $data[0]->CstZipcode : '' }}" maxlength="100" required>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group">
                                <strong>หมายเหตุ</strong>
                                <textarea name="Remark" id="Remark" maxlength="250"  placeholder="กรอกหมายเหตุ" class="form-control" rows="3">{{ ($StaPage == 'edit') ? $data[0]->Remark : '' }}</textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-primary">บันทึก</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@endsection

@section('javascript')

<script>

</script>

@endsection