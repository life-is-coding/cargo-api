<table class="table table-bordered table-hover" id="dataTable">
    <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">ข้อมูลลูกค้า</th>
            <th scope="col">ข้อมูลการสั่งซื้อ</th>
            <th scope="col">ราคาค่าสินค้า (หยวน)</th>
            <th scope="col">อัตราแลกเปลี่ยน</th>
            <th scope="col">ราคาค่าสินค้า (บาท)</th>
            <th scope="col">สถานะคำสั่งซื้อ</th>
            <th scope="col">จัดการ</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $key => $value)
        <tr>
            <td rowspan="2" scope="row">{{ $key + 1 }}</td>
            <td>
                <h6>{{ $value->CstID}}</h6>
                <b>{{ $value->CstFirstname.' '.$value->CstLastname}}</b><br />
                <small>( {{ $value->CstTypeName}} )</small><br />
                <!-- @if($value->CstCompanyName!="")
                <small>{{ '('.$value->CstCompanyName.')' }}</small><br />
                @endif
                <small>{{ $value->CstTel }}</small><br />
                <small>{{ $value->email }}</small> -->
            </td>
            <td>
                <h6>{{ $value->No}}
                    @if($value->OrderType == 'Import')
                    <span class="badge badge-warning">{{ $value->OrderType}}</span>
                    @endif
                    @if($value->OrderType == 'Preorder')
                    <span class="badge badge-primary">{{ $value->OrderType}}</span>
                    @endif
                </h6>
                <b>วันที่สั่งซื้อ : </b>{{ date('d/m/y H:i', strtotime($value->created_at )) }}<br />
                <b>ประเภทขนส่ง : </b>{{ $value->TransName }}<br />
                <b>ประเภทสินค้า : </b>{{ $value->ProdName }}
            </td>
            <td align="right">{{ $value->total_cn }}</td>
            <td align="right">{{ $value->ExchangeRate }}</td>
            <td align="right">{{ $value->total_th }}</td>
            <td>
                {{ $value->AdminStatus }}
                @if(Auth::user()->role=='superadmin')
                <br />
                <small>
                    <b>owner : </b>{{ $value->AdminName }}
                </small>
                @endif
            </td>
            <td>

                <button title="แสดงรายละเอียดคำสั่งซื้อ" class="btn btn-sm btn-primary btn-icon" type="button" onclick="viewOrder( {{ $value->ID }} )">
                    <i class="fas fa-search"></i>​
                </button>

                <ul class="navbar-nav ml-auto">
                    <div class="topbar-divider d-none d-sm-block"></div>
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="actionsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <button class="btn btn-icon" type="button">
                                <i class="fas fa-ellipsis-v fa-sm fa-fw mr-2 text-gray-400"></i>
                            </button>
                        </a>
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="actionsDropdown">

                            <a class="dropdown-item" href="{{ route('orders-edit',$value->ID) }}">
                                <i class="fas fa-edit fa-sm fa-fw mr-2 text-gray-400"></i>
                                แก้ไขรายละเอียดคำสั่งซื้อ
                            </a>

                            @if ($value->Orderstatus==2)
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" onclick="updateStatus( {{ $value->ID }} , 3 ,{},'order')">
                                <i class="fas fa-exclamation fa-sm fa-fw mr-2 text-gray-400"></i>
                                รอลูกค้ายืนยันคำสั่งซื้อ
                            </a>
                            @endif

                            @if ($value->Orderstatus==3)
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" onclick="updateStatus( {{ $value->ID }} , 4 ,{},'order')">
                                <i class="fas fa-exclamation fa-sm fa-fw mr-2 text-gray-400"></i>
                                ลูกค้ายืนยันคำสั่งซื้อแล้ว
                            </a>
                            @endif

                            @if ($value->Orderstatus==4)
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" onclick="updateOrderModal( {{ $value->ID }} , 5 ,'order_cn','order')">
                                <i class="fas fa-exclamation fa-sm fa-fw mr-2 text-gray-400"></i>
                                สั่งซื้อสินค้ากับร้านค้าจีนแล้ว
                            </a>
                            @endif

                            @if ($value->Orderstatus==5)
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" onclick="updateStatus( {{ $value->ID }} , 9 ,{},'order')">
                                <i class="fas fa-exclamation fa-sm fa-fw mr-2 text-gray-400"></i>
                                คำสั่งซื้อเสร็จสมบูรณ์แล้ว
                            </a>
                            @endif

                            @if ($value->Orderstatus==1 || $value->Orderstatus==2)
                            <div class="dropdown-divider"></div>
                            <a class="dropdown-item" onclick="cancelOrder( {{ $value->ID }} )">
                                <i class="fas fa-times fa-sm fa-fw mr-2 text-gray-400"></i>
                                ยกเลิกคำสั่งซื้อ
                            </a>
                            @endif

                        </div>

                    </li>
                </ul>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                @foreach($value->qu as $key3 => $value3)
                เสนอราคา
                @if($value3->Product == "Y")
                ค่าสินค้า
                @endif

                @if($value3->Delivery != "" )
                ค่าจัดส่ง
                @endif
                -
                {{ $value3->qu_status_name }}
                <br />
                @endforeach
            </td>
            <td colspan="4">
                @foreach($value->delivery as $key2 => $value2)
                <b>{{ $value2->OrderTracking }} </b> - {{ $value2->del_status_name }}
                @if ($value2->del_status_id==1)
                <a title="สินค้าส่งออกจากจีนแล้ว" onclick="updateOrderModal( {{ $value2->ID }} , 2 ,'trans_to_thai','delivery')">
                    <span style="cursor:pointer" class="badge badge-secondary">อัพเดทสถานะ</span>
                </a>
                @endif
                @if ($value2->del_status_id==2)
                <a title="สินค้าถึงโกดังไทยแล้ว" onclick="updateOrderModal( {{ $value2->ID }} , 3 ,'thai_received','delivery')">
                    <span style="cursor:pointer" class="badge badge-secondary">อัพเดทสถานะ</span>
                </a>
                @endif
                @if ($value2->del_status_id==3)
                <a title="ลูกค้าชำระค่าขนส่งแล้ว" onclick="updateStatus( {{ $value2->ID }} , 4 ,{},'delivery')">
                    <span style="cursor:pointer" class="badge badge-secondary">อัพเดทสถานะ</span>
                </a>
                @endif
                @if ($value2->del_status_id==4)
                <a title="จัดส่งสินค้าให้ลูกค้าแล้ว" onclick="updateOrderModal( {{ $value2->ID }} , 5 ,'delivered','delivery')">
                    <span style="cursor:pointer" class="badge badge-secondary">อัพเดทสถานะ</span>
                </a>
                @endif
                <br />
                @endforeach
            </td>
        </tr>
        @endforeach
    </tbody>
</table>