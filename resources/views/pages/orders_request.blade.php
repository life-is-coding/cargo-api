<table class="table table-bordered table-hover" id="dataTable">
    <thead class="thead-dark">
        <tr>
            <th scope="col">#</th>
            <th scope="col">ข้อมูลลูกค้า</th>
            <th scope="col">ข้อมูลการสั่งซื้อ</th>
            <th scope="col">รายละเอียดสินค้า</th>
            <th scope="col">ราคา (หยวน)</th>
            <th scope="col">อัตราแลกเปลี่ยน</th>
            <th scope="col">ราคา (บาท)</th>
            <th scope="col">เลือก</th>
        </tr>
    </thead>
    <tbody>
        @foreach($data as $key => $value)
        <tr>
            <th scope="row">{{ $key + 1 }}</th>
            <td>
                <h6>{{ $value->CstID}}</h6>
                <b>{{ $value->CstFirstname.' '.$value->CstLastname}}</b><br />
                <small>( {{ $value->CstTypeName}} )</small><br />
                @if($value->CstCompanyName!="")
                <small>{{ '('.$value->CstCompanyName.')' }}</small><br />
                @endif
                <small>{{ $value->CstTel }}</small><br />
                <small>{{ $value->email }}</small><br />
            </td>
            <td>
                <h6>{{$value->No}}</h6>
                <b>วันที่สั่งซื้อ : </b>{{ date('d/m/y', strtotime($value->created_at)) }}<br />
                <b>ประเภทขนส่ง : </b>{{ $value->TransName }}<br />
                <b>ประเภทสินค้า : </b>{{ $value->ProdName }}
            </td>
            <td>
                @foreach($value->detail as $key2 => $value2)
                <small>{{$key2+1}}. {{ $value2->ItemName }} ({{ $value2->Remark }})</small><br />
                @endforeach
            </td>
            <td align="right">{{ $value->total_cn }}</td>
            <td align="right">{{ $value->ExchangeRate }}</td>
            <td align="right">{{ $value->total_th }}</td>
            <td>
                <input type="checkbox" class="checkOrder" name="checkOrder" id="checkOrder" value="{{$value->ID}}" />
            </td>
        </tr>
        @endforeach
    </tbody>
</table>