<div class="col-12">
    <h6>สถานะคำสั่งซื้อ</h6>
</div>
@foreach($data as $key => $value)
<div class="col-xl-3 col-md-6" id="status_{{ $value->Orderstatus }}" onclick="getOrderTable( {{ $value->Orderstatus }} )">
    <div class="card {{ $value->Color }} shadow pointerclick">
        <div class="card-body py-0">
            <div class="row no-gutters align-items-center">
                <div class="col mr-4">
                    <div class="h6 mb-0 font-weight-bold text-gray-800">
                        {{ $value->AdminStatus }}
                    </div>
                </div>
                <div class="col-auto h2 mb-0">
                    {{ $value->num_order }}
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach
<div class="col-12 mt-3">
    <h6>สถานะการจัดส่ง</h6>
</div>
@foreach($delivery as $key => $value)
<div class="col-xl-3 col-md-6" id="del_status" onclick="getOrderTable( 5,{{ $value->del_status_id }} )">
    <div class="card {{ $value->del_color }} shadow pointerclick">
        <div class="card-body py-0">
            <div class="row no-gutters align-items-center">
                <div class="col mr-4">
                    <div class="h6 mb-0 font-weight-bold text-gray-800">
                        {{ $value->del_status_name }}
                    </div>
                </div>
                <div class="col-auto h2 mb-0">
                    {{ $value->num_del }}
                </div>
            </div>
        </div>
    </div>
</div>
@endforeach