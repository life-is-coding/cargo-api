@extends('layouts.app')

@section('title')
<title>ข้อมูลลูกค้า</title>
@endsection
@section('content')
@if($message = Session::get('success'))
<div class="alert alert-success alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {{ $message }}
</div>
@endif
<div class="card">
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h1 class="h3 mb-0 text-gray-800">ข้อมูลลูกค้า</h1>
                    <a href="{{ route('customers-create') }}" class="btn btn-success">เพิ่ม</a>
                </div>
            </div>
            <div class="card-body">
                <table class="table table-bordered table-hover" id="dataTable">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">รหัสลูกค้า</th>
                            <th scope="col">ชื่อลูกค้า</th>
                            <th scope="col">ประเภทลูกค้า</th>
                            <th scope="col">เบอร์โทรศัพท์</th>
                            <th scope="col">E-mail</th>
                            <th scope="col">สถานะ</th>
                            <th scope="col">จัดการ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data as $key => $value)
                        <tr>
                            <th scope="row">{{ $key + 1 }}</th>
                            <td>{{ $value->CstID }}</td>
                            <td>{{ $value->CstFirstname.' '.$value->CstLastname }}</td>
                            <td>{{ $value->cst_type_name }}</td>
                            <td>{{ $value->CstTel }}</td>
                            <td>{{ $value->email }}</td>
                            <td>{{ $value->CstStatus == '1' ? '' : '' }}
                                @php
                                if($value->user_status == '1'){
                                echo "ยังไม่ได้ยืนยัน E-mail" ;
                                }else if ($value->CstStatus == '1'){
                                echo "ใช้งาน" ;
                                }else {
                                echo "ไม่ใช้งาน" ;
                                }
                                @endphp
                            </td>
                            <td>
                                <ul class="navbar-nav ml-auto">
                                    <div class="topbar-divider d-none d-sm-block"></div>
                                    <li class="nav-item dropdown no-arrow">
                                        <a class="nav-link dropdown-toggle" href="#" id="actionsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            <button class="btn btn-icon" type="button">
                                                <i class="fas fa-ellipsis-v fa-sm fa-fw mr-2 text-gray-400"></i>
                                            </button>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="actionsDropdown">

                                            @if($value->user_status == '1')
                                            <a href="{{ route('customers-resend-email',$value->UserID) }}" class="dropdown-item">
                                                <i class="fas fa-envelope fa-sm fa-fw mr-2 text-gray-400"></i>
                                                ส่งอีเมล์ยืนยันใหม่อีกครั้ง
                                            </a>
                                            <div class="dropdown-divider"></div>
                                            <a href="{{ route('customers-confirm-email',$value->UserID) }}" class="dropdown-item">
                                                <i class="fas fa-check fa-sm fa-fw mr-2 text-gray-400"></i>
                                                ยืนยัน Account ให้ลูกค้า
                                            </a>
                                            <div class="dropdown-divider"></div>
                                            @endif

                                            <a href="{{ route('customers-edit',$value->ID) }}" class="dropdown-item">
                                                <i class="fas fa-edit fa-sm fa-fw mr-2 text-gray-400"></i>
                                                แก้ไขข้อมูลลูกค้า
                                            </a>
                                            <div class="dropdown-divider"></div>
                                            <a href="{{ route('customers-orders',$value->UserID) }}" class="dropdown-item">
                                                <i class="fas fa-file fa-sm fa-fw mr-2 text-gray-400"></i>
                                                ดูรายละเอียดคำสั่งซื้อของลูกค้า
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('javascript')

<script>
    $(document).ready(function() {
        $('#dataTable').DataTable();
    });
</script>

@endsection