@extends('layouts.app')

@section('title')
<title>ข้อมูลคำสั่งซื้อของลูกค้า</title>
@endsection
@section('content')
<div class="card">
    <div class="row">
        <div class="col-lg-12">
            <div class="card-header">
                <div class="d-sm-flex align-items-center justify-content-between">
                    <h1 class="h3 mb-0 text-gray-800">ข้อมูลคำสั่งซื้อของลูกค้า</h1>
                    <a href="{{ route('customers') }}" class="btn btn-primary">กลับ</a>
                </div>
            </div>
            <div class="card-body">
                <div class="container">
                    <div class="row">
                        <div class="col-4">
                            <b>รหัสลูกค้า : </b>{{ $customer->CstID}}
                        </div>
                        <div class="col-8">
                            <b>ประเภทลูกค้า : </b>{{ $customer->cst_type_name}}
                        </div>
                        <div class="col-4">
                            <b>ชื่อลูกค้า : </b>{{ $customer->CstFirstname.' '.$customer->CstLastname}}
                        </div>
                        <div class="col-8">
                            <b>ชื่อบริษัท : </b>{{ $customer->CstCompanyName}}
                        </div>
                        <div class="col-4">
                            <b>โทรศัพท์ : </b>{{ $customer->CstTel}}
                        </div>
                        <div class="col-8">
                            <b>Email : </b>{{ $customer->email}}
                        </div>
                        <div class="col-12">
                            <b>ที่อยู่ : </b>{{ $customer->CstAddress }} {{ $customer->CstDistrict }} {{ $customer->CstSubdistrict }} {{ $customer->CstProvice }} {{ $customer->CstZipcode }}
                        </div>
                    </div>
                </div>
                <hr />

                <table class="table table-bordered table-hover" id="dataTable">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <!-- <th scope="col">ข้อมูลลูกค้า</th> -->
                            <th scope="col">ข้อมูลการสั่งซื้อ</th>
                            <th scope="col">รายละเอียดสินค้า</th>
                            <th scope="col">ราคา (หยวน)</th>
                            <th scope="col">อัตราแลกเปลี่ยน</th>
                            <th scope="col">ราคา (บาท)</th>
                            <th scope="col">สถานะคำสั่งซื้อ</th>
                            <th scope="col">จัดการ</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($orders as $key => $value)
                        <tr>
                            <th scope="row">{{ $key + 1 }}</th>
                            <!-- <td>
                                <b>{{ $value->CstFirstname.' '.$value->CstLastname}}</b><br />
                                @if($value->CstCompanyName!="")
                                <small>{{ '('.$value->CstCompanyName.')' }}</small><br />
                                @endif
                                <small>{{ $value->CstTel }}</small><br />
                                <small>{{ $value->email }}</small>
                            </td> -->
                            <td>
                                <b>
                                    <h6>{{$value->No}}</h6>
                                </b>
                                <b>วันที่สั่งซื้อ : </b>{{ date('d/m/y', strtotime($value->created_at)) }}<br />
                                <b>ประเภทขนส่ง : </b>{{ $value->TransName }}<br />
                                <b>ประเภทสินค้า : </b>{{ $value->ProdName }}
                            </td>
                            <td>
                                @foreach($value->detail as $key2 => $value2)
                                <small>{{$key2+1}}. {{ $value2->ItemName }} ({{ $value2->Remark }})</small><br />
                                @endforeach
                            </td>
                            <td align="right">{{ $value->sum_price_cn }}</td>
                            <td align="right">{{ $value->ExchangeRate }}</td>
                            <td align="right">{{ number_format($value->sum_price_cn*$value->ExchangeRate,2) }}</td>
                            <td>
                                {{ $value->AdminStatus }}
                                @if(Auth::user()->role=='superadmin')
                                <br />
                                <small>
                                    <b>owner : </b>{{ $value->AdminName }}
                                </small>
                                @endif
                            </td>
                            <td>
                                <button class="btn btn-teal btn-icon" type="button" onclick="viewOrder( {{ $value->ID }} )">
                                    <i class="fas fa-search"></i>
                                </button>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection


@section('javascript')

<script>
    $(document).ready(function() {
        $('#dataTable').DataTable({
            "ordering": false
        });
    });
</script>

@endsection