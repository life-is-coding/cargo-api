<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderFiles extends Model
{
    use HasFactory;
    public  $table = 'order_files';
    public  $key = 'file_id';
    public  $timestamps = false;

    protected $fillable = ['order_id', 'file_name', 'file_comment', 'created_at'];
}
