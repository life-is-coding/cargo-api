<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;
    public  $table = 'cart';
    public  $key = 'cart_id';
    public  $timestamps = false;
}
