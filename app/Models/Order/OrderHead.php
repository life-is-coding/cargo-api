<?php

namespace App\Models\Order;

use DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderHead extends Model
{
    use HasFactory;
    public  $table = 'order_head';
    public  $key = 'ID';
    public  $timestamps = false;

    public function detail()
    {
        return $this->hasMany('App\Models\Order\OrderItem', 'OrdID', 'ID');
    }
    public function items()
    {
        return $this->hasMany('App\Models\Order\OrderItem', 'OrdID', 'ID')
            ->select(
                'OrdID',
                'ItemName as item_name',
                'Detail as item_detail',
                'Link as item_url',
                'Pic as img_url',
                'Remark as remark',
                DB::raw('CAST(Price AS DECIMAL (10,2)) as price_cny'),
                'Qty as qty',
                DB::raw('CAST(TotalLine AS DECIMAL (10,2)) as total_cny'),
                DB::raw('CAST(TotalLineTHB AS DECIMAL (10,2)) as total_thb'),
            );
    }

    public function qu()
    {
        return $this->hasMany('App\Models\Order\Quotation', 'OrderID', 'ID')
            ->with('images')
            ->join('quotation_status', 'qu_status_id', 'Status')
            ->select(
                'ID',
                'OrderID',
                'Product',
                'Delivery',
                'Status',
                'Remark',
                DB::raw("DATE_FORMAT(created_at,'%e/%m/%y') as qu_date"),
                DB::raw('CAST(Total AS DECIMAL (10,2)) as Total'),
                'qu_status_name',
                'created_at'
            );
    }

    public function delivery()
    {
        return $this->hasMany('App\Models\Order\OrderDelivery', 'OrdID', 'ID')
            ->join('delivery_status', 'del_status_id', 'DeliveryStatus')
            ->leftjoin('shipping_company AS sh', 'sh.ID', '=', 'ShipComID')
            ->select(
                'order_delivery.*',
                'del_status_name',
                'del_status_id',
                'OrderTracking as cn_order_tracking',
                DB::raw("DATE_FORMAT(WarehouseCNDate,'%e/%m/%y') as warehouse_date"),
                DB::raw("DATE_FORMAT(ExportDate,'%e/%m/%y') as export_date"),
                'PurchaseOrder as po_number',
                'Container as container_no',
                DB::raw("DATE_FORMAT(ImportDate,'%e/%m/%y') as import_date"),
                'TrackingNO as delivery_tracking_no',
                'sh.CompanyName as delivery_company',
                DB::raw("DATE_FORMAT(DeliveryDate,'%e/%m/%y') as delivery_date"),
                'KG as kg',
                'Queue as cbm',
                'Width as width',
                'Length as length',
                'Height as height',
                DB::raw('CAST(DeliveryCNCN AS DECIMAL (10,2)) as delivery_cn_cn'),
                DB::raw('CAST(DeliveryCNTH AS DECIMAL (10,2)) as delivery_cn_th'),
                DB::raw('CAST(DeliveryTHTH AS DECIMAL (10,2)) as delivery_th_th')
            );
    }

    public function images()
    {
        return $this->hasMany('App\Models\Order\OrderFiles', 'order_id', 'ID');
    }
}
