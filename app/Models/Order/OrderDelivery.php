<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDelivery extends Model
{
    use HasFactory;
    public  $table = 'order_delivery';
    public  $key = 'id';
    public  $timestamps = false;
}
