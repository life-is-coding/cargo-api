<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderMain extends Model
{
    use HasFactory;
    public  $table = 'order_head';
    public  $key = 'ID';
    public  $timestamps = false;
}
