<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderDetail1 extends Model
{
    use HasFactory;
    public  $table = 'order_detail';
    public  $key = 'id';
    public  $timestamps = false;
}
