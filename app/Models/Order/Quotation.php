<?php

namespace App\Models\Order;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Quotation extends Model
{
    use HasFactory;
    public  $table = 'quotation';
    public  $key = 'ID';
    public  $timestamps = false;

    public function images()
    {
        return $this->hasMany('App\Models\Order\OrderFiles', 'qu_id', 'ID');
    }
}
