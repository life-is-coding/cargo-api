<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Customer extends Model
{
    use HasFactory;
    public  $table = 'customer';
    public  $key = 'CstID';
    public  $timestamps = false;
    
    protected $fillable = [
        'ID',
        'UserID',
        'CstID',
        'CstFirstname',
        'CstLastname',
        'CstCompanyName',
        'CstTel',
        'CstAddress',
        'CstDistrict',
        'CstSubdistrict',
        'CstProvice',
        'CstZipcode',
    ];

}
