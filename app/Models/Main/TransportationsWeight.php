<?php

namespace App\Models\Main;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransportationsWeight extends Model
{
    use HasFactory;
    public  $table = 'transportations_weight';
    public  $key = 'WeightID';
    public  $timestamps = false;
}
