<?php

namespace App\Models\Main;

use DB;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transportations extends Model
{
    use HasFactory;
    public  $table = 'transportations';
    public  $key = 'TransID';
    public  $timestamps = false;

    public function weight()
    {
        return $this->hasMany('App\Models\Main\TransportationsWeight', 'TransID', 'TransID')
            ->join('products', 'products.ProdID', 'transportations_weight.ProdID')
            ->select('transportations_weight.TransID', 'ProdName', 'KG', 'CBM')
            ->where('ProdStatus', 1)
            ->orderBy(DB::raw("case when ProdName='ทั่วไป' then 1 when ProdName='มอก.' then 2  when ProdName='อย.' then 3 else 4 end"), "asc");
    }
}
