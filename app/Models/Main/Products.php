<?php

namespace App\Models\Main;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
    use HasFactory;

    public  $table = 'products';
    public  $key = 'ProdID';
    public  $timestamps = false;
}
