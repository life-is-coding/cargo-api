<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bank extends Model
{
    use HasFactory;
    protected $table = 'bank_account';
    public  $key = 'bank_id';
    public  $timestamps = false;
    protected $fillable = [
        'acc_name',
        'acc_no',
        'acc_bank'
    ];
}
