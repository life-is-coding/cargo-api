<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Articles extends Model
{
    use HasFactory;
    protected $table = 'articles';
    public  $key = 'article_id';
    public  $timestamps = false;
    protected $fillable = [
        'article_title',
        'article_subtitle',
        'article_link',
        'article_picture',
        'article_status'
    ];
}
