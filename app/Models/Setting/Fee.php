<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Fee extends Model
{
    use HasFactory;
    protected $table = 'transportations_weight';
    protected $fillable = [
        'KG',
        'CBM',
        'ProdID',
        'TransID',
        'WeightStatus',
        'CstTypeId'
    ];
}
