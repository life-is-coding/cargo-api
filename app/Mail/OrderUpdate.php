<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use App\Models\Order\OrderHead;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderUpdate extends Mailable
{
    use Queueable, SerializesModels;
    public $order;
    public $type;
    public $status_id;
    public $delivery;
    public function __construct(OrderHead $order, $type, $status_id, $delivery)
    {
        $this->order = $order;
        $this->type = $type;
        $this->status_id = $status_id;
        $this->delivery = $delivery;
    }

    public function build()
    {
        $email = $this->subject("[NO REPLY] แจ้งอัพเดทสถานะคำสั่งซื้อ " . $this->order->No)
            ->view('email.update');

        return $email;
    }
}
