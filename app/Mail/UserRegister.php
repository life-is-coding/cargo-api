<?php

namespace App\Mail;

use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserRegister extends Mailable
{
    use Queueable, SerializesModels;
    public $user;
    public $token;
    public function __construct(User $user, $token)
    {
        $this->user = $user;
        $this->token = $token;
    }

    public function build()
    {
        $email = $this->subject("[NO REPLY] กรุณายืนยันการสมัครสมาชิก Bananathecargo.com")
            ->view('email.register');

        return $email;
    }
}
