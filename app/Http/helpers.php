<?php

use App\Models\Auth\Customer;
use App\Models\Order\OrderHead;

function genLastOrderNo()
{
    // YYMMxxxx

    $stringstart = "B" . (date("y") + 43) . date("m");
    $last = OrderHead::select('No')->whereNotNull('No')->orderBy('No', 'desc')->first();
    if (isset($last->No)) {
        $last_no = $last->No;

        $year_last = substr($last_no, 1, 2) - 43;
        $month_last = substr($last_no, 3, 2);
        $count_last = substr($last_no, 5, 4);

        $case_num = ($year_last != date("y") || $month_last != date("m")) ? 1 : $count_last + 1;
    } else {
        $case_num = 1;
    }
    $case_num = sprintf("%04d", $case_num);
    $order_no = $stringstart . $case_num;

    return $order_no;
}

function genLastCstID()
{
    // BNN-13XX

    $stringstart = "BNN-";
    $last = Customer::select('CstID')
        ->whereNotNull('CstID')
        ->orderBy('CstID', 'desc')
        ->whereRaw("LENGTH(CstID) = 8")
        ->first();
    if (isset($last->CstID)) {
        $last_no = $last->CstID;
        $count_last = substr($last_no, 4, 4);
        $case_num = $count_last + 1;
    } else {
        $case_num = 1300;
    }
    $cust_id = $stringstart . $case_num;
    return $cust_id;
}

function checkWebsite($link)
{
    $link = strtolower($link);
    if ($link == '') {
        return "";
    } else if (strpos($link, 'taobao')) {
        return "taobao";
    } else if (strpos($link, 'tmall')) {
        return "tmall";
    } else if (strpos($link, '1688')) {
        return "1688";
    } else if (strpos($link, 'alibaba')) {
        return "alibaba";
    }
}
