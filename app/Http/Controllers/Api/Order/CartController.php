<?php

namespace App\Http\Controllers\Api\Order;

use DB;
use App\Models\Order\Cart;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Auth\Customer;
use App\Models\Order\OrderHead;
use App\Models\Setting\Exchange;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class CartController extends Controller
{

    public function manual_save(Request $request)
    {
        $user = $request->user();
        $user_id = $user->id;
        $url = "";
        $file = $request->img_base64;
        if ($file != "") {
            $arr = explode("base64,", $file);
            $base = base64_decode($arr[1]);
            $fileExt = explode('/', explode(':', substr($file, 0, strpos($file, ';')))[1])[1];
            $file_name = Str::random(10) . "." . $fileExt;

            $destinationPath = public_path() . "/images/items/" . $file_name;
            $path_check = public_path() . "/images/items/";
            if (!file_exists($path_check)) {
                mkdir($path_check, 0777, true);
            }
            file_put_contents($destinationPath, $base);
            $url = asset('/images/items/' . $file_name);
        }

        $price = number_format($request->price, 2);
        $qty = $request->qty;
        $price_total = $price * $qty;
        $data = new Cart();
        $data->user_id = $user_id;
        $data->title = $request->title;
        $data->options = $request->options;
        $data->price = $price;
        $data->qty = $qty;
        $data->price_total = $price_total;
        $data->remark = $request->remark;
        $data->img_url = $url;
        $data->detail_url = $request->detail_url;
        $data->added_at = date("Y-m-d H:i:s");
        $data->save();

        return Redirect::to('api/cart/view-all');
    }

    public function save(Request $request)
    {
        $user = $request->user();
        $user_id = $user->id;

        $options = $request->options;
        $option_value = "";
        foreach ($options as $option) {
            $type = $option['key_name'];
            $value = $option['value_name'];
            $option_value .= $type . " : " . $value . " | ";
        }
        $option_value = substr(trim($option_value), 0, -1);

        $chk_data = Cart::where('user_id', $user_id)
            ->where('options', $option_value)
            ->first();

        if (isset($chk_data->cart_id)) {
            $price = number_format($request->price, 2);
            $qty = $request->qty;
            $new_qty = $qty + $chk_data->qty;
            $new_price_total = $price * $new_qty;

            Cart::where('cart_id', $chk_data->cart_id)
                ->update([
                    'qty' => $new_qty,
                    'price_total' => $new_price_total
                ]);
        } else {
            $price = number_format($request->price, 2);
            $qty = $request->qty;
            $price_total = $price * $qty;
            $data = new Cart();
            $data->user_id = $user_id;
            $data->title = $request->title;
            $data->options = $option_value;
            $data->price = $price;
            $data->qty = $qty;
            $data->price_total = $price_total;
            $data->remark = $request->remark;
            $data->img_url = $request->img_url;
            $data->detail_url = $request->detail_url;
            $data->added_at = date("Y-m-d H:i:s");
            $data->save();
        }
        return Redirect::to('api/cart/view-all');
    }

    public function view_all(Request $request)
    {
        $user = $request->user();
        $user_id = $user->id;

        $res = array();
        $res['data'] = Cart::where('user_id', $user_id)
            ->select(
                'cart_id',
                'qty',
                DB::raw('CAST(price AS DECIMAL (10,2)) as price'),
                'title',
                'options',
                'img_url',
                'detail_url',
                DB::raw('CAST(price_total AS DECIMAL (10,2)) as price_total'),
                'remark'
            )
            ->orderBy('added_at')
            ->get();

        $total = Cart::where('user_id', $user_id)
            ->sum('price_total');

        $res['sum'] = number_format($total, 2);

        return response()->json($res);
    }

    public function delete($cart_id)
    {
        Cart::where('cart_id', $cart_id)->delete();
        return Redirect::to('api/cart/view-all');
    }

    public function update(Request $request)
    {
        $cart_id = $request->cart_id;
        $qty = $request->qty;
        $remark = $request->remark;

        $chk_data = Cart::where('cart_id', $cart_id)
            ->first();

        $new_price_total = number_format($qty * $chk_data->price, 2);

        Cart::where('cart_id', $cart_id)
            ->update([
                'qty' => $qty,
                'price_total' => $new_price_total,
                'remark' => $remark
            ]);
        return Redirect::to('api/cart/view-all');
    }

    public function order(Request $request)
    {
        $user = $request->user();
        $user_id = $user->id;
        // $user_email = $user->email;

        $trans_id = $request->trans_id;
        $data_user = $request->data_user;
        $name =  $data_user['name'];
        $lastname =  $data_user['lastname'];
        $tel =  $data_user['tel'];
        $email =  $data_user['email'];
        $address =  $data_user['address'];
        $district =  $data_user['district'];
        $subdistrict =  $data_user['subdistrict'];
        $province =  $data_user['province'];
        $zipcode =  $data_user['zipcode'];

        // $name =  $request->name;
        // $lastname =  $request->lastname;
        // $tel =  $request->tel;
        // $email =  $request->email;
        // $address =  $request->address;
        // $district =  $request->district;
        // $subdistrict =  $request->subdistrict;
        // $province =  $request->province;
        // $zipcode =  $request->zipcode;

        $date = date('Y-m-d H:i:s');
        $ratedata = Exchange::select('THRate')->first();
        $rate = $ratedata->THRate;

        $cart = Cart::where('user_id', $user_id)->get();
        $userdata = Customer::select('CstCompanyName')->where('UserID', $user_id)->first();

        $order_no = genLastOrderNo();

        $headID = DB::table('order_head')->insertGetId([
            'No' => $order_no,
            'TransID' => $trans_id,
            'CstFirstname' => $name,
            'CstLastname' => $lastname,
            'CstCompanyName' => $userdata->CstCompanyName,
            'CstTel' => $tel,
            'email' => $email,
            'CstAddress' => $address,
            'CstDistrict' => $district,
            'CstSubdistrict' => $subdistrict,
            'CstProvice' => $province,
            'CstZipcode' => $zipcode,
            'ExchangeRate' => $rate,
            'created_at' => $date,
            'Orderstatus' => 1,
            'UserID' => $user_id,
            'OrderType' => 'Preorder'
        ]);

        $web = "";
        $total_cn = 0;

        foreach ($cart as $key => $value) {

            $qty = $value['qty'];
            $price = number_format($value['price'], 2, '.', '');
            $price_total = number_format($price * $qty, 2, '.', '');
            $price_total_th = number_format($price_total * $rate, 2, '.', '');
            $title = $value['title'];
            $options = $value['options'];
            $remark = $value['remark'];
            $detail_url = $value['detail_url'];
            $img_url = $value['img_url'];


            if ($title != '') {
                $DTID = DB::table('order_detail')->insertGetId([
                    'OrdID' => $headID,
                    'ItemName' => $title,
                    'Detail' => $options,
                    'Remark' => $remark,
                    'Link' => $detail_url,
                    'Price' => $price,
                    'Qty' => $qty,
                    'TotalLine' => $price_total,
                    'Pic' => $img_url,
                    'TotalLineTHB' => $price_total_th,
                ]);

                $web = checkWebsite($detail_url);
                $total_cn += $price_total;
            }
        }
        $total_cn = number_format($total_cn, 2, '.', '');
        $total_th = number_format($total_cn * $rate, 2, '.', '');

        OrderHead::where('ID', $headID)
            ->update([
                'OrderWeb' => $web,
                'TotalAllCNY' => $total_cn,
                'TotalAllTHB' => $total_th,
            ]);

        Cart::where('user_id', $user_id)->delete();

        $res = array();
        $res['status'] = "success";
        $res['message'] = "สร้างคำสั่งซื้อสำเร็จแล้ว กรุณารอเจ้าหน้าที่ติดต่อกลับอีกครั้ง";
        return response()->json($res);
    }
}
