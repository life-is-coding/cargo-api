<?php

namespace App\Http\Controllers\Api\Order;

use DB;
use ImageResize;
use Illuminate\Http\Request;
use App\Models\Order\OrderHead;
use App\Models\Order\Quotation;
use App\Models\Order\OrderFiles;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function view(Request $request, $status)
    {
        $user = $request->user();
        $user_id = $user->id;

        $data = OrderHead::join('transportations AS t', 't.TransID', '=', 'order_head.TransID')
            ->join('order_status AS s', 's.ID', '=', 'order_head.Orderstatus')
            ->leftjoin('products AS p', 'p.ProdID', '=', 'order_head.ProdID')
            ->select(
                'order_head.ID as ID',
                DB::raw("DATE_FORMAT(order_head.created_at,'%e/%m/%y') as order_date"),
                'order_head.No as order_no',
                DB::raw('CAST(ExchangeRate AS DECIMAL (10,2)) as exchange_rate'),
                DB::raw('CAST(totalallcny AS DECIMAL (10,2)) as total_cn'),
                DB::raw('CAST(TotalAllTHB AS DECIMAL (10,2)) as total_th'),
                's.UserStatus as order_status',
                't.TransName as transport_name',
                'p.ProdName as product_name',
                DB::raw("case when (select count(*) from quotation where quotation.OrderID=order_head.ID and Status = 1) > 0 then 'Y' else 'N' end as qu_status"),
            );

        if ($status == 'pending') {
            $data = $data->whereNotIn('Orderstatus', ['9', '99']);
        } else if ($status == 'complete') {
            $data = $data->where('Orderstatus', 9);
        }

        $data = $data->where('UserID', $user_id)
            ->orderBy('Orderstatus', 'asc')
            ->orderBy('order_head.created_at', 'asc')
            ->get();

        return response()->json($data);
    }

    public function view_order(Request $request, $order_id)
    {
        $user = $request->user();
        $user_id = $user->id;

        $data = OrderHead::with('items', 'qu', 'delivery')
            ->join('transportations AS t', 't.TransID', '=', 'order_head.TransID')
            ->join('order_status AS s', 's.ID', '=', 'order_head.Orderstatus')
            ->leftjoin('products AS p', 'p.ProdID', '=', 'order_head.ProdID')
            ->select(
                'order_head.ID as ID',
                DB::raw("DATE_FORMAT(order_head.created_at,'%e/%m/%y') as order_date"),
                'order_head.No as order_no',
                'order_head.OrderNumber as cn_order_no',
                DB::raw("DATE_FORMAT(OrderDate,'%e/%m/%y') as cn_order_date"),
                DB::raw('CAST(ExchangeRate AS DECIMAL (10,2)) as exchange_rate'),
                DB::raw('CAST(TotalAllCNY AS DECIMAL (10,2)) as total_cn'),
                DB::raw('CAST(TotalAllTHB AS DECIMAL (10,2)) as total_th'),
                's.UserStatus as order_status',
                't.TransName as transport_name',
                'p.ProdName as product_name',
                DB::raw("CstFirstname as cust_firstname"),
                DB::raw("CstLastname as cust_lastname"),
                'CstCompanyName as cust_company',
                'CstTel as cust_tel',
                'email as cust_email',
                DB::raw("(select CstID from customer where customer.UserID = order_head.UserID) as cust_id"),
                DB::raw("CstAddress + ' ' + CstDistrict + ' ' + CstSubdistrict + ' ' + CstProvice + ' ' + CstZipcode as cust_address"),
            )
            ->where('order_head.ID', $order_id)
            ->first();

        return response()->json($data);
    }

    public function confirm_order(Request $request)
    {
        $qu_id = $request->order_id;
        $files = $request->file;

        // order_id --> qu id
        $qu = Quotation::select('OrderID')->where('id', $qu_id)->first();
        $order_id = $qu->OrderID;

        $order = OrderHead::select('no', 'Orderstatus')->where('id', $order_id)->first();

        $i = 0;
        foreach ($files as $file) {
            $i++;
            $arr = explode("base64,", $file);
            $base = base64_decode($arr[1]);
            $fileExt = explode('/', explode(':', substr($file, 0, strpos($file, ';')))[1])[1];
            $file_name = $order->no . '_' . $qu_id . "." . $fileExt;

            $destinationPath = public_path() . "/upload/" . $file_name;
            $path_check = public_path() . "/upload/";
            if (!file_exists($path_check)) {
                mkdir($path_check, 0777, true);
            }

            file_put_contents($destinationPath, $base);

            $data = new OrderFiles();
            $data->order_id = $order_id;
            $data->qu_id = $qu_id;
            $data->file_name = $file_name;
            $data->file_comment = "ไฟล์แนบยืนยันการชำระเงิน";
            $data->created_at = date('Y-m-d H:i:s');
            $data->save();
        }

        if ($order->Orderstatus == 3) {
            OrderHead::where('id', $order_id)->update([
                'Orderstatus' => 4
            ]);
        }
        Quotation::where('id', $qu_id)->update([
            'Status' => 2
        ]);

        return 'update - complete';
    }
}
