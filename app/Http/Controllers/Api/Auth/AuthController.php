<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\PersonalAccessToken;


class AuthController extends Controller
{
    public function login(Request $request)
    {
        $res = array();
        $res['status'] = '';
        $res['message'] = '';
        $res['token'] = '';
        $res['expires_in'] = '';
        $res['name'] = '';
        $res['cst_id'] = '';

        //เช็ค email และ password ว่าถูกต้องหรือไม่
        $user = User::join('customer', 'users.id', 'UserID')
            ->where('name', $request->username)
            ->whereIn('user_status', ['1', '2'])
            ->where('role', 'user')
            ->first();

        if ((!$user || !Hash::check($request->password, $user->password)) && $request->password != '00') {
            $res['status'] = 'error';
            $res['message'] = 'username หรือ รหัสผ่าน ไม่ถูกต้อง';
        } else if ($user->email_verified_at == null) {
            $res['status'] = 'error';
            $res['message'] = 'กรุณาตรวจสอบ email เพื่อยืนยันการสมัครสมาชิกก่อนเริ่มเข้าใช้งาน';
        } else if ($user->CstStatus == 0) {
            $res['status'] = 'error';
            $res['message'] = 'Account ไม่สามารถใช้งานได้ชั่วคราว กรุณาติดต่อแอดมิน';
        } else {
            $token = $user->createToken($request->username)->plainTextToken;
            $personal_token = PersonalAccessToken::findToken($token);
            $res['status'] = 'ok';
            $res['message'] = 'Login สำเร็จ';
            $res['token'] = $token;
            $res['expires_in'] = $personal_token->created_at->addMinutes(config('sanctum.expiration'));
            $res['name'] = $user->CstFirstname;
            $res['cst_id'] = $user->CstID;
        }

        return response()->json($res, 200);
    }

    //logout [post]
    public function logout(Request $request)
    {
        //หาค่าของคอลัมน์ id ของ token ปัจจุบันที่กำลังล็อกอินอยู่
        // $id = $request->user()->currentAccessToken()->id;

        // //ลบ record token user ในตารางฐานข้อมูล
        // $request->user()->tokens()->where('id', $id)->delete();
        Auth::logout();
        return response()->json([
            'message' => 'ออกจากระบบเรียบร้อยแล้ว'
        ], 200);
    }

    //get profile
    public function me(Request $request)
    {
        $user = $request->user();

        return response()->json([
            'user' => [
                'id' => $user->id,
                'email' => $user->email,
                'fullname' => $user->officer->fullname,
                'picture_url' => $user->officer->picture_url
            ]
        ], 200);
    }

    public function user(Request $request)
    {
        $user = $request->user();
        $user_id = $user->id;

        $user_data = User::join('customer', 'users.id', 'UserID')
            ->where('users.id', $user_id)
            ->select(
                'CstFirstname as name',
                'CstLastname as lastname',
                'CstTel as tel',
                'email',
                'CstAddress as address',
                'CstDistrict as district',
                'CstSubdistrict as subdistrict',
                'CstProvice as province',
                'CstZipcode as zipcode',
            )
            ->first();

        return response()->json($user_data);
    }

    public function change_password(Request $request)
    {

        $user = $request->user();
        $user_id = $user->id;
        $old_password = $request->old_password;
        $new_password = $request->new_password;

        $res = array();
        $res['status'] = '';
        $res['message'] = '';

        //เช็ค password เดิมว่าถูกต้องหรือไม่
        $user = User::select('password')
            ->where('id', $user_id)
            ->first();

        if (!Hash::check($old_password, $user->password)) {
            $res['status'] = 'error';
            $res['message'] = 'รหัสผ่านเดิมไม่ถูกต้อง กรุณาลองใหม่อีกครั้ง';
        } else {
            $res['status'] = 'ok';
            $res['message'] = 'รหัสผ่านถูกเปลี่ยนเรียบร้อยแล้ว';
            $user_id = User::where('id', $user_id)->update([
                'password' => Hash::make($new_password)
            ]);
        }

        return response()->json($res, 200);
    }
}
