<?php

namespace App\Http\Controllers\Api\Auth;

use App\Models\User;
use App\Mail\UserRegister;
use Illuminate\Http\Request;
use App\Models\Auth\Customer;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class RegisterController extends Controller
{
    public function check_email(Request $request)
    {
        $email = $request->email;
        $res = array();
        $res['status'] = 'ok';
        $res['message'] = 'อีเมล์นี้สามารถใช้งานได้';
        $chk = User::where('email', $email)->count();
        if ($chk > 0) {
            $res['status'] = 'error';
            $res['message'] = 'อีเมล์นี้ได้ลงทะเบียนในระบบเรียบร้อยแล้ว กรุณาใช้อีเมล์อื่น';
        }
        return response()->json($res);
    }

    public function check_username(Request $request)
    {
        $username = $request->username;
        $res = array();
        $res['status'] = 'ok';
        $res['message'] = 'ชื่อผู้ใช้งานนี้สามารถใช้งานได้';
        $chk = User::where('name', $username)->count();
        if ($chk > 0) {
            $res['status'] = 'error';
            $res['message'] = 'ชื่อผู้ใช้งานซ้ำ กรุณาลองใหม่อีกครั้ง';
        }
        return response()->json($res);
    }

    public function register(Request $request)
    {

        $res = array();
        $username = $request->username;
        $password = $request->password;
        $email = $request->email;

        $firstname = $request->firstname;
        $lastname = $request->lastname;
        $companyname = $request->companyname;
        $address = $request->address;
        $district = $request->district;
        $subdistrict = $request->subdistrict;
        $province = $request->province;
        $zipcode = $request->zipcode;
        $phone = $request->phone;

        try {
            // insert new users and get new id
            $newID = genLastCstID();
            $date = date("Y-m-d H:i:s");

            $all_md5 = $email . $date;
            $md5 = md5($all_md5);

            $user_id = DB::table('users')->insertGetId([
                'name' => $username,
                'email' => $email,
                'password' => Hash::make($password),
                'created_at' => $date,
                'updated_at' => $date,
                'remember_token' => $md5,
                'role' => 'user',
                'user_status' => 1
            ]);

            $data = new Customer();
            $data['UserID'] = $user_id;
            $data['CstID'] = $newID;
            $data['CstFirstname'] = $firstname;
            $data['CstLastname'] = $lastname;
            $data['CstCompanyName'] = $companyname;
            $data['CstTel'] = $phone;
            $data['CstAddress'] = $address;
            $data['CstDistrict'] = $district;
            $data['CstSubdistrict'] = $subdistrict;
            $data['CstProvice'] = $province;
            $data['CstZipcode'] = $zipcode;
            $data['CstStatus'] = 1;
            $data['CstTypeId'] = 1; // ลูกค้าทั่วไป
            $data->save();

            $res['status'] = 'ok';
            $res['message'] = 'ลงทะเบียนเรียบร้อย กรุณายืนยันใน Email อีกครั้งเพื่อเริ่มใช้งาน';

            $data = User::join('customer', 'users.id', 'UserID')->find($user_id);
            $mail = Mail::to($email);
            $mail = $mail->send(new UserRegister($data, $md5));
        } catch (Exception $e) {
            $res['status'] = 'error';
            $res['message'] = $e->getMessage();

            DB::table('users')->where('id', $user_id)->delete();
        }

        return response()->json($res);
    }

    public function verify(Request $request)
    {
        $tk = $request->tk;
        $res = array();
        $res['status'] = 'error';
        $res['message1'] = 'Error';
        $res['message2'] = 'Account นี้เคยทำการยืนยันการสมัครสมาชิกแล้ว';
        $chk = User::where('remember_token', $tk)->count();
        if ($chk == 1) {
            User::where('remember_token', $tk)->update([
                'email_verified_at' => date("Y-m-d H:i:s"),
                'remember_token' => NULL,
                'user_status' => 2
            ]);
            $res['status'] = 'ok';
            $res['message1'] = 'Registrations Completed';
            $res['message2'] = 'ยืนยันการสมัครสมาชิกเรียบร้อยแล้ว คุณสามารถ login เพื่อเข้าใช้งานระบบได้';
        }
        return response()->json($res);
    }
}
