<?php

namespace App\Http\Controllers\API\Auth;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {
        $res = User::whereIn('role', ['admin', 'superadmin'])
            ->orderBy('name')
            ->get();

        return response()->json($res);
    }

    public function store(Request $request)
    {
        $res = array();

        try {
            $name = $request->name;
            $email = $request->email;
            $date = $request->date;
            $password = $request->password;
            $role = $request->role;

            $date = date("Y-m-d H:i:s");
            $data = new User();
            $data->name = $name;
            $data->email = $email;
            $data->email_verified_at = $date;
            $data->password = Hash::make($password);
            $data->user_status = 2;
            $data->role = $role;
            $data->save();

            $res['status'] = 'ok';
            $res['message'] = 'เพื่มผู้ใช้งานใหม่เรียบร้อย';
        } catch (Exception $e) {
            $res['status'] = 'error';
            $res['message'] = $e->getMessage();
        }

        return response()->json($res);
    }

    public function show($id)
    {
        $res = User::find($id);
        return response()->json($res);
    }

    public function update(Request $request, $id)
    {
        $data = User::find($id);
        $data->name = $request->name;
        $data->email = $request->email;
        if ($data->password != Hash::make($request->password)) {
            $data->password = Hash::make($request->password);
        }
        $data->user_status = $request->user_status;
        $data->role = $request->role;
        $data->save();

        return response()->json([
            'message' => 'แก้ไขข้อมูลเรียบร้อย',
            'data' => $data
        ], 200);
    }

    public function destroy($id)
    {
        //
    }
}
