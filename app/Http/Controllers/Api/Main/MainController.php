<?php

namespace App\Http\Controllers\Api\Main;

use Illuminate\Http\Request;
use App\Models\Main\Products;
use App\Models\Setting\Articles;
use App\Http\Controllers\Controller;
use App\Models\Main\Transportations;

class MainController extends Controller
{
    public function active()
    {
        $data = array();
        $data['products'] = Products::orderBy('ProdName')->where('ProdStatus', 1)->get();
        $data['transportations'] = Transportations::with(['weight' => function ($query) {
            $query->where('CstTypeId', 1);
        }])
            ->orderBy('TransName')
            ->where('TransStatus', 1)
            ->get();
        $data['articles'] = Articles::where('article_status', '1')->orderBy('article_id')->take(4)->get();
        $data['transportations_vip'] = Transportations::with(['weight' => function ($query) {
            $query->where('CstTypeId', 2);
        }])
            ->orderBy('TransName')
            ->where('TransStatus', 1)
            ->get();

        // $data['table'] = Transportations::orderBy('TransName')->where('TransStatus', 1)->get();

        return response()->json($data);
    }
}
