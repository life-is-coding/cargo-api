<?php

namespace App\Http\Controllers\Admin;

use PDF;
use App\Mail\OrderUpdate;
use App\Models\Setting\Bank;
use Illuminate\Http\Request;
use App\Models\Auth\Customer;
use App\Models\Order\OrderHead;
use App\Models\Order\OrderItem;
use Illuminate\Support\Facades\DB;
use App\Models\Order\OrderDelivery;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Models\Main\TransportationsWeight;

class OrdersController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function quotation($ID)
    {

        $qu = DB::table('quotation')
            ->where('ID', '=', $ID)
            ->first();

        $orderid = $qu->OrderID;
        $deliveryid = $qu->Delivery;
        $product = $qu->Product;

        $data = DB::table('order_head')
            ->join('transportations AS t', 't.TransID', '=', 'order_head.TransID')
            ->join('customer AS c', 'c.UserID', '=', 'order_head.UserID')
            ->join('customer_type AS ct', 'ct.cst_type_id', '=', 'c.CstTypeId')
            ->leftjoin('products', 'products.ProdID', '=', 'order_head.ProdID')
            ->select('order_head.*', 't.TransName', 'c.CstID', 'ct.cst_type_name', 'products.ProdName')
            ->where('order_head.ID', '=', $orderid)
            ->get();

        $detail = array();
        if ($product == 'Y') {
            $detail = OrderItem::where('OrdID', '=', $orderid)
                ->orderBy('ID', 'asc')
                ->get();
        }

        $delivery = "";
        if ($deliveryid != "") {
            $delivery = DB::table('order_delivery')
                ->leftjoin('shipping_company', 'shipping_company.ID', 'ShipComID')
                ->whereRaw("order_delivery.ID in (" . $deliveryid . ")")
                ->select(
                    'order_delivery.*',
                    'CompanyName'
                )
                ->orderBy('order_delivery.ID', 'asc')
                ->get();
        }

        $bank_item = Bank::where('acc_type', 'item')->first();
        $bank_trans = Bank::where('acc_type', 'trans')->first();
        // print_r($delivery);

        $pdf = PDF::loadView('pages.setting.pdf_quotation', [
            'data' => $data,
            'detail' => $detail,
            'delivery' => $delivery,
            'bank_item' => $bank_item,
            'bank_trans' => $bank_trans,
            'qu' => $qu
        ]);


        return $pdf->stream('ใบเสนอราคาสินค้า ' . $data[0]->No . '.pdf');
    }

    public function request()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        $data = OrderHead::with('detail')
            ->join('transportations AS t', 't.TransID', '=', 'order_head.TransID')
            ->leftjoin('products AS p', 'p.ProdID', '=', 'order_head.ProdID')
            ->select(
                'order_head.*',
                't.TransName',
                'p.ProdName',
                DB::raw("(select CstID from customer where customer.UserID = order_head.UserID ) as CstID"),
                DB::raw("(select cst_type_name from customer_type where cst_type_id = ( select CstTypeId from customer where customer.UserID = order_head.UserID ) ) as CstTypeName"),
                DB::raw('CAST(TotalAllCNY AS DECIMAL (10,2)) as total_cn'),
                DB::raw('CAST(TotalAllTHB AS DECIMAL (10,2)) as total_th')
            )
            ->where('Orderstatus', '1')
            ->orderBy('ID', 'asc')
            ->get();
        return view('pages.orders_request', ['data' => $data]);
    }

    public function booking(Request $request)
    {
        OrderHead::whereIn('ID', $request->val)
            ->update([
                "AdminID" => Auth::user()->id,
                "Orderstatus" => '2',
            ]);

        return 'booking = complete';
    }

    public function new()
    {
        $data = OrderHead::whereIn('Orderstatus', [1])->count();
        return $data;
    }

    public function order_status()
    {
        $role = Auth::user()->role;
        $admin_id = Auth::user()->id;

        $data = OrderHead::join('order_status', 'Orderstatus', 'order_status.ID')
            ->select(
                'Orderstatus',
                'AdminStatus',
                'Color',
                DB::raw('count(order_head.ID) as num_order')
            );
        if ($role != 'superadmin') {
            $data = $data->where('AdminID', $admin_id);
        }
        $data = $data->whereNotIn('order_status.ID', [1, 99, 9])
            ->groupBy('Orderstatus', 'AdminStatus', 'Color')
            ->orderBy('order_status.ID')
            ->get();

        $delivery = OrderHead::join('order_delivery', 'order_delivery.OrdID', 'order_head.ID')
            ->join('delivery_status', 'del_status_id', 'DeliveryStatus')
            ->select(
                'del_status_id',
                'del_status_name',
                'del_color',
                DB::raw('count(del_status_name) as num_del')
            );
        if ($role != 'superadmin') {
            $delivery = $delivery->where('AdminID', $admin_id);
        }
        $delivery = $delivery->whereNotIn('Orderstatus', [1, 99, 9])
            ->groupBy('del_status_id', 'del_status_name', 'del_color')
            ->orderBy('del_status_id')
            ->get();

        return view('pages.orders_status', ['data' => $data, 'delivery' => $delivery]);
    }

    public function index()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        return view('pages.orders');
    }

    public function order_table($status, Request $request)
    {
        $role = Auth::user()->role;
        $admin_id = Auth::user()->id;

        $searchTable = $request->searchTable;
        $sub = $request->sub;

        $data = OrderHead::with('qu', 'delivery')
            ->join('transportations AS t', 't.TransID', '=', 'order_head.TransID')
            ->join('order_status AS s', 's.ID', '=', 'order_head.Orderstatus')
            ->leftjoin('products AS p', 'p.ProdID', '=', 'order_head.ProdID')
            ->select(
                'order_head.*',
                't.TransName',
                'p.ProdName',
                's.AdminStatus',
                'order_head.ExchangeRate',
                DB::raw('CAST(TotalAllCNY AS DECIMAL (10,2)) as total_cn'),
                DB::raw('CAST(TotalAllTHB AS DECIMAL (10,2)) as total_th'),
                DB::raw("(select CstID from customer where customer.UserID = order_head.UserID ) as CstID"),
                DB::raw("(select cst_type_name from customer_type where cst_type_id = ( select CstTypeId from customer where customer.UserID = order_head.UserID ) ) as CstTypeName"),
                DB::raw("(select sum(TotalLine) from order_detail where order_detail.OrdID = order_head.ID ) as sum_price_cn"),
                DB::raw("(select name from users where users.id = AdminID ) as AdminName")
            );
        if ($status != 'all') {
            $data = $data->where('Orderstatus', $status);
        }
        if ($role != 'superadmin') {
            $data = $data->where('AdminID', $admin_id);
        }
        if ($sub != '') {
            $data = $data->whereIn('order_head.ID', [DB::raw("select OrdId from order_delivery where DeliveryStatus = '" . $sub . "' ")]);
        }
        if ($searchTable != "") {
            $data = $data->where(function ($q) use ($searchTable) {
                $q->where('order_head.No', $searchTable)
                    ->orwhere('order_head.CstFirstname', 'like', '%' . $searchTable . '%')
                    ->orwhere('order_head.OrderNumber', $searchTable)
                    ->orWhereIn('order_head.UserID', [DB::raw("select UserID from customer where CstFirstname like '%" . $searchTable . "%' or CstID  = '" . $searchTable . "'")])
                    ->orWhereIn('order_head.ID', [DB::raw("select OrdID from order_delivery where TrackingNo = '" . $searchTable . "' or OrderTracking  = '" . $searchTable . "' or PurchaseOrder = '" . $searchTable . "' ")]);
            });
        } else {
            $data = $data->whereNotIn('Orderstatus', ['1', '9', '99']);
        }
        $data = $data->orderBy('order_head.ID', 'asc')
            ->get();

        return view('pages.orders_table', ['data' => $data]);
    }

    public function create()
    {
        $trans = DB::table('transportations')->whereIn('TransStatus', ['1'])->get();
        $ShipCom = DB::table('shipping_company')->whereIn('Status', ['1'])->get();
        $prod = DB::table('products')->whereIn('ProdStatus', ['1'])->get();
        $exchange = DB::table('exchange')->where('ID', '=', 1)->get();
        $cst = DB::table('customer')
            ->join('users', 'users.id', '=', 'customer.UserID')
            ->where('CstStatus', ['1'])
            ->select('customer.*', 'users.email')
            ->get();

        return view('pages.orders_form', [
            'StaPage' => 'create',
            'cst' => $cst,
            'ShipCom' => $ShipCom,
            'delivery' => array(),
            'exchange' => $exchange,
            'trans' => $trans,
            'prod' => $prod,
            'quotation' => array(),
        ]);
    }

    public function store(Request $request)
    {
        $order_no = genLastOrderNo();

        $cst = Customer::where('ID', $request->CstID)->select('UserID')->first();

        $headID = DB::table('order_head')->insertGetId([
            'No' => $order_no,
            'OrderNumber' => $request->OrderNumber,
            'OrderDate' => isset($request->OrderDate) ? $request->OrderDate : NULL,
            'TransID' => $request->TransID,
            'ProdID' => $request->ProdID,
            'CstFirstname' => $request->CstFirstname,
            'CstLastname' => $request->CstLastname,
            'CstCompanyName' => $request->CstCompanyName,
            'CstTel' => $request->CstTel,
            'UserID' => $cst->UserID,
            'email' => $request->email,
            'CstAddress' => $request->CstAddress,
            'CstDistrict' => $request->CstDistrict,
            'CstSubdistrict' => $request->CstSubdistrict,
            'CstProvice' => $request->CstProvice,
            'CstZipcode' => $request->CstZipcode,
            'ExchangeRate' => $request->ExchangeRate,
            'OrderWeb' => $request->OrderWeb,
            'AdminAccount' => $request->AdminAccount,
            'TotalAllCNY' => $request->TotalAllCNY,
            'TotalAllTHB' => $request->TotalAllTHB,
            'created_at' => date('Y-m-d H:i:s'),
            'Orderstatus' => ($request->OrderType == 'Import') ? 5 : 2,
            'AdminID' => Auth::user()->id,
            'OrderType' => $request->OrderType
        ]);
        for ($i = 1; $i < $request->nRowDlv; $i++) {
            isset($request->{'TotalBox' . $i}) ? $TotalBox = $request->{'TotalBox' . $i} : $TotalBox = 0;
            if ($TotalBox > 0) {
                DB::table('order_delivery')->insert([
                    'OrdID' => $headID,
                    'TotalBox' => $TotalBox,
                    'KG' => $request->{'KG' . $i},
                    'Queue' => $request->{'Queue' . $i},
                    'Width' => $request->{'Width' . $i},
                    'Length' => $request->{'Length' . $i},
                    'Height' => $request->{'Height' . $i},
                    'DeliveryCNCN' => $request->{'DeliveryCNCN' . $i},
                    'DeliveryCNTH' => $request->{'DeliveryCNTH' . $i},
                    'DeliveryTHTH' => $request->{'DeliveryTHTH' . $i},
                    'OrderTracking' => $request->{'OrderTracking' . $i},
                    'WarehouseCNDate' => isset($request->{'WarehouseCNDate' . $i}) ? $request->{'WarehouseCNDate' . $i} : NULL,
                    'ExportDate' => isset($request->{'ExportDate' . $i}) ? $request->{'ExportDate' . $i} : NULL,
                    'PurchaseOrder' => $request->{'PurchaseOrder' . $i},
                    'Container' => $request->{'Container' . $i},
                    'ImportDate' => isset($request->{'ImportDate' . $i}) ? $request->{'ImportDate' . $i} : NULL,
                    'DeliveryDate' => isset($request->{'DeliveryDate' . $i}) ? $request->{'DeliveryDate' . $i} : NULL,
                    'ShipComID' => $request->{'ShipComID' . $i},
                    'TrackingNO' => $request->{'TrackingNO' . $i},
                    'DeliveryStatus' => 1
                ]);
            }
        }
        $web = "";
        for ($i = 1; $i < $request->nRow; $i++) {
            isset($request->{'ItemName' . $i}) ? $ItemName = $request->{'ItemName' . $i} : $ItemName = '';
            isset($request->{'Price' . $i}) ? $Price = $request->{'Price' . $i} : $Price = 0;
            isset($request->{'Qty' . $i}) ? $Qty = $request->{'Qty' . $i} : $Qty = 0;
            isset($request->{'TotalLine' . $i}) ? $TotalLine = $request->{'TotalLine' . $i} : $TotalLine = 0;
            isset($request->{'TotalLineTHB' . $i}) ? $TotalLineTHB = $request->{'TotalLineTHB' . $i} : $TotalLineTHB = 0;
            if ($ItemName != '') {
                $DTID = DB::table('order_detail')->insertGetId([
                    'OrdID' => $headID,
                    'ItemName' => $ItemName,
                    'Detail' => $request->{'Detail' . $i},
                    'Remark' => $request->{'Remark' . $i},
                    'Link' => $request->{'Link' . $i},
                    'Price' => $Price,
                    'Qty' => $Qty,
                    'TotalLine' => $TotalLine,
                    'TotalLineTHB' => $TotalLineTHB,
                ]);

                $web = checkWebsite($request->{'Link' . $i});
            }
            if ($request->hasFile('Pic' . $i)) {
                $digits = 3;
                $filename = $DTID . '_' . str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT) . '.' . request()->{'Pic' . $i}->getClientOriginalExtension();
                request()->{'Pic' . $i}->move(public_path('images/items'), $filename);

                DB::table('order_detail')->where('ID', $DTID)->update([
                    'Pic' => asset('/images/items/' . $filename)
                ]);
            }
        }

        OrderHead::where('ID', $headID)
            ->update([
                'OrderWeb' => $web
            ]);

        return redirect()->route('orders')
            ->with('success', 'เพิ่มคำสั่งซื้อเรียบร้อย.');
    }

    public function edit($ID)
    {
        $trans = DB::table('transportations')->whereIn('TransStatus', ['1'])->get();
        $prod = DB::table('products')->whereIn('ProdStatus', ['1'])->get();
        $data = DB::table('order_head')->where('ID', '=', $ID)->get();
        $quotation = DB::table('quotation')
            ->join('quotation_status', 'qu_status_id', 'Status')
            ->where('OrderID', '=', $ID)->get();
        $detail = DB::table('order_detail')->where('OrdID', '=', $ID)->orderBy('ID', 'asc')->get();
        $ShipCom = DB::table('shipping_company')->whereIn('Status', ['1'])->get();
        $exchange = DB::table('exchange')->where('ID', '=', 1)->get();
        $delivery = DB::table('order_delivery')->where('OrdID', '=', $ID)->orderBy('ID', 'asc')->get();
        $cst = DB::table('customer')
            ->join('users', 'users.id', '=', 'customer.UserID')
            ->where('CstStatus', ['1'])
            ->select('customer.*', 'users.email')
            ->get();
        $cst_type = DB::table('customer_type')
            ->select('cst_type_id', 'cst_type_name')
            ->where('cst_type_id', DB::raw("( select CstTypeId from customer where UserID = " . $data[0]->UserID . " ) "))
            ->get();

        return view('pages.orders_form', [
            'StaPage' => 'edit',
            'data' => $data,
            'detail' => $detail,
            'delivery' => $delivery,
            'ShipCom' => $ShipCom,
            'exchange' => $exchange,
            'cst' => $cst,
            'trans' => $trans,
            'prod' => $prod,
            'cst_type' => $cst_type,
            'quotation' => $quotation,
        ]);
    }

    public function update(Request $request, $ID)
    {
        DB::table('order_head')->where('ID', $ID)->update([
            'OrderNumber' => $request->OrderNumber,
            'OrderDate' => isset($request->OrderDate) ? $request->OrderDate : NULL,
            'ExchangeRate' => $request->ExchangeRate,
            'TransID' => $request->TransID,
            'ProdID' => $request->ProdID,
            'CstFirstname' => $request->CstFirstname,
            'CstLastname' => $request->CstLastname,
            'CstCompanyName' => $request->CstCompanyName,
            'CstTel' => $request->CstTel,
            'email' => $request->email,
            'CstAddress' => $request->CstAddress,
            'CstDistrict' => $request->CstDistrict,
            'CstSubdistrict' => $request->CstSubdistrict,
            'CstProvice' => $request->CstProvice,
            'CstZipcode' => $request->CstZipcode,
            'OrderWeb' => $request->OrderWeb,
            'AdminAccount' => $request->AdminAccount,
            'TotalAllCNY' => $request->TotalAllCNY,
            'TotalAllTHB' => $request->TotalAllTHB,
            'updated_at' => date('Y-m-d H:i:s'),
            'AdminID' => Auth::user()->id,
            'OrderType' => $request->OrderType
        ]);

        for ($i = 1; $i < $request->nRowDlv; $i++) {
            isset($request->{'DlvID' . $i}) ? $DlvID = $request->{'DlvID' . $i} : $DlvID = '';
            isset($request->{'TotalBox' . $i}) ? $TotalBox = $request->{'TotalBox' . $i} : $TotalBox = 0;
            if ($DlvID == '') {
                if ($TotalBox > 0) {
                    DB::table('order_delivery')->insert([
                        'OrdID' => $ID,
                        'TotalBox' => $TotalBox,
                        'KG' => $request->{'KG' . $i},
                        'Queue' => $request->{'Queue' . $i},
                        'Width' => $request->{'Width' . $i},
                        'Length' => $request->{'Length' . $i},
                        'Height' => $request->{'Height' . $i},
                        'DeliveryCNCN' => $request->{'DeliveryCNCN' . $i},
                        'DeliveryCNTH' => $request->{'DeliveryCNTH' . $i},
                        'DeliveryTHTH' => $request->{'DeliveryTHTH' . $i},
                        'OrderTracking' => $request->{'OrderTracking' . $i},
                        'WarehouseCNDate' => isset($request->{'WarehouseCNDate' . $i}) ? $request->{'WarehouseCNDate' . $i} : NULL,
                        'ExportDate' => isset($request->{'ExportDate' . $i}) ? $request->{'ExportDate' . $i} : NULL,
                        'PurchaseOrder' => $request->{'PurchaseOrder' . $i},
                        'Container' => $request->{'Container' . $i},
                        'ImportDate' => isset($request->{'ImportDate' . $i}) ? $request->{'ImportDate' . $i} : NULL,
                        'DeliveryDate' => isset($request->{'DeliveryDate' . $i}) ? $request->{'DeliveryDate' . $i} : NULL,
                        'ShipComID' => $request->{'ShipComID' . $i},
                        'TrackingNO' => $request->{'TrackingNO' . $i},
                        'DeliveryStatus' => 1
                    ]);
                }
            } else {
                DB::table('order_delivery')->where('ID', $DlvID)->update([
                    'TotalBox' => $TotalBox,
                    'KG' => $request->{'KG' . $i},
                    'Queue' => $request->{'Queue' . $i},
                    'Width' => $request->{'Width' . $i},
                    'Length' => $request->{'Length' . $i},
                    'Height' => $request->{'Height' . $i},
                    'DeliveryCNCN' => $request->{'DeliveryCNCN' . $i},
                    'DeliveryCNTH' => $request->{'DeliveryCNTH' . $i},
                    'DeliveryTHTH' => $request->{'DeliveryTHTH' . $i},
                    'OrderTracking' => $request->{'OrderTracking' . $i},
                    'WarehouseCNDate' => isset($request->{'WarehouseCNDate' . $i}) ? $request->{'WarehouseCNDate' . $i} : NULL,
                    'ExportDate' => isset($request->{'ExportDate' . $i}) ? $request->{'ExportDate' . $i} : NULL,
                    'PurchaseOrder' => $request->{'PurchaseOrder' . $i},
                    'Container' => $request->{'Container' . $i},
                    'ImportDate' => isset($request->{'ImportDate' . $i}) ? $request->{'ImportDate' . $i} : NULL,
                    'DeliveryDate' => isset($request->{'DeliveryDate' . $i}) ? $request->{'DeliveryDate' . $i} : NULL,
                    'ShipComID' => $request->{'ShipComID' . $i},
                    'TrackingNO' => $request->{'TrackingNO' . $i},

                ]);
            }
        }
        // DB::table('order_detail')->where('OrdID', $ID)->delete();
        for ($i = 1; $i < $request->nRow; $i++) {
            isset($request->{'ItemID' . $i}) ? $ItemID = $request->{'ItemID' . $i} : $ItemID = '';
            isset($request->{'ItemName' . $i}) ? $ItemName = $request->{'ItemName' . $i} : $ItemName = '';
            isset($request->{'Price' . $i}) ? $Price = $request->{'Price' . $i} : $Price = 0;
            isset($request->{'Qty' . $i}) ? $Qty = $request->{'Qty' . $i} : $Qty = 0;
            isset($request->{'TotalLine' . $i}) ? $TotalLine = $request->{'TotalLine' . $i} : $TotalLine = 0;
            isset($request->{'TotalLineTHB' . $i}) ? $TotalLineTHB = $request->{'TotalLineTHB' . $i} : $TotalLineTHB = 0;
            if ($ItemID == '') {
                $ItemID = DB::table('order_detail')->insertGetId([
                    'OrdID' => $ID,
                    'ItemName' => $ItemName,
                    'Detail' => $request->{'Detail' . $i},
                    'Remark' => $request->{'Remark' . $i},
                    'Link' => $request->{'Link' . $i},
                    'Price' => $Price,
                    'Qty' => $Qty,
                    'TotalLine' => $TotalLine,
                    'TotalLineTHB' => $TotalLineTHB,
                ]);
            } else {
                DB::table('order_detail')->where('ID', $ItemID)->update([
                    'ItemName' => $ItemName,
                    'Detail' => $request->{'Detail' . $i},
                    'Remark' => $request->{'Remark' . $i},
                    'Link' => $request->{'Link' . $i},
                    'Price' => $Price,
                    'Qty' => $Qty,
                    'TotalLine' => $TotalLine,
                    'TotalLineTHB' => $TotalLineTHB,

                ]);
            }
            if ($request->hasFile('Pic' . $i)) {
                $digits = 3;
                $filename = $ItemID . '_' . str_pad(rand(0, pow(10, $digits) - 1), $digits, '0', STR_PAD_LEFT) . '.' . request()->{'Pic' . $i}->getClientOriginalExtension();
                request()->{'Pic' . $i}->move(public_path('images/items'), $filename);

                DB::table('order_detail')->where('ID', $ItemID)->update([
                    'Pic' => asset('/images/items/' . $filename)
                ]);
            }
        }
        return redirect()->route('orders')
            ->with('success', 'แก้ไขคำสั่งซื้อเรียบร้อย.');
    }

    public function change_order(Request $request)
    {
        $val = $request->val;
        $order_id = $request->order_id;

        $total_th = 0;
        $total_cn = 0;
        $items = OrderItem::select('TotalLine', 'TotalLineTHB')->whereIn('ID', $val)->get();
        foreach ($items as $each) {
            $total_th += $each['TotalLineTHB'];
            $total_cn += $each['TotalLine'];
        }

        $old_head_data = DB::table('order_head')->where('ID', '=', $order_id)->first();
        $order_no = genLastOrderNo();

        $headID = DB::table('order_head')->insertGetId([
            'No' => $order_no,
            'TransID' => $old_head_data->TransID,
            'ProdID' => $old_head_data->ProdID,
            'CstFirstname' => $old_head_data->CstFirstname,
            'CstLastname' => $old_head_data->CstLastname,
            'CstCompanyName' => $old_head_data->CstCompanyName,
            'CstTel' => $old_head_data->CstTel,
            'UserID' => $old_head_data->UserID,
            'email' => $old_head_data->email,
            'CstAddress' => $old_head_data->CstAddress,
            'CstDistrict' => $old_head_data->CstDistrict,
            'CstSubdistrict' => $old_head_data->CstSubdistrict,
            'CstProvice' => $old_head_data->CstProvice,
            'CstZipcode' => $old_head_data->CstZipcode,
            'ExchangeRate' => $old_head_data->ExchangeRate,
            'OrderWeb' => $old_head_data->OrderWeb,
            'created_at' => date('Y-m-d H:i:s'),
            'Orderstatus' => 2,
            'AdminID' => $old_head_data->AdminID,
            'TotalAllCNY' => $total_cn,
            'TotalAllTHB' => $total_th,
        ]);

        OrderItem::whereIn('ID', $val)->update([
            'OrdID' => $headID
        ]);

        $chang_old_order_cn = $old_head_data->TotalAllCNY - $total_cn;
        $chang_old_order_th = $old_head_data->TotalAllTHB - $total_th;

        OrderHead::where('ID', $order_id)->update([
            'TotalAllCNY' => $chang_old_order_cn,
            'TotalAllTHB' => $chang_old_order_th,
        ]);

        $res = array();
        $res['order_no'] = $order_no;
        $res['order_id'] = $headID;

        return response()->json($res);
    }

    public function delete(Request $request)
    {
        $admin_id = Auth::user()->id;
        $deleteID = $request->deleteID;
        $deleteRemark = $request->deleteRemark;

        $date = date('Y-m-d H:i:s');

        OrderHead::where('ID', $deleteID)
            ->update([
                "CancelRemark" => $deleteRemark,
                "CancelBy" => $admin_id,
                "CancelDate" => $date,
                "Orderstatus" => "99"
            ]);

        return 'delete - complete';
    }

    public function view($ID)
    {
        $data = OrderHead::with('detail', 'images', 'qu')
            ->join('transportations', 'transportations.TransID', '=', 'order_head.TransID')
            ->join('order_status', 'order_status.ID', '=', 'order_head.Orderstatus')
            ->leftjoin('products', 'products.ProdID', '=', 'order_head.ProdID')
            ->select(
                'order_head.*',
                'transportations.TransName',
                'products.ProdName',
                'order_status.AdminStatus',
                'order_head.ExchangeRate',
                DB::raw("(select CstID from customer where customer.UserID = order_head.UserID ) as CstID"),
                DB::raw("(select cst_type_name from customer_type where cst_type_id = ( select CstTypeId from customer where customer.UserID = order_head.UserID ) ) as CstTypeName"),
                DB::raw("(select name from users where users.id = AdminID ) as AdminName"),
                DB::raw("(select sum(TotalLine) from order_detail where order_detail.OrdID = order_head.ID ) as sum_price_cn")
            )
            ->where('order_head.ID', $ID)
            ->first();

        $delivery = DB::table('order_delivery')
            ->leftjoin('shipping_company', 'shipping_company.ID', '=', 'order_delivery.ShipComID')
            ->select(
                'order_delivery.*',
                'shipping_company.CompanyName'

            )
            ->where('OrdID', '=', $ID)->orderBy('order_delivery.ID', 'asc')->get();
        // $quotation = DB::table('quotation')
        //     ->join('quotation_status', 'qu_status_id', 'Status')
        //     ->select(
        //         'quotation.*',

        //     )
        //     ->where('OrderID', '=', $ID)
        //     ->orderBy('ID', 'asc')->get();

        return view('pages.orders_view', [
            'data' => $data,
            'delivery' => $delivery,
            // 'quotation' => $quotation
        ]);
    }

    public function update_status(Request $request, $id, $status_id, $type)
    {
        $mail = false;
        $update = array();
        if ($type == 'order') {

            if ($status_id == 3 || $status_id == 5 || $status_id == 9) {
                $mail = true;
            }
            $update['Orderstatus'] = $status_id;
            // 5
            if (isset($request->OrderNumber)) {
                $update['OrderNumber'] = $request->OrderNumber;
            }
            if (isset($request->OrderDate)) {
                $update['OrderDate'] = $request->OrderDate;
            }
            if (isset($request->AdminAccount)) {
                $update['AdminAccount'] = $request->AdminAccount;
            }

            OrderHead::where('ID', $id)->update($update);
            $delivery = "";
            $order_id = $id;
        }
        if ($type == 'delivery') {

            if ($status_id == 2 || $status_id == 3 || $status_id == 5) {
                $mail = true;
            }

            $update['DeliveryStatus'] = $status_id;
            // 2
            if (isset($request->WarehouseCNDate)) {
                $update['WarehouseCNDate'] = $request->WarehouseCNDate;
            }
            if (isset($request->ExportDate)) {
                $update['ExportDate'] = $request->ExportDate;
            }
            if (isset($request->PurchaseOrder)) {
                $update['PurchaseOrder'] = $request->PurchaseOrder;
            }
            if (isset($request->Container)) {
                $update['Container'] = $request->Container;
            }
            if (isset($request->Width)) {
                $update['Width'] = $request->Width;
            }
            if (isset($request->Length)) {
                $update['Length'] = $request->Length;
            }
            if (isset($request->Height)) {
                $update['Height'] = $request->Height;
            }
            if (isset($request->TotalBox)) {
                $update['TotalBox'] = $request->TotalBox;
            }
            if (isset($request->Queue)) {
                $update['Queue'] = $request->Queue;
            }
            if (isset($request->KG)) {
                $update['KG'] = $request->KG;
            }

            // 3
            if (isset($request->ImportDate)) {
                $update['ImportDate'] = $request->ImportDate;
            }

            // 4
            if (isset($request->DeliveryDate)) {
                $update['DeliveryDate'] = $request->DeliveryDate;
            }
            if (isset($request->ShipComID)) {
                $update['ShipComID'] = $request->ShipComID;
            }
            if (isset($request->TrackingNO)) {
                $update['TrackingNO'] = $request->TrackingNO;
            }
            OrderDelivery::where('ID', $id)->update($update);
            $delivery = OrderDelivery::where('order_delivery.ID', $id)
                ->leftjoin('shipping_company AS sh', 'sh.ID', '=', 'ShipComID')
                ->select('order_delivery.*', 'CompanyName')
                ->first();
            $order_id = $delivery->OrdID;
        }

        if ($mail) {
            $data = OrderHead::join('transportations', 'transportations.TransID', '=', 'order_head.TransID')
                ->join('order_status', 'order_status.ID', '=', 'order_head.Orderstatus')
                ->leftjoin('products', 'products.ProdID', '=', 'order_head.ProdID')
                ->select(
                    'order_head.*',
                    'transportations.TransName',
                    'products.ProdName',
                    'order_status.UserStatus',
                    'order_head.ExchangeRate',
                    DB::raw("(select CstID from customer where customer.UserID = order_head.UserID ) as CstID"),
                    DB::raw("(select cst_type_name from customer_type where cst_type_id = ( select CstTypeId from customer where customer.UserID = order_head.UserID ) ) as CstTypeName"),
                    DB::raw("(select name from users where users.id = AdminID ) as AdminName"),
                    DB::raw("(select sum(TotalLine) from order_detail where order_detail.OrdID = order_head.ID ) as sum_price_cn")
                )
                ->where('order_head.ID', $order_id)
                ->first();

            $mail = Mail::to($data->email);
            $mail = $mail->send(new OrderUpdate($data, $type, $status_id, $delivery));
        }
        return 'update - complete';
    }

    public function cal_fees(Request $request)
    {
        $TransID = $request->TransID;
        $ProdID = $request->ProdID;
        $CstType = $request->CstType;
        $KG = $request->KG;
        $CBM = $request->CBM;

        $res = array();
        $res['status'] = "error";
        $res['message'] = "ยังไม่ได้ตั้งค่าอัตราค่าขนส่ง";
        $res['data'] = array();
        $res['data']['weight_price'] = 0;
        $res['data']['cbm_price'] = 0;

        $data = TransportationsWeight::select('KG', 'CBM')
            ->where('TransID', $TransID)
            ->where('ProdID', $ProdID)
            ->where('CstTypeId', $CstType)
            ->first();

        if (isset($data->KG)) {
            $res['status'] = "ok";
            $res['message'] = "ยังไม่ได้ตั้งค่าอัตราค่าขนส่ง";
            $weight_price = number_format($data->KG * $KG, 2);
            $cbm_price = number_format($data->CBM * $CBM, 2);
            $res['data']['weight_price'] = $weight_price;
            $res['data']['cbm_price'] = $cbm_price;
        }

        return response()->json(compact('res'));
    }

    public function create_qu(Request $request)
    {
        $order_id = $request->order_id;
        $delivery = $request->delivery;
        $product = $request->product;
        $Total = 0;
        if ($product == 'Y') {
            $data = OrderHead::select('TotalAllTHB')->where('ID', $order_id)->first();
            $Total += $data->TotalAllTHB;
        }
        if ($delivery != "") {
            $data2 = DB::table('order_delivery')->selectRaw('sum(DeliveryTHTH + DeliveryCNCN + DeliveryCNTH ) as sum')->whereRaw("ID in (" . $delivery . ")")->first();
            $Total += $data2->sum;
        }
        $headID = DB::table('quotation')->insertGetId([
            'OrderID' => $order_id,
            'Product' => $product,
            'delivery' => $delivery,
            'Total' => $Total,
            'Status' => '1',
            'created_at' => date('Y-m-d H:i:s'),
        ]);

        $res = array();
        $res['status'] = 'ok';

        return response()->json($res);
    }

    public function cancel_qu(Request $request)
    {
        $quid = $request->quid;

        DB::table('quotation')
            ->where('ID', $quid)
            ->delete();

        return 'delete - complete';
    }
}
