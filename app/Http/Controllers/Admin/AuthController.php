<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Sanctum\PersonalAccessToken;


class AuthController extends Controller
{

    public function verify(Request $request)
    {
        $tk = $request->tk;
        $res = array();
        $res['status'] = 'error';
        $res['message1'] = 'Error';
        $res['message2'] = 'Email Account นี้เคยทำการยืนยันการสมัครสมาชิกแล้ว';
        $chk = User::where('remember_token', $tk)->count();
        if ($chk == 1) {
            User::where('remember_token', $tk)->update([
                'email_verified_at' => date("Y-m-d H:i:s"),
                'remember_token' => NULL,
                'user_status' => 2
            ]);
            $res['status'] = 'ok';
            $res['message1'] = 'Registration Completed';
            $res['message2'] = 'ยืนยันการสมัครสมาชิกเรียบร้อยแล้ว คุณสามารถ login เพื่อเข้าใช้งานระบบได้';
        }
        return view('auth.verify', ["res" => $res]);
    }

    public function index(Request $request)
    {
        if (Auth::check()) {
            return redirect()->route('orders');
        }
        $message = (isset($request->message)) ? $request->message : "";
        return view('auth.login', ["message" => $message]);
    }

    public function login_admin(Request $request)
    {
        if (Auth::attempt(['name' => $request->username, 'password' => $request->password, 'user_status' => 2, 'role' => 'admin']) || Auth::attempt(['name' => $request->username, 'password' => $request->password, 'user_status' => 2, 'role' => 'superadmin'])) {
            // Authentication passed... 
            return redirect()->intended('orders');
        } else {
            return redirect()->route('login');
        }
    }


    public function checklogin()
    {
        if (Auth::check()) {
            // Authentication passed... 
            return redirect()->route('dashboard');
        } else {
            return redirect()->route('login');
        }
    }

    public function logout_admin()
    {
        Auth::logout();
        return redirect()->route('login');
    }
}
