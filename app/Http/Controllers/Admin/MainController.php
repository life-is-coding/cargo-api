<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Http\Controllers\Controller;

class MainController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index()
    {
        $ShipCom = DB::table('shipping_company')->whereIn('Status', ['1'])->get();
        return view('components.transportation', ['ShipCom' => $ShipCom]);
    }
}
