<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Mail\UserRegister;
use Illuminate\Http\Request;
use App\Models\Auth\Customer;
use app\common\behavior\Custom;
use App\Models\Order\OrderHead;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class CustomersController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth');
    }


    public function index()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        $data = Customer::join('customer_type', 'CstTypeId', 'cst_type_id')
            ->join('users', 'users.id', 'customer.UserID')
            ->select(
                'customer.*',
                'cst_type_name',
                'email',
                'user_status'
            )
            ->orderBy('CstID')
            ->get();
        return view('pages.customers', ['data' => $data]);
    }

    public function create()
    {
        $users = DB::table('users')->where('role', '=', 'user')->where('user_status', '=', '2')->get();
        $cst_type = DB::table('customer_type')->where('cst_type_status', '1')->get();
        return view('pages.customers_form', [
            'users' => $users,
            'StaPage' => 'create',
            'cst_type' => $cst_type
        ]);
    }

    public function store(Request $request)
    {
        try {

            $newID = genLastCstID();
            $date = date('Y-m-d H:i:s');
            $email = $request->email;
            if ($email == "") {
                $email = $request->name . "@dummy.com";
            }
            $UserID = DB::table('users')->insertGetId([
                'name' => $request->name,
                'email' => $email,
                'password' => Hash::make($request->password),
                // 'user_status' => $request->CstStatus == 'Y' ? 2 : 0,
                'email_verified_at' => $date,
                'created_at' => $date,
                'role' => 'user',
                'user_status' => 2
            ]);
            DB::table('customer')->insert([
                'CstID' => $newID,
                'UserID' => $UserID,
                'CstFirstname' => $request->CstFirstname,
                'CstLastname' => $request->CstLastname,
                'CstCompanyName' => $request->CstCompanyName,
                'CstTel' => $request->CstTel,
                'CstAddress' => $request->CstAddress,
                'CstProvice' => $request->CstProvice,
                'CstDistrict' => $request->CstDistrict,
                'CstSubdistrict' => $request->CstSubdistrict,
                'CstZipcode' => $request->CstZipcode,
                'CstStatus' => $request->CstStatus == 'Y' ? 1 : 0,
                'created_at' => $date,
                'CstTypeId' => $request->CstTypeId,
                'Remark' => $request->Remark,
            ]);

            $status = 'success';
            $message = 'เพิ่มข้อมูลลูกค้าเรียบร้อย';
        } catch (Exception $e) {
            $status = 'error';
            $message = $e->getMessage();

            DB::table('users')->where('id', $UserID)->delete();
        }
        return redirect()->route('customers')
            ->with($status, $message);
    }

    public function edit($ID)
    {
        $data = DB::table('customer')
            ->join('users', 'users.id', '=', 'customer.UserID')
            ->where('customer.ID', '=', $ID)
            ->get();
        $cst_type = DB::table('customer_type')->where('cst_type_status', '1')->get();

        return view('pages.customers_form', [
            'data' => $data,
            'StaPage' => 'edit',
            'cst_type' => $cst_type
        ]);
    }

    public function update(Request $request, $ID)
    {
        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            // 'user_status' => $request->CstStatus == 'Y' ? 2 : 0,
            'updated_at' => date('Y-m-d H:i:s')
        );
        if ($request->password != '') {
            $data['password'] = Hash::make($request->password);
        }
        DB::table('users')->where('id', $request->UserID)->update($data);
        DB::table('customer')->where('ID', $ID)->update([
            'CstID' => $request->CstID,
            'UserID' => $request->UserID,
            'CstFirstname' => $request->CstFirstname,
            'CstLastname' => $request->CstLastname,
            'CstCompanyName' => $request->CstCompanyName,
            'CstTel' => $request->CstTel,
            'CstAddress' => $request->CstAddress,
            'CstProvice' => $request->CstProvice,
            'CstDistrict' => $request->CstDistrict,
            'CstSubdistrict' => $request->CstSubdistrict,
            'CstZipcode' => $request->CstZipcode,
            'CstStatus' => $request->CstStatus == 'Y' ? 1 : 0,
            'updated_at' => date('Y-m-d H:i:s'),
            'CstTypeId' => $request->CstTypeId,
            'Remark' => $request->Remark,
        ]);
        return redirect()->route('customers')
            ->with('success', 'แก้ไขข้อมูลลูกค้าเรียบร้อย.');
    }

    public function destroy($id)
    {
        $data = DB::table('customer')->select('UserID')->where('id', '=', $id)->first();

        DB::table('customer')->where('id', $id)->delete();
        DB::table('users')->where('id', $data->UserID)->delete();

        return redirect()->route('customers')
            ->with('success', 'Customer delete successfully.');
    }

    public function orders($custid)
    {
        $customer = DB::table('customer')
            ->join('users', 'users.id', '=', 'customer.UserID')
            ->join('customer_type', 'cst_type_id', '=', 'CstTypeId')
            ->where('users.id', '=', $custid)
            ->first();

        $orders = OrderHead::with('detail')
            ->join('transportations AS t', 't.TransID', '=', 'order_head.TransID')
            ->join('order_status AS s', 's.ID', '=', 'order_head.Orderstatus')
            ->leftjoin('products AS p', 'p.ProdID', '=', 'order_head.ProdID')
            ->select(
                'order_head.*',
                't.TransName',
                'p.ProdName',
                's.AdminStatus',
                'order_head.ExchangeRate',
                DB::raw("(select sum(TotalLine) from order_detail where order_detail.OrdID = order_head.ID ) as sum_price_cn"),
                DB::raw("(select name from users where users.id = AdminID ) as AdminName")
            )
            ->whereNotIn('Orderstatus', ['1', '99'])
            ->where('UserID', $custid)
            ->orderBy('order_head.ID', 'asc')
            ->get();

        return view('pages.customers_orders', [
            'orders' => $orders,
            'customer' => $customer
        ]);
    }

    public function resend($user_id)
    {
        try {
            $data = User::join('customer', 'users.id', 'UserID')->find($user_id);

            $user_id = $data->UserID;
            $md5 = $data->remember_token;
            $email = $data->email;

            $mail = Mail::to($email);
            $mail = $mail->send(new UserRegister($data, $md5));

            $status = "success";
            $message = "Send email successfully.";
        } catch (Exception $e) {
            $status = 'error';
            $message = $e->getMessage();
        }
        return redirect()->route('customers')
            ->with($status, $message);
    }

    public function confirm($user_id)
    {
        try {
            User::where('id', $user_id)->update([
                'email_verified_at' => date("Y-m-d H:i:s"),
                'remember_token' => NULL,
                'user_status' => 2
            ]);

            $status = "success";
            $message = "Verify account successfully.";
        } catch (Exception $e) {
            $status = 'error';
            $message = $e->getMessage();
        }
        return redirect()->route('customers')
            ->with($status, $message);
    }
}
