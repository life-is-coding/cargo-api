<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Models\Setting\Shipping;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ShippingController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        $data = DB::table('shipping_company')
            ->get();
        return view('pages.setting.shipping', [
            'data' => $data
        ]);
    }

    public function create()
    {
        // $shipping = DB::table('shipping_company')->where('Status', 1)->get();
        return view('pages.setting.shipping_form', [
            'StaPage' => 'create'
        ]);
    }

    public function store(Request $request)
    {
        shipping::create([
            'CompanyName' => $request->CompanyName,
            'Status' => $request->Status == 'Y' ? 1 : 0,
            "created_at" => date('Y-m-d H:i:s'),
            "AdminID" => Auth::user()->id,
        ]);
        return redirect()->route('shipping')
            ->with('success', 'เพิ่มขนส่งเรียบร้อย.');
    }

    public function edit($ID)
    {
        $data = DB::table('shipping_company')->where('ID', '=', $ID)->get();
        return view('pages.setting.shipping_form', [
            'StaPage' => 'edit',
            'data' => $data,
        ]);
    }

    public function update(Request $request, $ID)
    {
        DB::table('shipping_company')->where('ID', $ID)->update([
            'CompanyName' => $request->CompanyName,
            'Status' => $request->Status == 'Y' ? 1 : 0,
            'updated_at' => date('Y-m-d H:i:s'),
            "AdminID" => Auth::user()->id,
        ]);
        return redirect()->route('shipping')
            ->with('success', 'แก้ไขขนส่งเรียบร้อย.');
    }

    public function destroy($ID)
    {
        DB::table('shipping_company')->where('ID', $ID)->delete();
        return redirect()->route('shipping')
            ->with('success', 'ลบขนส่งเรียบร้อยแล้ว.');
    }
}
