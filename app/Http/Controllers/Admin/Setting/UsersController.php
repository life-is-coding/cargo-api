<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        $data = DB::table('users')->whereIn('role', ['admin', 'superadmin'])->get();
        return view('pages.setting.users', ['data' => $data]);
    }

    public function create()
    {
        return view('pages.setting.users_form', [
            'StaPage' => 'create'
        ]);
    }

    public function store(Request $request)
    {
        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'user_status' => $request->user_status == 'Y' ? 2 : 0,
            'role' => $request->role
        ]);
        return redirect()->route('users')
            ->with('success', 'เพิ่มผู้ใช้งานเรียบร้อย.');
    }

    public function edit($id)
    {
        $data = DB::table('users')
            ->where('ID', '=', $id)
            ->get();
        return view('pages.setting.users_form', [
            'data' => $data,
            'StaPage' => 'edit',
        ]);
    }

    public function update(Request $request, $id)
    {
        $data = array(
            'name' => $request->name,
            'email' => $request->email,
            'user_status' => $request->user_status == 'Y' ? 2 : 0,
            'role' => $request->role,
            'updated_at' => date('Y-m-d H:i:s')
        );
        if ($request->password != '') {
            $data['password'] = Hash::make($request->password);
        }
        DB::table('users')->where('id', $id)->update($data);
        return redirect()->route('users')
            ->with('success', 'แก้ไขผู้ใช้งานเรียบร้อย.');
    }

    public function destroy($id)
    {
        DB::table('users')->where('id', $id)->delete();
        return redirect()->route('users-index')
            ->with('success', 'User delete successfully.');
    }
}
