<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Models\Setting\Fee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class FeesController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        $data = DB::table('transportations_weight AS tw')
            ->join('transportations AS t', 't.TransID', '=', 'tw.TransID')
            ->join('products AS p', 'p.ProdID', '=', 'tw.ProdID')
            ->join('customer_type', 'cst_type_id', 'CstTypeId')
            ->select('tw.*', 't.TransName', 'p.ProdName', 'cst_type_name')
            ->orderBy('cst_type_id', 'asc')
            ->orderBy('t.TransID', 'asc')
            ->orderBy('p.ProdID', 'asc')
            ->orderBy('KG', 'asc')
            ->get();
        return view('pages.setting.fees', [
            'data' => $data
        ]);
    }

    public function create()
    {
        $trans = DB::table('transportations')->whereIn('TransStatus', ['1'])->get();
        $prod = DB::table('products')->whereIn('ProdStatus', ['1'])->get();
        $cst = DB::table('customer_type')->where('cst_type_status', 1)->get();
        return view('pages.setting.fees_form', [
            'StaPage' => 'create',
            'trans' => $trans,
            'prod' => $prod,
            'cst' => $cst
        ]);
    }

    public function store(Request $request)
    {
        Fee::create([
            'TransID' => $request->TransID,
            'ProdID' => $request->ProdID,
            'CBM' => $request->CBM,
            'KG' => $request->KG,
            'CstTypeId' => $request->CstTypeId,
            'WeightStatus' => $request->WeightStatus == 'Y' ? 1 : 0,
            "AdminID" => Auth::user()->id,
        ]);
        return redirect()->route('fees')
            ->with('success', 'เพิ่มค่าธรรมเนียมขนส่งเรียบร้อย.');
    }

    public function edit($WeightID)
    {
        $trans = DB::table('transportations')->whereIn('TransStatus', ['1'])->get();
        $prod = DB::table('products')->whereIn('ProdStatus', ['1'])->get();
        $cst = DB::table('customer_type')->where('cst_type_status', 1)->get();
        $data = DB::table('transportations_weight')->where('WeightID', '=', $WeightID)->get();
        return view('pages.setting.fees_form', [
            'StaPage' => 'edit',
            'data' => $data,
            'trans' => $trans,
            'prod' => $prod,
            'cst' => $cst,
        ]);
    }

    public function update(Request $request, $WeightID)
    {
        DB::table('transportations_weight')->where('WeightID', $WeightID)->update([
            'TransID' => $request->TransID,
            'ProdID' => $request->ProdID,
            'CBM' => $request->CBM,
            'KG' => $request->KG,
            'CstTypeId' => $request->CstTypeId,
            'updated_at' => date('Y-m-d H:i:s'),
            'WeightStatus' => $request->WeightStatus == 'Y' ? 1 : 0,
            "AdminID" => Auth::user()->id,
        ]);
        return redirect()->route('fees')
            ->with('success', 'แก้ไขค่าธรรมเนียมขนส่งเรียบร้อย.');
    }

    public function destroy($WeightID)
    {
        DB::table('transportations_weight')->where('WeightID', $WeightID)->delete();
        return redirect()->route('fees')
            ->with('success', 'Fee delete successfully.');
    }
}
