<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Models\Setting\Fee;
use Illuminate\Http\Request;
use App\Models\Setting\Exchange;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ExchangeController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        $data = DB::table('exchange')->where('ID', '=', 1)
            ->get();
        return view('pages.setting.exchange', ['data' => $data]);
    }

    public function get_data()
    {
        $data = Exchange::select(
            'THRate',
            DB::raw("DATE_FORMAT(LastUpd,'%e/%m/%y %H:%i') as LastUpd")
        )->first();
        return response()->json($data);
    }


    public function update(Request $request, $ID)
    {
        DB::table('exchange')->where('ID', $ID)->update([
            'CNRate' => $request->CNRate,
            'THRate' => $request->THRate,
            'LastUpd' => date('Y-m-d H:i:s')
        ]);
        return redirect()->route('exchange')
            ->with('success', 'แก้ไขอัตราแลกเปลี่ยนส่งเรียบร้อย.');
    }
}
