<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Models\Setting\Fee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class ProductsController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        $data = DB::table('products')
            ->get();
        return view('pages.setting.products', ['data' => $data]);
    }

    public function create()
    {
        return view('pages.setting.products_form', [
            'StaPage' => 'create',
        ]);
    }

    public function store(Request $request)
    {
        DB::table('products')->insert([
            'ProdName' => $request->ProdName,
            'ProdStatus' => $request->ProdStatus == 'Y' ? 1 : 0,
            'created_at' => date('Y-m-d H:i:s'),
        ]);
        return redirect()->route('products')
            ->with('success', 'เพิ่มประเภทสินค้าส่งเรียบร้อย.');
    }

    public function edit($ProdID)
    {
        $data = DB::table('products')->where('ProdID', '=', $ProdID)->get();
        return view('pages.setting.products_form', [
            'StaPage' => 'edit',
            'data' => $data,
        ]);
    }

    public function update(Request $request, $ProdID)
    {
        DB::table('products')->where('ProdID', $ProdID)->update([
            'ProdName' => $request->ProdName,
            'updated_at' => date('Y-m-d H:i:s'),
            'ProdStatus' => $request->ProdStatus == 'Y' ? 1 : 0,
        ]);
        return redirect()->route('products')
            ->with('success', 'แก้ไขประเภทสินค้าส่งเรียบร้อย.');
    }
}
