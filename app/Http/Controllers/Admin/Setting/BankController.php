<?php

namespace App\Http\Controllers\Admin\Setting;

use App\Models\Setting\Bank;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class BankController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        $data = Bank::orderBy('acc_id')
            ->get();

        return view('pages.setting.bank', [
            'data' => $data
        ]);
    }

    public function create()
    {
        $arr_bank = ['ธนาคารกรุงเทพ', 'ธนาคารกสิกรไทย', 'ธนาคารกรุงไทย', 'ธนาคารทหารไทยธนชาต', 'ธนาคารไทยพาณิชย์', 'ธนาคารกรุงศรีอยุธยา', 'ธนาคารเกียรตินาคินภัทร', 'ธนาคารซีไอเอ็มบีไทย', 'ธนาคารทิสโก้', 'ธนาคารยูโอบี', 'ธนาคารออมสิน', 'ธนาคารอาคารสงเคราะห์'];
        return view('pages.setting.bank_form', [
            'StaPage' => 'create',
            'arr_bank' => $arr_bank
        ]);
    }

    public function store(Request $request)
    {
        Bank::create([
            'acc_name' => $request->acc_name,
            'acc_no' => $request->acc_no,
            'acc_bank' => $request->acc_bank,
            'acc_type' => NULL
        ]);
        return redirect()->route('banks')
            ->with('success', 'เพิ่มบัญชีธนาคารเรียบร้อย.');
    }

    public function edit($acc_id)
    {
        $data = Bank::where('acc_id', '=', $acc_id)->get();
        $arr_bank = ['ธนาคารกรุงเทพ', 'ธนาคารกสิกรไทย', 'ธนาคารกรุงไทย', 'ธนาคารทหารไทยธนชาต', 'ธนาคารไทยพาณิชย์', 'ธนาคารกรุงศรีอยุธยา', 'ธนาคารเกียรตินาคินภัทร', 'ธนาคารซีไอเอ็มบีไทย', 'ธนาคารทิสโก้', 'ธนาคารยูโอบี', 'ธนาคารออมสิน', 'ธนาคารอาคารสงเคราะห์'];
        return view('pages.setting.bank_form', [
            'StaPage' => 'edit',
            'data' => $data,
            'arr_bank' => $arr_bank
        ]);
    }

    public function update(Request $request, $acc_id)
    {
        Bank::where('acc_id', $acc_id)->update([
            'acc_name' => $request->acc_name,
            'acc_no' => $request->acc_no,
            'acc_bank' => $request->acc_bank
        ]);
        return redirect()->route('banks')
            ->with('success', 'แก้ไขรายละเอียดบัญชีธนาคารเรียบร้อย.');
    }

    public function destroy($acc_id)
    {
        Bank::where('acc_id', $acc_id)->delete();
        return redirect()->route('banks')
            ->with('success', 'Bank delete successfully.');
    }


    public function change(Request $request)
    {
        Bank::where('acc_type', $request->type)->update([
            'acc_type' => NULL
        ]);
        Bank::where('acc_id', $request->acc_id)->update([
            'acc_type' => $request->type
        ]);
        return redirect()->route('banks')
            ->with('success', 'แก้ไขตั้งค่าบัญชีธนาคารเรียบร้อย.');
    }
}
