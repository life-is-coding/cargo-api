<?php

namespace App\Http\Controllers\Admin\Setting;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Setting\Articles;
use App\Http\Controllers\Controller;

class ArticlesController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index()
    {
        $data = Articles::orderBy('article_id')
            ->get();

        return view('pages.setting.articles', [
            'data' => $data
        ]);
    }

    public function create()
    {
        return view('pages.setting.articles_form', [
            'StaPage' => 'create'
        ]);
    }

    public function store(Request $request)
    {
        $filename = "";
        if ($request->hasFile('article_picture')) {
            $filename = Str::random(10) . '.' . request()->{'article_picture'}->getClientOriginalExtension();
            request()->{'article_picture'}->move(public_path('images/articles'), $filename);
        }

        Articles::create([
            'article_title' => $request->article_title,
            'article_subtitle' => $request->article_subtitle,
            'article_link' => $request->article_link,
            'article_picture' => asset('/images/articles/' . $filename),
            'article_status' => $request->article_status == '1' ? 1 : 0,
        ]);
        return redirect()->route('articles')
            ->with('success', 'เพิ่มบทความเรียบร้อย');
    }

    public function edit($article_id)
    {
        $data = Articles::where('article_id', '=', $article_id)->get();
        return view('pages.setting.articles_form', [
            'StaPage' => 'edit',
            'data' => $data
        ]);
    }

    public function update(Request $request, $article_id)
    {
        $filename = "";
        if ($request->hasFile('article_picture')) {
            $filename = Str::random(10) . '.' . request()->{'article_picture'}->getClientOriginalExtension();
            request()->{'article_picture'}->move(public_path('images/articles'), $filename);
        }

        Articles::where('article_id', $article_id)->update([
            'article_title' => $request->article_title,
            'article_subtitle' => $request->article_subtitle,
            'article_link' => $request->article_link,
            'article_picture' => asset('/images/articles/' . $filename),
            'article_status' => $request->article_status == '1' ? 1 : 0,
        ]);
        return redirect()->route('articles')
            ->with('success', 'แก้ไขบทความเรียบร้อย.');
    }

    public function destroy($article_id)
    {
        Articles::where('article_id', $article_id)->delete();
        return redirect()->route('articles')
            ->with('success', 'ลบบทความเรียบร้อย');
    }
}
