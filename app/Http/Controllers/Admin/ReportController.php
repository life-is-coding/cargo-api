<?php

namespace App\Http\Controllers\Admin;

use DB;
use App\Models\Auth\Customer;
use App\Models\Order\OrderHead;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class ReportController extends Controller
{

    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }
        $cst = DB::table('customer')
            ->join('users', 'users.id', '=', 'customer.UserID')
            ->where('CstStatus', ['1'])
            ->select('customer.*', 'users.email')
            ->get();

        return view('pages.report', [
            'cst' => $cst
        ]);
    }

    public function export_data()
    {

        $input = request()->all();
        $date = $input['date'];
        $chk = $input['chk'];
        $cstid = $input['cstid'];

        $arrHeadTable = ['#', 'เลขที่คำสั่งซื้อ', 'สถานะคำสั่งซื้อ', 'วันที่สั่งซื้อ', 'Admin ที่ดูแล', 'รหัสลูกค้า', 'ประเภทลูกค้า', 'ชื่อ (ลูกค้า)', 'บริษัท (ลูกค้า)', 'เบอร์โทร (ลูกค้า)', 'อีเมล์ (ลูกค้า)', 'ที่อยู่ (ลูกค้า)', 'ขื่อ (จัดส่ง)', 'บริษัท (จัดส่ง)', 'เบอร์โทร (จัดส่ง)', 'อีเมล์ (จัดส่ง)', 'ที่อยู่ (จัดส่ง)', 'เว็บไซท์', 'บัญชีแอดมิน', 'ประเภทขนส่ง', 'ประเภทสินค้า', 'ราคาสินค้ารวม (หยวน)', 'อัตราแลกเปลี่ยน', 'ราคาสินค้ารวม (บาท)', 'เลขที่คำสั่งซื้อ (ร้านค้า)', 'วันที่สั่งซื้อ (ร้านค้า)', 'ข้อมูลการเสนอราคา', 'เลขที่จัดส่ง (ร้านค้า)',  'สถานะการจัดส่ง', 'ค่าขนส่ง จีน-จีน', 'ค่าขนส่ง จีน-ไทย', 'ค่าขนส่ง ไทย-ไทย', 'น้ำหนัก (KG)', 'ปริมาตร (CBM)', 'กว้าง', 'ยาว', 'สูง', 'วันที่ของถึงโกดังจีน', 'วันที่ส่งออกจากโกดังจีน', 'เลขที่บิลใบสั่งซื้อ (PO)', 'ตู้ที่', 'วันที่ของถึงโกดังไทย', 'วันที่จัดส่งหาลูกค้า', 'บริษัทขนส่ง', 'เลขที่พัสดุจัดส่ง'];
        $arrFieldTable = ['no', 'order_no', 'order_status', 'order_date', 'admin_name', 'cust_id', 'cust_type', 'cust_name', 'cust_company', 'cust_tel', 'cust_email', 'cust_address', 'ship_name', 'ship_company', 'ship_tel', 'ship_email', 'ship_address', 'order_web', 'admin_account', 'transport_name', 'product_name', 'total_cn', 'exchange_rate', 'total_th', 'cn_order_no', 'cn_order_date', 'qu', 'delivery'];

        $arrDeliveryField = ['cn_order_tracking', 'del_status_name', 'delivery_cn_cn', 'delivery_cn_th', 'delivery_th_th', 'kg', 'cbm', 'width', 'length', 'height',  'warehouse_date', 'export_date', 'po_number', 'container_no', 'import_date', 'delivery_tracking_no', 'delivery_company', 'delivery_date'];

        $sheetno = 0;
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getSheet($sheetno);
        $spreadsheet->getActiveSheet()->setTitle('สรุป');
        $sheet = $spreadsheet->getActiveSheet();

        $label = "A";
        $row = 1;
        foreach ($arrHeadTable as $head) {
            $sheet->setCellValue($label . $row, $head);
            $label++;
        }
        $row++;

        $data = OrderHead::with('items', 'delivery', 'qu')
            ->join('transportations AS t', 't.TransID', '=', 'order_head.TransID')
            ->join('order_status AS s', 's.ID', '=', 'order_head.Orderstatus')
            ->join('customer AS cst', 'cst.UserID', '=', 'order_head.UserID')
            ->join('customer_type', 'cst.CstTypeId', '=', 'cst_type_id')
            ->leftjoin('products AS p', 'p.ProdID', '=', 'order_head.ProdID')
            // ->leftjoin('shipping_company AS sh', 'sh.ID', '=', 'order_head.ShipComID')
            ->select(
                DB::raw("(select name from users where users.id = order_head.AdminID ) as admin_name"),
                'order_head.ID as ID',
                DB::raw("DATE_FORMAT(order_head.created_at,'%e/%m/%y') as order_date"),
                'order_head.No as order_no',
                'order_head.OrderNumber as cn_order_no',
                DB::raw("DATE_FORMAT(OrderDate,'%e/%m/%y') as cn_order_date"),
                // 'order_head.OrderTracking as cn_order_tracking',
                // DB::raw("DATE_FORMAT(WarehouseCNDate,'%e/%m/%y') as warehouse_date"),
                // DB::raw("DATE_FORMAT(ExportDate,'%e/%m/%y') as export_date"),
                // 'order_head.PurchaseOrder as po_number',
                // 'order_head.Container as container_no',
                // DB::raw("DATE_FORMAT(ImportDate,'%e/%m/%y') as import_date"),
                // 'order_head.TrackingNO as delivery_tracking_no',
                // 'sh.CompanyName as delivery_company',
                // DB::raw("DATE_FORMAT(DeliveryDate,'%e/%m/%y') as delivery_date"),
                DB::raw('CAST(ExchangeRate AS DECIMAL (10,2)) as exchange_rate'),
                DB::raw('CAST(TotalAllCNY AS DECIMAL (10,2)) as total_cn'),
                DB::raw('CAST(TotalAllTHB AS DECIMAL (10,2)) as total_th'),
                's.AdminStatus as order_status',
                // 't.TransName as transport_name',
                // 'p.ProdName as product_name',
                // 'KG as kg',
                // 'Queue as cbm',
                // 'Width as width',
                // 'Length as length',
                // 'Height as height',
                // DB::raw('CAST(DeliveryCNCN AS DECIMAL (10,2)) as delivery_cn_cn'),
                // DB::raw('CAST(DeliveryCNTH AS DECIMAL (10,2)) as delivery_cn_th'),
                // DB::raw('CAST(DeliveryTHTH AS DECIMAL (10,2)) as delivery_th_th'),
                'cst_type_name as cust_type',
                DB::raw("CstID as cust_id"),
                DB::raw("cst.CstFirstname + ' ' + cst.CstLastname as cust_name"),
                'cst.CstCompanyName as cust_company',
                'cst.CstTel as cust_tel',
                DB::raw("( select email from users where id = cst.UserID) as cust_email"),
                DB::raw("cst.CstAddress + ' ' + cst.CstDistrict + ' ' + cst.CstSubdistrict + ' ' + cst.CstProvice + ' ' + cst.CstZipcode as cust_address"),
                DB::raw("order_head.CstFirstname + ' ' + order_head.CstLastname as ship_name"),
                'order_head.CstCompanyName as ship_company',
                'order_head.CstTel as ship_tel',
                'order_head.email as ship_email',
                DB::raw("order_head.CstAddress + ' ' + order_head.CstDistrict + ' ' + order_head.CstSubdistrict + ' ' + order_head.CstProvice + ' ' + order_head.CstZipcode as ship_address"),
                'OrderWeb as order_web',
                'AdminAccount as admin_account',
            );

        if ($date != "") {
            $report_name = "รายงานสรุปคำสั่งซื้อตามช่วงวันที่ " . $date . ".xlsx";
            $arrayDate = explode(' - ', $date);
            $data = $data->whereBetween(DB::raw("DATE_FORMAT(order_head.created_at,'%Y-%m-%e')"), $arrayDate);
        } else {
            $cust = Customer::select('CstID')->where('UserID', $cstid)->first();
            $report_name = "รายงานสรุปคำสั่งซื้อของลูกค้า " . $cust->CstID . ".xlsx";
            $data = $data->where('order_head.UserID', $cstid);
        }
        $data = $data->orderBy('order_head.No')
            ->get();

        // return $date;
        foreach ($data as $key => $order) {
            $label = "A";
            foreach ($arrFieldTable as $field) {
                $merge_row = (count($order['delivery']) > 1) ? count($order['delivery']) - 1 : 0;
                if ($field == "delivery") {
                    foreach ($order['delivery'] as $delivery) {
                        $tmp_label = $label;
                        foreach ($arrDeliveryField as $field_delivery) {
                            $showdata = $delivery[$field_delivery];
                            $sheet->setCellValue($tmp_label . $row, $showdata);
                            $tmp_label++;
                        }
                        if (count($order['delivery']) > 1) {
                            $row++;
                        }
                    }
                } else {
                    if ($field == "qu") {
                        $showdata = "";
                        foreach ($order['qu'] as $qu) {
                            $showdata .= " - " . $qu['qu_date'] . " (";
                            $showdata .= ($qu['Product'] == "Y") ? "ค่าสินค้า" : "";
                            $showdata .= ($qu['Delivery'] != "") ? "ค่าจัดส่ง" : "";
                            $showdata .= ") ";
                            $showdata .= $qu['qu_status_name'];
                            $showdata .= "\r";
                        }
                    } else if ($field == "no") {
                        $showdata = $key + 1;
                    } else if ($field == "total_cn" || $field == "total_th" || $field == "delivery_cn_cn" || $field == "delivery_cn_th" || $field == "delivery_th_th") {
                        $showdata = number_format($order[$field], 2);
                    } else {
                        $showdata = $order[$field];
                    }
                    $sheet->setCellValue($label . $row, $showdata);
                    $sheet->mergeCells($label . $row . ":" . $label . ($row + $merge_row));
                    // A1:V1;
                    $label++;
                }
            }
            if (count($order['delivery']) <= 1) {
                $row++;
            }
        }

        $styleArray = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ],
            'borders' => [
                'top' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],
            ],
            'fill' => [
                'fillType' => \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID,
                'startColor' => [
                    'argb' => 'FFA0A0A0',
                ]
            ],
        ];

        $highestColumn = $spreadsheet->getActiveSheet()->getHighestColumn();
        $spreadsheet->getActiveSheet()->getStyle('A1:' . $highestColumn . '1')->applyFromArray($styleArray);

        $newHigh = $highestColumn++;
        for ($col = 'A'; $col !== $newHigh; $col++) {
            $spreadsheet->getActiveSheet()
                ->getColumnDimension($col)
                ->setAutoSize(true);
        }

        if ($chk == "true") {

            $arrItemHeadTable = ['#', 'รูปสินค้า', 'ชื่อสินค้า', 'รายละเอียดสินค้า', 'Link', 'ราคา/ชิ้น (หยวน)', 'จำนวน', 'ราคารวม (หยวน)', 'ราคารวม (บาท)', 'หมายเหตุ'];
            $arrItemFieldTable = ['item_no', 'img_url', 'item_name', 'item_detail', 'item_url', 'price_cny', 'qty', 'total_cny', 'total_thb', 'remark'];

            foreach ($data as $order) {
                /* Create a new worksheet, after the default sheet */
                $sheetno++;
                $spreadsheet->createSheet();
                $spreadsheet->setActiveSheetIndex($sheetno);
                $spreadsheet->getActiveSheet()->setTitle($order['order_no'] . " (สินค้า)");
                $sheet = $spreadsheet->getActiveSheet();

                $label = "A";
                $row = 1;
                foreach ($arrItemHeadTable as $head) {
                    $sheet->setCellValue($label . $row, $head);
                    $label++;
                }
                $row++;

                foreach ($order['items'] as $item) {
                    $label = "A";
                    foreach ($arrItemFieldTable as $field) {
                        if ($field == "item_no") {
                            $showdata = $row - 1;
                        } else if ($field == "price_cny" || $field == "total_cny" || $field == "total_thb") {
                            $showdata = number_format($item[$field], 2);
                        } else {
                            $showdata = $item[$field];
                        }
                        $sheet->setCellValue($label . $row, $showdata);
                        $label++;
                    }
                    $row++;
                }

                $highestColumn = $spreadsheet->getActiveSheet()->getHighestColumn();
                $spreadsheet->getActiveSheet()->getStyle('A1:' . $highestColumn . '1')->applyFromArray($styleArray);

                $newHigh = $highestColumn++;
                for ($col = 'A'; $col !== $newHigh; $col++) {
                    $spreadsheet->getActiveSheet()
                        ->getColumnDimension($col)
                        ->setAutoSize(true);
                }
            }
        }
        $spreadsheet->setActiveSheetIndex(0);

        $writer = new Xlsx($spreadsheet);
        ob_start();
        $writer->save('php://output');
        $content = ob_get_contents();
        ob_end_clean();

        Storage::disk('public')->put('export/' . $report_name, $content);
        return $report_name;
    }
    public function get_file($report_name)
    {
        return Storage::disk('public')->download('export/' . $report_name, $report_name);
    }
}
