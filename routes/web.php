<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PDFController;
use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\MainController;
use App\Http\Controllers\Admin\OrdersController;
use App\Http\Controllers\Admin\ReportController;
use App\Http\Controllers\Admin\CustomersController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\Setting\BankController;
use App\Http\Controllers\Admin\Setting\FeesController;
use App\Http\Controllers\Admin\Setting\UsersController;
use App\Http\Controllers\Admin\Setting\ShippingController;

use App\Http\Controllers\Admin\Setting\ArticlesController;
use App\Http\Controllers\Admin\Setting\ExchangeController;
use App\Http\Controllers\Admin\Setting\ProductsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/verify', [AuthController::class, 'verify'])->name("verify");

Route::get('/', [AuthController::class, 'checklogin'])->name("checklogin");
Route::get('/login', [AuthController::class, 'index'])->name("login");
Route::post('/login-admin', [AuthController::class, 'login_admin'])->name("login-admin");
Route::get('/logout-admin', [AuthController::class, 'logout_admin'])->name("logout-admin");

Route::get('/main-data', [MainController::class, 'index'])->name("main-data");


Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::get('/report', [ReportController::class, 'index'])->name('report');
Route::post('/report/export', [ReportController::class, 'export_data'])->name('export-data');
Route::get('/file/{file_name}', [ReportController::class, 'get_file'])->name('get-file');

Route::get('/users', [UsersController::class, 'index'])->name('users');
Route::get('/users-create', [UsersController::class, 'create'])->name('users-create');
Route::post('/users-store', [UsersController::class, 'store'])->name('users-store');
Route::get('/users-edit/{id}', [UsersController::class, 'edit'])->name('users-edit');
Route::post('/users-update/{id}', [UsersController::class, 'update'])->name('users-update');
Route::get('/users-destroy/{id}', [UsersController::class, 'destroy'])->name('users-destroy');

Route::get('/fees', [FeesController::class, 'index'])->name('fees');
Route::get('/fees-create', [FeesController::class, 'create'])->name('fees-create');
Route::post('/fees-store', [FeesController::class, 'store'])->name('fees-store');
Route::get('/fees-edit/{id}', [FeesController::class, 'edit'])->name('fees-edit');
Route::post('/fees-update/{id}', [FeesController::class, 'update'])->name('fees-update');
Route::get('/fees-destroy/{id}', [FeesController::class, 'destroy'])->name('fees-destroy');

Route::get('/products', [ProductsController::class, 'index'])->name('products');
Route::get('/products-create', [ProductsController::class, 'create'])->name('products-create');
Route::post('/products-store', [ProductsController::class, 'store'])->name('products-store');
Route::get('/products-edit/{id}', [ProductsController::class, 'edit'])->name('products-edit');
Route::post('/products-update/{id}', [ProductsController::class, 'update'])->name('products-update');
Route::get('/products-destroy/{id}', [ProductsController::class, 'destroy'])->name('products-destroy');

Route::get('/exchange', [ExchangeController::class, 'index'])->name('exchange');
Route::get('/exchange-rate', [ExchangeController::class, 'get_data'])->name('exchange-rate');
Route::post('/exchange-update/{id}', [ExchangeController::class, 'update'])->name('exchange-update');

Route::get('/customers', [CustomersController::class, 'index'])->name('customers');
Route::get('/customers-create', [CustomersController::class, 'create'])->name('customers-create');
Route::post('/customers-store', [CustomersController::class, 'store'])->name('customers-store');
Route::get('/customers-edit/{id}', [CustomersController::class, 'edit'])->name('customers-edit');
Route::post('/customers-update/{id}', [CustomersController::class, 'update'])->name('customers-update');
Route::get('/customers-destroy/{id}', [CustomersController::class, 'destroy'])->name('customers-destroy');
Route::get('/customers-orders/{id}', [CustomersController::class, 'orders'])->name('customers-orders');
Route::get('/customers-resend-email/{id}', [CustomersController::class, 'resend'])->name('customers-resend-email');
Route::get('/customers-confirm-email/{id}', [CustomersController::class, 'confirm'])->name('customers-confirm-email');

Route::get('/orders', [OrdersController::class, 'index'])->name('orders');
Route::get('/orders-request', [OrdersController::class, 'request'])->name('orders-request');
Route::get('/orders-new', [OrdersController::class, 'new'])->name('orders-new');
Route::post('/orders-table/{status}', [OrdersController::class, 'order_table'])->name('orders-table');
Route::get('/orders-status', [OrdersController::class, 'order_status'])->name('orders-status');
Route::post('/orders-booking', [OrdersController::class, 'booking'])->name('orders-booking');
Route::get('/orders-create', [OrdersController::class, 'create'])->name('orders-create');
Route::post('/orders-store', [OrdersController::class, 'store'])->name('orders-store');
Route::get('/orders-edit/{id}', [OrdersController::class, 'edit'])->name('orders-edit');
Route::post('/orders-update/{id}', [OrdersController::class, 'update'])->name('orders-update');
Route::post('/orders-cancel', [OrdersController::class, 'delete'])->name('orders-cancel');
Route::get('/orders-view/{id}', [OrdersController::class, 'view'])->name('orders-view');
Route::post('/change-to-new-orders', [OrdersController::class, 'change_order'])->name('change-order');

Route::post('/create-qu', [OrdersController::class, 'create_qu'])->name('create-qu');
Route::post('/cancel-qu', [OrdersController::class, 'cancel_qu'])->name('cancel-qu');

Route::post('/orders-send-status/{id}/{status_id}/{type}', [OrdersController::class, 'update_status'])->name('orders-update-status');

Route::get('/orders-quotation/{id}', [OrdersController::class, 'quotation'])->name('orders-quotation');

Route::post('/orders-cal-fees', [OrdersController::class, 'cal_fees'])->name('orders-cal-fees');

Route::get('/banks', [BankController::class, 'index'])->name('banks');
Route::get('/banks-create', [BankController::class, 'create'])->name('banks-create');
Route::post('/banks-store', [BankController::class, 'store'])->name('banks-store');
Route::get('/banks-edit/{acc_id}', [BankController::class, 'edit'])->name('banks-edit');
Route::post('/banks-update/{acc_id}', [BankController::class, 'update'])->name('banks-update');
Route::get('/banks-destroy/{acc_id}', [BankController::class, 'destroy'])->name('banks-destroy');
Route::post('/banks-change', [BankController::class, 'change'])->name('banks-change');

Route::get('/articles', [ArticlesController::class, 'index'])->name('articles');
Route::get('/articles-create', [ArticlesController::class, 'create'])->name('articles-create');
Route::post('/articles-store', [ArticlesController::class, 'store'])->name('articles-store');
Route::get('/articles-edit/{acc_id}', [ArticlesController::class, 'edit'])->name('articles-edit');
Route::post('/articles-update/{acc_id}', [ArticlesController::class, 'update'])->name('articles-update');
Route::get('/articles-destroy/{acc_id}', [ArticlesController::class, 'destroy'])->name('articles-destroy');

Route::get('/shipping', [ShippingController::class, 'index'])->name('shipping');
Route::get('/shipping-create', [ShippingController::class, 'create'])->name('shipping-create');
Route::post('/shipping-store', [ShippingController::class, 'store'])->name('shipping-store');
Route::get('/shipping-edit/{id}', [ShippingController::class, 'edit'])->name('shipping-edit');
Route::post('/shipping-update/{id}', [ShippingController::class, 'update'])->name('shipping-update');
Route::get('/shipping-destroy/{id}', [ShippingController::class, 'destroy'])->name('shipping-destroy');