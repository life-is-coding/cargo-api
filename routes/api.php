<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\TestController;
use App\Http\Controllers\Api\Auth\AuthController;
use App\Http\Controllers\Api\Main\MainController;

use App\Http\Controllers\Api\Order\CartController;
use App\Http\Controllers\Api\Order\OrderController;
use App\Http\Controllers\Api\Main\ProductController;
use App\Http\Controllers\Api\Auth\RegisterController;
use App\Http\Controllers\Api\Main\TransportationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/main-data', [MainController::class, 'active']);

// AUTH
Route::prefix('auth')->group(function () {
    Route::post('/validate/email', [RegisterController::class, 'check_email']);
    Route::post('/validate/username', [RegisterController::class, 'check_username']);
    Route::post('/register', [RegisterController::class, 'register']);
    Route::post('/verify', [RegisterController::class, 'verify']);
    Route::post('/login', [AuthController::class, 'login']);
    Route::get('/logout', [AuthController::class, 'logout']);
});

Route::prefix('auth')->middleware('auth:sanctum')->group(function () {
    Route::get('/user', [AuthController::class, 'user']);
    Route::post('/change-password', [AuthController::class, 'change_password']);
});

Route::prefix('cart')->middleware('auth:sanctum')->group(function () {
    Route::post('/manual-save', [CartController::class, 'manual_save']);
    Route::post('/save', [CartController::class, 'save']);
    Route::get('/view-all', [CartController::class, 'view_all']);
    Route::get('/delete/{cart_id}', [CartController::class, 'delete']);
    Route::post('/update', [CartController::class, 'update']);
    Route::post('/order', [CartController::class, 'order']);
});


Route::prefix('order')->middleware('auth:sanctum')->group(function () {
    Route::get('/view/{status}', [OrderController::class, 'view']);
    Route::get('/view-order/{order_id}', [OrderController::class, 'view_order']);
    Route::post('/confirm-order', [OrderController::class, 'confirm_order']);
});


// TEST
Route::get('test', [TestController::class, 'test']);


// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::prefix('active')->group(function () {
//     Route::get('/products', [ProductController::class, 'active']);
//     Route::get('/transportations', [TransportationController::class, 'active']);
// });
